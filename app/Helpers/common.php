<?php

use App\Model\admin\CurrencyModel;
use Illuminate\Support\Facades\DB;

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for converting string into indian currency format>
 */
function convertToCurrency($cash, $fromCurrency="NGN", $toCurrency="GBP")
{
//     $fromCurrency = "INR";
//     $toCurrency = "USD";
//     $amount = "2004.5";
    // echo $cash." ".$fromCurrency." ".$toCurrency." ";
    
    if( $fromCurrency == "USD" )
    {
        $exhangeRate = 1;
        $convertedAmount = $cash;
        $fromCurrency = urlencode($fromCurrency);
        $toCurrency = urlencode($toCurrency);
    }
    else
    {
        $fromCurrency = urlencode($fromCurrency);
        $toCurrency = urlencode($toCurrency);
        $url  = "https://www.google.com/search?q=".$fromCurrency."+to+".$toCurrency;
        $get = file_get_contents($url);
        $data = preg_split('/\D\s(.*?)\s=\s/',$get);
        $exhangeRate = (float) substr($data[1],0,7);
        $convertedAmount = $cash * $exhangeRate;
    }
    
    return array( 'exhangeRate' => $exhangeRate, 'convertedAmount' => $convertedAmount, 'fromCurrency' => strtoupper( $fromCurrency ), 'toCurrency' => strtoupper( $toCurrency ) );
    
	$num=(int)$cash; //take only numeric part
	$decpart = $cash - $num; //take  decimal part 
	//$decpart=sprintf("%01.2f",$decpart); //get only two digit of decimal part 
	$decpart=substr($decpart,1,3); 
	$explrestunits ='';

		if(strlen($num)>3){ //if number is greater than 100  
				$lastthree = substr($num, strlen($num)-3, strlen($num)); 	
				$restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits 	
				$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping. 
				$expunit = str_split($restunits, 2); 	
				for($i=0; $i<sizeof($expunit); $i++){ 	
					$explrestunits .= (int)$expunit[$i].","; // creates each of the 2's group and adds a comma to the end 
			 }    
				$thecash = $explrestunits.$lastthree; 
		}	else { 
			  $thecash = (int)$cash; 
		} 
		   // return $thecash.".".$currency; // writes the final format where $currency is the currency symbol. 
	return $thecash.$decpart;
}  

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for @abstact function will check if form is posted>
 */
function isPost()
{
	if( $_SERVER['REQUEST_METHOD'] == "POST" || !empty($_POST) )
	{
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for >
 * This function decode ids and return in array.
 *basically it will decode using base64 algo.
 */
function _de($id)
{
	return base64_decode($id);
}

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for >
 *	This function encode ids and return in array.
 *	basically it will encode using base64 algo.
 */
function _en($id)
{
	return base64_encode($id);
}

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for >
 *	Load image from url. if not file exist then
 *	it will load default selected image.
 *	@params : $url -> URL of image [url will be relative].
 *			  $fl -> Flag stand for return image path only.
 *	@returrn : Path of image
 */
function load_image($url)
{
	if( $url != '' && file_exists('./'.$url) )
		return url($url);
	else
		return url("public/images/no-image.jpg");
}

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for >
 *	Load image from url. if not file exist then
 *	it will load default selected image.
 *	@params : $url -> URL of image [url will be relative].
 *			  $fl -> Flag stand for return image path only.
 *	@returrn : Path of image
 */
function load_gif( $path, $name)
{
    if( $name != '' && file_exists( $path.'/'.$name ) )
        return url( $path.'/'.$name );
	else
		return url("public/images/drag-drop-upload.gif");
}

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for @abstract Function will check if array is empty>
 */	
function isEmptyArr( $arr )
{
    if( is_array($arr) )
    {
        $arr = array_filter($arr);
        if ( !empty($arr) ) { return false; }
        else { return true; }
    }
    else 
    { 
		return false; 
	}
}

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for @abstract function will convert timein seconds to string>
 */
function time2string($time)
{
    $second = 1;
    $minute = 60*$second;
    $hour   = 60*$minute;
    $day    = 24*$hour;
    
    $ans[0] = floor($time/$day);
    $ans[1] = floor(($time%$day)/$hour);
    $ans[2] = floor((($time%$day)%$hour)/$minute);
    $ans[3] = floor(((($time%$day)%$hour)%$minute)/$second);
    
    return ( !empty($ans[0]) ? $ans[0].' Day ': '' ).( !empty($ans[1]) ? $ans[1].' Hr ': '' ).( !empty($ans[2]) ? $ans[2].' Min ': '' ).( !empty($ans[3]) ? $ans[3].' Sec': '' );
}

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for @abstract fetch string within specified start and end>
 */
function fetchSubStr( $str, $start, $end, &$offsetI=0 )
{
	$pos1 = strpos( $str, $start );
	if( $pos1 !== FALSE )
	{
		$pos1 = $pos1 + strlen( $start );

		$pos2 = FALSE;
		if( !empty( $end ) )	
		{
			$pos2 = strpos( $str, $end, $pos1 );
		}
		
		if( $pos2 !== FALSE )
		{
			$offsetI = $pos2;
			return substr( $str, $pos1, ( $pos2 - $pos1 ) );
		}
		else
		{
			$offsetI = $pos1;
			return substr( $str, $pos1 );
		}
	}
}

/**
 * @Function:        <login>
 * @Author:          Gautam Kakadiya( Sixty13 Dev Team )
 * @Created On:      <10-02-2020>
 * @Last Modified By:Gautam Kakadiya
 * @Last Modified:   Gautam Kakadiya
 * @Description:     <This function for @abstract fetch last substring within specified start and end>
 */
function fetchLastSubStr( $str, $start, $end, &$offsetI=0 )
{
	$pos1 = strrpos( $str, $start );
	if( $pos1 !== FALSE )
	{
		$pos1 = $pos1 + strlen( $start );

		$pos2 = FALSE;
		if( !empty( $end ) )
		{
			$pos2 = strpos( $str, $end, $pos1 );
		}

		if( $pos2 !== FALSE )
		{
			$offsetI = $pos2;
			return substr( $str, $pos1, ( $pos2 - $pos1 ) );
		}
		else
		{
			$offsetI = $pos1;
			return substr( $str, $pos1 );
		}
	}
}

/**
 * @Function:        <cmn_getURL>
 * @Author:          Gautam Kakadiya
 * @Created On:      <07-02-2020>
 * @Last Modified By:
 * @Last Modified: 
 * @Description:     <This methode create proper browser URL>
 */
function cmn_getURL( $url = "" )
{   
    if ( strpos( $url, 'http://') !== false) {
        //
    }
    else if ( strpos( $url, 'https://') !== false) {
        //
    }
    else
    {
        $url = "http://".$url;
    }
    
    return $url;
}

/**
 * @Function:        <startQueryLog>
 * @Author:          Gautam Kakadiya
 * @Created On:      <27-02-2020>
 * @Last Modified By:
 * @Last Modified:
 * @Description:     <This methode start query log>
 */
function startQueryLog()
{
    DB::enableQueryLog();
}

/**
 * @Function:        <displayQueryResult>
 * @Author:          Gautam Kakadiya
 * @Created On:      <27-02-2020>
 * @Last Modified By:GAUTAM KAKADIYA
 * @Last Modified:
 * @Description:     <This methode create result array>
 */
function displayQueryResult()
{
    $query = DB::getQueryLog();
    pr($query);
}

/**
 * @Function:        <getURLSegmentValue>
 * @Author:          Gautam Kakadiya
 * @Created On:      <04-07-2020>
 * @Last Modified By: GAUTAM KAKADIYA
 * @Last Modified:
 * @Description:     <This methode return segmant value>
 */
function getURLSegmentValue( $val=1 )
{
    $uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri_segments = explode('/', $uri_path);
    return $uri_segments[$val];
}

/**
 * 
 */
function getUserCurrency( $country_id = 0 )
{
    $country_id = ( !empty( $country_id ) ) ? $country_id : getDefaultCountry();
    
	return CurrencyModel::where('country_id', $country_id)->first();
}

/**
 *
 */
function getDefaultCountry()
{
    return 160;//NIGERIA
}