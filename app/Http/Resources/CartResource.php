<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Traits\CommonFunctions;

class CartResource extends JsonResource
{
    use CommonFunctions;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_name' => $this->product_name,
            'product_image' => (optional($this->product)->images) ? $this->getImage($this->product->images) : null,
            'quantity' => $this->quantity,
            'weight' => $this->weight,
            'real_price' => $this->real_price,
            'price' => $this->price,
            'discount' => $this->discount,
            'product_id' => $this->product_id,
            'discounted_price' => $this->discounted_price,
            'stock' => $this->stock,
            'sell_type' => $this->sell_type,
            'sell_type_options' => $this->sell_type_options,
			'gst' => $this->gst,
            'error_message' => $this->getError(),
            'total_available_stocks' => $this->product->stocks->total_available_stock
        ];
    }

    public function getImage($imagesObject) 
    {
        $image = null;
        $productImages = json_decode($imagesObject, true);
        if (count($productImages) > 0 ) {
            foreach ($productImages as $img) {
                if (!$image) {
                    $image = config('villezone.website_url').'/storage/'.getStorageURL().'products/'.$img;
                }
            }
        }
        return $image;
    }

    public function getError() 
    {
        $product = $this->product;
        if ($product) {
            $calculatedPriceAndWeight = ($product->sell_type == 'weight' && $this->weight) ?
                $this->calculatePriceAndStock($this->weight, $product) : null;
                
            $minusStockValue = ($calculatedPriceAndWeight) ?
                ($calculatedPriceAndWeight['finalWeight'] * $this->quantity) : $this->quantity;

            if ($product->stock - $minusStockValue < 0) {
                return 'Opps Product is out of stock better luck next time';
            } else {
                return false;
            }
        } else {
            return 'Sorry this product has been removed';
        }
    }
}
