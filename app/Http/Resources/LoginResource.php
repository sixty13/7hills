<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LoginResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'message' => $this['message'],
            'token_type' => 'bearer',
            'access_token' => $this['access_token'],
            'user_detail' => new AccountResource(auth()->guard('staff-api')->user()),
            // 'total_cart_products' => auth()->guard('customer-api')->user()->cartDetails->count()
        ];
    }
}
