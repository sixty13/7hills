<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Traits\CommonFunctions;
use Carbon\Carbon;

class AreaResourceForSingle extends JsonResource
{
    use CommonFunctions;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'city_name' => $this->city->name,
            'state_name' => $this->state->name,
            'country_name' => $this->country->name,
            'pincode' => json_decode($this->pincode),
            'minimum_order' => ($this->minimum_order_2 != 0) ? $this->minimum_order_2 : $this->minimum_order,
            'delivery_charge' => $this->getDeliveryCharge($this->getUserCartTotal()),
            'expected_delivery_date' => $this->formatExpectedDate($this->delivery_in_days, true),//date('d-m-Y'),//
            'time_slot' => oh_getExpectedDeliveryDateOfOrder($this->delivery_in_days, true),
        ];
    }

    public function formatExpectedDate($deliveryInDays) 
    {
        $expectedDate = oh_getExpectedDeliveryDateOfOrder($deliveryInDays);
        $res = 0;
        foreach ( $expectedDate as $k=>$ar )
        {
            if( $res == 0 )
            {
                return Carbon::parse($expectedDate[$k]['date'])->format('d-m-Y');
                break;
            }
            
            $res++;
        }
    }
}
