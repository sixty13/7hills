<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LocationSearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $totalWeight = 0;
        foreach ( $this->orderDetails as $ar )
        {
            $total = cmn_vw_QuantityPerfaction( $ar );
            $totalWeight+= $total;
        }
  
        return [
            'id' => $this->id,
            'customer_id' => $this->customer->id,
            'name' => $this->customer->name,
            'image' => $this->customer->profic_pic,
            'number' => $this->customer->mobile_number,
            'total' => $this->total_with_delivery_charge,
            'payment_type' => $this->payment_method,
            'location' => $this->order_location,
            'order_key' => $this->order_key,
            'total_weight' => $totalWeight,
        ];
    }

}
