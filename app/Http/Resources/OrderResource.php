<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $firstProduct = optional($this->orderDetails()->first())->product;

        return [
            'id' => $this->id,
            'order_key' => $this->order_key,
            'order_date' => Carbon::parse($this->order_date)->format('d-m-Y h:i a'),
            'expected_delivery_date' => Carbon::parse($this->expected_delivery_date)->format('d-m-Y').' between '.$this->delivery_time_slot,
            'time_slot' => $this->delivery_time_slot,
            'total_order_price' => $this->total_order_price,
            'wallet_amount' => $this->wallet_amount,
            'payable_amount' => $this->payable_amount,
            'delivery_charge' => $this->delivery_charge,
			'coupon_discount' => $this->coupon_discount,
            'total_with_delivery_charge' => $this->total_with_delivery_charge,
            'payment_method' => $this->payment_method,
            'payment_status' => $this->payment_status,
            'order_status' => $this->order_status,
            'delivery_boy' => ['name' => ( @$this->deliveryboy->name ) ? $this->deliveryboy->name : '', 'mobile_number' => ( @$this->deliveryboy->mobile_number ) ? $this->deliveryboy->mobile_number : '', 'profile_pic' => ( @$this->deliveryboy->image ) ? $this->deliveryboy->image : ''],
            'image' => ($firstProduct) ? $this->getImage($firstProduct) : null,
            'feedback' => ($this->feedback) ? $this->feedback->feedback : null,
            'updated_at' => Carbon::parse($this->updated_at)->format('d-m-Y h:i A'),
        ];
    }

    public function getImage($product) 
    {
        if (!$product->images) {
            return null;
        }

        $image = null;
        $productImages = json_decode($product->images, true);
        if (count($productImages) > 0 ) {
            foreach ($productImages as $img) {
                if (!$image) {
                    $image = config('villezone.website_url').'/storage/'.getStorageURL().'products/'.$img;
                }
            }
        }
        return $image;
    }
}
