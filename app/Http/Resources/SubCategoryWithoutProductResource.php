<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SubCategoryWithoutProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $imageUrl = ($this->image) ? config('villezone.website_url').'/storage/'.getStorageURL().'sub-category/'.$this->image : null;
        return [
            'id' => $this->id,
            'title' => $this->title,
            'image' => $imageUrl,
        ];
    }
}
