<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'name' => $this->name,
            'gujarati_name' => $this->gujarati_name,
            'hindi_name' => $this->hindi_name,
            'description' => $this->description,
            'gujarati_description' => $this->gujarati_description,
            'hindi_description' => $this->hindi_description,
            'price' => $this->price,
            'discount' => $this->discount,
            'discounted_price' => $this->discounted_price,
            'sell_type' => $this->sell_type,
            'sell_type_options' => json_decode($this->sell_type_options, true),
            'stock' => $this->stocks->total_available_stock ?? 0,
            'images' => ($this->images) ? $this->getImages($this->images) : [],
        ];
    }
    
    public function getImages($imagesObject)
    {
        $images = [];
        $productImages = json_decode($imagesObject, true);
        if (count($productImages) > 0 ) {
            foreach ($productImages as $img) {
                $images[] = url('/storage/'.getStorageURL().'products/'.$img );
            }
        }
        return $images;
    }
}
