<?php

namespace App\Http\Resources;

use App\Models\Membership;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {  
        return [
                'id' => $this->id,
                'first_name' => $this->first_name,
                'last_name' => $this->last_name,
                'email' => $this->email,
                'mobile_number' => $this->mobile_number,
                'password' => $request->password,
                'designation' => $this->designation,
                'allowed_discount' => $this->allowed_discount,
        ];
    }
}
