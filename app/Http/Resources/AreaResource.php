<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Traits\CommonFunctions;
use Carbon\Carbon;

class AreaResource extends JsonResource
{
    use CommonFunctions;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'pincode' => json_decode($this->pincode),
            'minimum_order' => $this->minimum_order_2,
            'delivery_charge' => $this->delivery_charge_2,
            'expected_delivery_date' => $this->formatExpectedDate($this->delivery_in_days),
            'time_slot' => oh_getExpectedDeliveryDateOfOrder($this->delivery_in_days),
        ];
    }

    public function formatExpectedDate($deliveryInDays) 
    {
        $expectedDate = oh_getExpectedDeliveryDateOfOrder($deliveryInDays);
        $res = 0;
        foreach ( $expectedDate as $k=>$ar )
        {
            if( $res == 0 )
            {
                return Carbon::parse($expectedDate[$k]['date'])->format('d-m-Y');
                break;
            }
            
            $res++;
        }
    }
}
