<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $imageUrl = ($this->image) ? url('storage/'.getStorageURL().'category/'.$this->image) : null;
        $subCategories = $this->subCategories;
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'image' => $imageUrl,
            'sub_category' => ($subCategories->count() > 0) ? 1 : 0,
            'products' => ($subCategories->count() > 0) ? 0 : 1,
        ];
    }
}
