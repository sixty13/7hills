<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreOrUpdateProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(Request $request)
    {
        $rules = [];

        if (!$this->gujarati_name && !$this->hindi_name) {
            $rules['name'] = ['required', 'max:255']; 
        }
        if (!$this->gujarati_description && !$this->hindi_description) {
            $rules['description'] = ['required']; 
        }

        $rules['category_id'] = ['required', 'numeric']; 
        $rules['price'] = ['required', 'numeric']; 
        ($request['sell_type'] == "weight") ? $rules['set_default_price'] = ['required'] : ""; 
        $rules['discount'] = ['nullable', 'numeric'];
        $rules['discounted_price'] = ['required', 'numeric'];
        // $rules['stock'] = ['required', 'numeric'];
        
        if( !isset( $request['_method'] ) )
            $rules['sku'] = ['required', 'unique:products,sku'];

        return $rules;
    }
}
