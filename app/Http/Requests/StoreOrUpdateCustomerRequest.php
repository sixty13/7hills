<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'mobile_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            // 'address' => ['required'],
            'email' => ['required', 'email'],
            // 'password' => ['required', 'confirmed', 'min:8'],
            // 'area_id' => ['required'],
            // 'pin_code' => ['required'],
        ];
    }
}
