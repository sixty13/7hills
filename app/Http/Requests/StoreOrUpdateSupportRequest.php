<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreOrUpdateSupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => ['required'],
                'email' => ['required', 'email', 'unique:users'],
                'professional_email' => ['required', 'email', 'unique:users','different:email'],
                'joining_date' => ['required'],
                'address' => ['required'],
                'gender' => ['required'],
                'mobile_number' => ['required', 'numeric','unique:users','regex:/[0-9]{10}/'],
                'profile_image' => ['required','mimes:jpg,jpeg,png','max:5120']
        ];
    }
}
