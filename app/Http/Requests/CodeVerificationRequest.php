<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CodeVerificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/'],
            'code' => ['required', 'numeric'],
        ];
    }
}
