<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateDeliveryBoyRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users' ],
            'professional_email' => ['required', 'email', 'unique:users' ],
            'joining_date' => ['required'],
            'address' => ['required'],
            'gender' => ['required'],
            'mobile_number' => ['required', 'numeric', 'unique:users'],
            'area' => ['required', 'array'],
            'licence_number' => ['required'],
            'profile-image' => ['required']
        ];
    }
}
