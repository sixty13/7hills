<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateAreaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required'],
            'pincode' => ['required'],
            'minimum_order' => ['required', 'numeric'],
            'delivery_charge' => ['required', 'numeric'],
            'delivery_in_days' => ['required', 'numeric'],
            'minimum_order_2' => ['nullable', 'numeric'],
            'delivery_charge_2' => ['nullable', 'numeric'],
        ];
    }
}
