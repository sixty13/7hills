<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateTelecallerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'joining_date' => ['required'],
            'address' => ['required'],
            'gender' => ['required'],
            'mobile_number' => ['required', 'numeric','unique:users','regex:/[0-9]{10}/'],
            'profile_image' => ['required','mimes:jpg,jpeg,png','max:5120']
        ];
    }
}
