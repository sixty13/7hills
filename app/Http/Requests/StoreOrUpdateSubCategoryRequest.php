<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateSubCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'category_id' => ['required', 'numeric'],
            'title' => ['required'],
            'slug' => ['required'],
            'image' => ['required', 'mimes:jpeg,png']
        ];
    }
}
