<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    use HasFactory;
    
    public function subsettings(){
        return $this->hasMany('App\Models\SubSettings', 'setting_id', 'id');
    }
}
