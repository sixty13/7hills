<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubSettings extends Model
{
    use HasFactory;

    public function settings(){
        return $this->belongsTo('App\Models\Settings');
    }
}
