<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    use HasFactory;

    protected $table = 'quotation';

    public function customer(){
        return $this->belongsTo('App\Models\Customer', 'cid');
    }

    public function staff(){
        return $this->belongsTo('App\Models\Staff', 'sid');
    }

    public function quotationDetails(){
        return $this->hasMany('App\Models\QuotationDetails', 'qid');
    }
}
