<?php

namespace App\FlowDash\Http\Controllers\Api\staff;

use App\Http\Controllers\Controller;
// use App\Http\Requests\ChangePasswordRequest;
// use App\Models\CustomerAddress;
use App\Traits\SendSMSAndVerifyOTP;
// use App\Models\Customer;
use App\Http\Requests\MobileVerificationRequest;
use App\Http\Requests\CodeVerificationRequest;
use App\Http\Requests\StoreOrUpdateCustomerRequest;
// use App\Http\Requests\StoreOrUpdateCustomerRequest;
// use App\Http\Requests\UpdateCustomerRequest;
// use App\Http\Requests\ForgotPasswordRequest;
use App\Models\MobileVerification;
use App\Http\Resources\LoginResource;
use App\Models\Staff;
// use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
// use App\Mail\WelcomeCustomer;
// use Illuminate\Support\Facades\Mail;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Validation\ValidationException;
// use Illuminate\Http\Request;
// use App\Models\Guest_members;
// use Illuminate\Support\Facades\DB;

class StaffApiController extends Controller
{
    use SendSMSAndVerifyOTP;

    public function verifyMobile(MobileVerificationRequest $request) 
    {
        $inputs = $request->validated();
        MobileVerification::where('mobile_number', $inputs['mobile_number'])->delete();
        $staff = Staff::where('mobile_number', $inputs['mobile_number'])->first();

        $checkUserExist = Staff::where('mobile_number', $inputs['mobile_number'])->first();
        
        if ($checkUserExist)
        {
            $inputs['code'] = mt_rand(111111,999999);
            MobileVerification::create($inputs);  
    
            try {
                if($request->signCode){
                    $message = '<#> 7hills: Your code is '.$inputs['code'].' '.$request->signCode;
                }else 
                    $message = 'Thanks to be a part of our 7hills family, Your OTP is: '.$inputs['code'];
                
                $this->sendMessage($inputs['mobile_number'], $message);
    
                if(!empty($staff)) {
                    // dd($staff->email);
                    $email = $staff->email;
                    $this->sendMail($email, $message);
                }
            } catch (\Exception $e) {
                Log::error($e);
                return response(['error' => ['message' => 'Something went wrong, please try again.']], 500);
            }    
            return response()->json(['data' => ['message' => 'OTP sent successfully.', 'OTP' => $inputs['code'] ]], 200 );
        }
        else
            return response()->json(['data' => ['message' => 'User does not exist', 'is_register' => 0, 'pass_code' => 'FDNULL' ]], 201);
    }
    
    public function verifyCustomer(CodeVerificationRequest $request)
    {
        $inputs = $request->validated();
        $mobileNumberVerify = $this->verifyMobileVerificationCode($inputs['mobile_number'], $inputs['code']);
        
        if ($mobileNumberVerify)
        {
            return response()->json( ['data' => ['message' => 'OTP verified successfully.', 'is_register' => 0] ], 200);
        }
        
        return response(['error' => ['message' => 'OTP incorrect. Please try again.']], 422);
    }
    
    /**
     * @deprecated
     * @param CodeVerificationRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    // public function verifyCodeForCreateCustomer(CodeVerificationRequest $request) 
    // {
    //     $inputs = $request->validated();
    //     $mobileNumberVerify = $this->verifyMobileVerificationCode($inputs['mobile_number'], $inputs['code']);
        
    //     if ($mobileNumberVerify)
    //         return response()->json(['data' => ['message' => 'OTP verified successfully.' ]], 200);
            
    //     return response(['error' => ['message' => 'OTP incorrect. Please try again.']], 422);
    // }
    
    /**
     * @deprecated
     * @param CodeVerificationRequest $request
     * @return \Illuminate\Http\JsonResponse|\App\Http\Resources\LoginResource|\Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    // public function verifyCodeForLoginCustomer(CodeVerificationRequest $request)
    // {
    //     $inputs = $request->validated();
    //     $mobileNumberVerify = $this->verifyMobileVerificationCode($inputs['mobile_number'], $inputs['code']);
      
    //     if ( $mobileNumberVerify )
    //     {
    //         $credentials = request(['mobile_number', 'password']);
            
    //         if ( !$token = auth()->guard('customer-api')->attempt($credentials) )
    //             return response()->json(['error' => ['message' => 'Invalid mobile number or password combination.']], 421);

    //         return new LoginResource(['access_token' => $token, 'message' => 'Logged in successfully.']);
    //     }
    //     return response(['error' => ['message' => 'OTP incorrect. Please try again.', 'dev_message' => 'OTP incorrect. Please try again.']], 422);
    // }

    public function createCustomer(StoreOrUpdateCustomerRequest $request) 
    {
        $inputs = $request->validated();
        if( !empty( $request['refer_code'] ) )
        {
            $referVerification = Staff::where( 'refer_code', strtoupper( $request['refer_code'] ) )->first();
            
            if( !$referVerification )
                return response(['error' => ['message' => 'Please verify your refer code first.', 'dev_message' => 'Please verify your refer code first.']], 401 );
            else
                $inputs['reference_id'] = $referVerification->id;
        }
        
        $verifiedNumber = MobileVerification::where('mobile_number', $inputs['mobile_number'])->where('verified', 1)->whereNull('customer_id')->first();
        if (!$verifiedNumber)
            return response(['error' => ['message' => 'Please verify your mobile number first.', 'dev_message' => 'Please verify your mobile number first.']], 403);
        
        $verifiedEmail = Staff::where('email', $inputs['email'])->first();
        
        if ($verifiedEmail)
            return response(['error' => ['message' => 'This email Id is already registered with us. Please use another email Id', 'dev_message' => 'This email Id is already registered with us. Please use another email Id']], 403);        
        
        $inputs['password'] = $inputs['mobile_number'];
        $loginDetails = ['mobile_number' => $inputs['mobile_number'], 'password' => $inputs['password']];
        $inputs['refer_code'] = strtoupper( "VL".substr( uniqid(), 0, 8 ) );//generateRandomString(8);
        $inputs['email'] = $inputs['email'];
        $inputs['pass_code'] = $inputs['password'];
        $inputs['password'] = Hash::make($inputs['password']);
        
        unset($inputs['register_otp']);
        Staff::create($inputs);
        $verifiedNumber->delete();
        $loginToken = $this->loginAfterRegister($loginDetails);

        $deviceID = request( ['device_id'] );
        $notification_token = request( ['notification_token'] );
        $customer_id = auth()->guard('customer-api')->user()->id;
        
        if( $deviceID )
        {
            $customer = Staff::find( $customer_id );
            $customer->device_id = $deviceID['device_id'];
            $customer->notification_token = $notification_token['notification_token'];
            $customer->save();
        }
        
        // if (array_key_exists('email', $inputs))
        //     Mail::to($inputs['email'])->queue(new WelcomeCustomer($inputs));
        
        return new LoginResource(['access_token' => $loginToken, 'message' => 'Customer created & Logged in successfully.', 'dev_message' => 'Customer created & Logged in successfully.']);
    }

    // public function loginAfterRegister($credentials) 
    // {
    //     if (! $token = auth()->guard('customer-api')->attempt($credentials))
    //         return response()->json(['error' => 'Unauthorized'], 401);
        
    //     return $token;
    // }

    // public function changeProfileDetails(UpdateCustomerRequest $request)  
    // {
    //     $inputs = $request->validated();
    //     auth()->guard('customer-api')->user()->update($inputs);
    //     return response()->json( ['data' => ['message' => 'Your profile details updated successfully.', 'dev_message' => 'Your profile details updated successfully.', 'customer' => auth()->guard('customer-api')->user() ] ] );
    // }

    // public function changePassword(ChangePasswordRequest $request) 
    // {
    //     $customer = auth()->guard('customer-api')->user();
    //     if (Hash::check($request->old_password, $customer->password)) {
    //         $customer->password = Hash::make($request->password);
    //         $customer->pass_code = $request->password;
    //         $customer->save();
    //         return response()->json(['data' => ['message' => 'Password updated successfully.', 'dev_message' => 'Password updated successfully.']], 200); 
    //     }

    //     throw ValidationException::withMessages(['old_password' => 'Old password does not match with your existing password,please try again.']);
    // }

    /**
     * @deprecated
     * @param MobileVerificationRequest $request
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse
     */
    // public function verifyMobileForForgotPassword(MobileVerificationRequest $request) 
    // {
    //     $inputs = $request->validated();
    //     $userExistWithThisNumber = Customer::where('mobile_number', $inputs['mobile_number'])->first();
    //     if (!$userExistWithThisNumber)
    //         return response(['error' => ['message' => 'This mobile number is not register with us', 'dev_message' => 'This mobile number is not register with us']], 409);
        

    //     MobileVerification::where('mobile_number', $inputs['mobile_number'])->delete();
    //     $inputs['code'] = mt_rand(1000,9999);
    //     $inputs['customer_id'] = $userExistWithThisNumber->id;
    //     MobileVerification::create($inputs);
    //     try {
    //         $message = 'Thanks to be a part of our villezone family, Your OTP for forgot password is: '.$inputs['code'];
    //         $this->sendMessage($inputs['mobile_number'], $message);
    //     } catch (\Exception $e) {
    //         Log::error($e);
    //         return response(['error' => ['message' => 'Something went wrong, please try again.', 'dev_message' => 'Something went wrong, please try again.']], 500);
    //     }

    //     return response()->json(['data' => ['message' => 'OTP sent successfully.', 'dev_message' => 'OTP sent successfully.']], 200);
    // }

    /**
     * @deprecated
     * @param CodeVerificationRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    // public function verifyCodeForForgotPassword(CodeVerificationRequest $request) {
    //     $inputs = $request->validated();
    //     $mobileNumberVerify = $this->verifyMobileVerificationCodeForForgotPassword($inputs['mobile_number'], $inputs['code']);
        
    //     if ($mobileNumberVerify)
    //         return response()->json(['data' => ['message' => 'OTP verified successfully.', 'dev_message' => 'OTP verified successfully.']], 200);
        
    //     return response(['error' => ['message' => 'OTP incorrect. Please try again.', 'dev_message' => 'OTP incorrect. Please try again.']], 422);
    // }

    /**
     * @deprecated
     * @param ForgotPasswordRequest $request
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse
     */
    // public function updateForgotPassword(ForgotPasswordRequest $request) 
    // {
    //     $inputs = $request->all();
    //     $verifiedNumber = MobileVerification::where('mobile_number', $inputs['mobile_number'])->where('verified', 1)->whereNotNull('customer_id')->first();

    //     if (!$verifiedNumber)
    //         return response(['error' => ['message' => 'Please verify your mobile number first.', 'dev_message' => 'Please verify your mobile number first.']], 403);
        
    //     $customer = Customer::findOrFail(2);
    //     $customer->password = Hash::make($inputs['password']);
    //     $customer->pass_code = $inputs['password'];
    //     $customer->save();
    //     $verifiedNumber->delete();
    //     return response()->json(['data' => ['message' => 'Your password updated Successfully.', 'dev_message' => 'Your password updated Successfully.']], 200);
    // }

    // public function verifyMobileForUpdateMobileNumber(MobileVerificationRequest $request) 
    // {
    //     $inputs = $request->validated();
    //     $userExistWithThisNumber = Customer::where('mobile_number', $inputs['mobile_number'])->first();
    //     if ($userExistWithThisNumber)
    //         return response(['error' => ['message' => 'This mobile number is already registered with us.', 'dev_message' => 'This mobile number is already registered with us.']], 409);
        
    //     MobileVerification::where('mobile_number', $inputs['mobile_number'])->delete();
    //     $inputs['code'] = mt_rand(1000,9999);
    //     $inputs['customer_id'] = Auth::user()->id;
    //     MobileVerification::create($inputs);
    //     try {
    //         $message = 'Thanks to be a part of our villezone family, Your OTP for update mobile number is: '.$inputs['code'];
    //         $this->sendMessage($inputs['mobile_number'], $message);
    //     } catch (\Exception $e) {
    //         Log::error($e);
    //         return response(['error' => ['message' => 'Something went wrong, please try again.', 'dev_message' => 'Something went wrong, please try again.']], 500);
    //     }

    //     return response()->json(['data' => ['message' => 'OTP sent successfully.', 'dev_message' => 'OTP sent successfully.']], 200);
    // }

    // public function verifyCodeForUpdateMobileNumber(CodeVerificationRequest $request) {
    //     $inputs = $request->validated();
    //     $mobileNumberVerifyRecord = $this->verifyMobileVerificationCodeForUpdateMobileNumber($inputs['mobile_number'], $inputs['code']);
        
    //     if ($mobileNumberVerifyRecord) {
    //         Auth::user()->update(['mobile_number' => $mobileNumberVerifyRecord->mobile_number]);
    //         $mobileNumberVerifyRecord->delete();
    //         return response()->json(['data' => ['message' => 'Your mobile number updated successfully.', 'dev_message' => 'Your mobile number updated successfully.']], 200);
    //     }
    //     return response(['error' => ['message' => 'OTP incorrect. Please try again.', 'dev_message' => 'OTP incorrect. Please try again.']], 422);
    // }
    
    // public function resendOTP( Request $request)
    // {
    //     $otp = getField( "mobile_verification", "mobile_number", "code", $request->mobile_number);
    //     try 
    //     {
    //         $message = 'Thanks to be a part of our villezone family, Your OTP for resend is: '.$otp;
    //         $this->sendMessage( $request->mobile_number, $message );
    //     }catch (\Exception $e) {
    //         Log::error($e);
    //         return response(['error' => ['message' => 'Something went wrong, please try again.', 'dev_message' => 'Something went wrong, please try again.']], 500);
    //     }
    //     return response()->json(['data' => ['message' => 'OTP sent successfully.', 'dev_message' => 'OTP sent successfully.']], 200);
    // }
    
    /**
     * 
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    // public function getCustomerAddressList()
    // {
    //     $customer = auth()->guard('customer-api')->user();//Auth::user();
    //     return ch_getCustomerAddress( $customer->id );
    // }
    
    /**
     * 
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    // public function submitCustomerAddress( Request $request )
    // {
    //     $customer = auth()->guard('customer-api')->user();//Auth::user();
    //     return ch_saveCustomerAddress( $request, $customer->id, $request->id );
    // }

    /**
     *
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    // public function deleteCustomerAddress( $id )
    // {
    //     return ch_deleteCustomerAddress( $id );
    // }
    
    /**
     * 
     * @param $id
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory
     */
    // public function defaultCustomerAddress($id){
    //     $customer = auth()->guard('customer-api')->user();//Auth::user();
    //     CustomerAddress::where('customer_id',$customer->id)->update(['is_default' => 0]);
    //     CustomerAddress::find($id)->update(['is_default' => 1]);
    //     return response(['data' => ['message' => 'Address changed successfully.', 'dev_message' => 'Address changed successfully.' , 'address' => CustomerAddress::find($id)]],200);
    // }
    
    /**
     * Update User Profile Picture
     */
    // function updateProfilePicture(Request $request, $id)
    // {
    //     $request->validate(['file' => 'required |mimes:jpg,jpeg,png | max:5120']);
    //     /* Getting file name */
    //     $filename = $_FILES['file']['name'];
        
    //     /* Location */
    //     $location = "public_html/images/customer/".$filename;
    //     $imageFileType = pathinfo($location,PATHINFO_EXTENSION);
        
    //     /* Valid Extensions */
    //     $valid_extensions = array("jpg","jpeg","png");
        
    //     /* Check file extension */
    //     if( in_array(strtolower($imageFileType),$valid_extensions) )
    //     {
    //         /* Upload file */
    //         if(move_uploaded_file($_FILES['file']['tmp_name'],$location))
    //         {
    //             $customer = Customer::find( $id );
    //             $customer->profile_pic = $location;
    //             $customer->save();
                
    //             return response()->json(['message' => 'Profile picture updated successfully.', 'dev_message' => 'Profile picture updated successfully.', 'fileName' => $location]);
    //         }
    //         else
    //             return response()->json(['message' => 'Profile picture not updated yet, please try again.', 'dev_message' => 'Profile picture not updated yet, please try again.', 'fileName' => ""]);
    //     }
    //     else
    //         return response()->json(['message' => 'Profile picture not updated yet, please try again.', 'dev_message' => 'Profile picture not updated yet, please try again.', 'fileName' => ""]);
            
    //     return;
            
    //     try{
    //         /* Getting file name */
    //         $file = $request->file('file');
    //         $customer = Customer::find($request['id']);
    //         if (file_exists(public_path(str_replace('public_html/','',$customer->profile_pic))))
    //             unlink(public_path(str_replace('public_html/','',$customer->profile_pic)));
            
    //         $filename = time() . '.' . $file->getClientOriginalExtension();
    //         /* Location */
    //         $location = "public_html/images/customer/".$filename;
    //         $uploadOk = 1;
    //         $imageFileType = $file->getClientOriginalExtension();
            
    //         /* Valid Extensions */
    //         $valid_extensions = array("jpg","jpeg","png");
            
    //         /* Check file extension */
    //         if( !in_array(strtolower($imageFileType),$valid_extensions) )
    //             $uploadOk = 0;
            
    //         if($uploadOk == 0)
    //             return response(['error' => ['message' =>'Invalid image type.', 'dev_message' =>'Invalid image type.']], 500);
    //         else
    //         {
    //             /* Upload file */
    //             if($file->move(public_path('images/customer/'), $filename)){
    //                 $img = 'public_html/images/customer/'.$filename;
    //                 $customer->update(['profile_pic' => $img]);
    //                 return response(['data' => ['message' => 'Profile picture updated successfully.', 'dev_message' => 'Profile picture updated successfully.' , 'image' => $location]],200);
    //             }
    //             else
    //                 return response(['data' => ['message' => 'Something went wrong, please try again.', 'dev_message' => 'Something went wrong, please try again.' , 'image' => $location]],400);
    //         }
    //     }catch (\Exception $e) {
    //         Log::error($e);
    //         return response(['error' => ['message' => $e->getMessage(), 'dev_message' => $e->getMessage()]], 500);
    //     }
    // }
    
    /**
     * 
     */
    // function getNotificationList()
    // {
    //     $customer = auth()->guard('customer-api')->user();
    //     return ch_getNotificationList( $customer->id );
    // }
    
    /**
     * 
     */
    // function updateNotification( $id )
    // {
    //     return ch_updateNotification( $id );
    // }

    // public function getCustomerData()
    // {
    //     $customer = auth()->guard('customer-api')->user();
    //     return response( [ 'data' => ['user' => $customer]] );
    // }
}
