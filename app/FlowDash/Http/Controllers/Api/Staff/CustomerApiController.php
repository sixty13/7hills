<?php

namespace App\FlowDash\Http\Controllers\Api\staff;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CustomerApiController extends Controller
{
    public function store(Request $request) {
        
        $validate = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
        ]);
        if ($validate->fails()) 
        {
            return response()->json($validate->errors());
        }

        $customer = new Customer;

        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->email = $request->email;
        $customer->mobile_no = $request->phone_number;
        $customer->address = $request->address;
        $customer->sid = $request->sid;
        $customer->save();

        $cid = $customer->id;

        return response(['message' => 'customer created successfully', 'cid' => $cid ], 200);
    }
}
