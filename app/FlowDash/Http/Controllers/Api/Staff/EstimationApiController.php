<?php
namespace App\FlowDash\Http\Controllers\Api\staff;

use App\Http\Controllers\Controller;
use App\Models\Quotation;
use App\Models\Customer;
use App\Models\GST;
use App\Models\Links;
use App\Models\QuotationDetails;
use Illuminate\Http\Request;
// use App\Traits\SendSMSAndVerifyOTP;
use URL;

class EstimationApiController extends Controller
{
    // use SendSMSAndVerifyOTP;

    public function calculation( Request $request ) {
        $settings = json_decode($request->arr,true);
        $settings = $settings['estimateArr'];
        
        $total = 0;
        $sub_total = 0;
        $discount = $settings['discount'];
        $cgst = GST::select('percentage')->where('id', 1)->first()->percentage;
        $sgst = GST::select('percentage')->where('id', 2)->first()->percentage;
        $igst = GST::select('percentage')->where('id', 3)->first()->percentage;

        $quotation = new Quotation();

        $quotation->cid = $settings['cid'];
        $quotation->sid = $settings['staff_id'];
        $quotation->total = 0;
        $quotation->sub_total = 0;
        $quotation->cgst = 0;
        $quotation->sgst = 0;
        $quotation->igst = 0;
        $quotation->payable_amount = 0;
        $quotation->discount = $discount;
        $quotation->product = $settings['p_type'];
        $quotation->special_discount = 0;
        $quotation->pdf = $settings['cid'].time().'.pdf';
        $quotation->image = $settings['product_image'];
        $quotation->save();

        $qid = $quotation->id;
        $carving = $height = $depth = $color = $width = $additionalCost = $discount_amount = $newTotal = 0;

        
        $settingArr = $settings['data'];
        
        foreach($settingArr as $k=>$ar) {
            $quotationDetails = new QuotationDetails();
            if( $ar['stype']  == 0 || $ar['stype']  == "0" )
            {
                if($ar['subsetting']['sid'] == 1) {
                    $carving = $ar['subsetting']['ssprice'];
                    $total += $carving;
                }

                if($ar['subsetting']['sid'] == 2) {
                    $height = $ar['subsetting']['ssprice'];
                    $total += $height;
                }

                if($ar['subsetting']['sid'] == 3) {
                    $backheight = $ar['subsetting']['ssprice'];
                    $total += $backheight;
                }

                if($ar['subsetting']['sid'] == 4) {
                    $width = $ar['subsetting']['ssprice'];
                    $total += $width;
                }

                if($ar['subsetting']['sid'] == 5) {
                    $depth = $ar['subsetting']['ssprice'];
                    $total += $depth;
                }   

                if($ar['subsetting']['sid'] == 10) {
                    $color = $ar['subsetting']['ssprice'];
                    $total += $color;
                }

                if($ar['subsetting']['sid'] == 15) {
                    $seatCover = $ar['subsetting']['ssprice'];
                    $total += $seatCover;
                }

                if($ar['subsetting']['sid'] == 21) {
                    $coverMaterial = $ar['subsetting']['ssprice'];
                    $total += $coverMaterial;
                }

                $quotationDetails->qid = $qid;
                $quotationDetails->setting = $ar['subsetting']['sname'];
                $quotationDetails->sid = $ar['subsetting']['sid'];
                $quotationDetails->sub_setting = @$ar['subsetting']['ssname'];
                $quotationDetails->ssid = $ar['subsetting']['ssid'] ?? 0;
                $quotationDetails->price = $ar['subsetting']['ssprice'];

                $quotationDetails->save();
            }

            // setting type = 5
            if( $ar['stype']  == 5  || $ar['stype']  == "5") {
                $additionalCost = $ar['subsetting']['additional_detail'];
                $total += $additionalCost;
                $quotationDetails->qid = $qid;
                $quotationDetails->setting = $ar['subsetting']['sname'];
                $quotationDetails->sid = $ar['subsetting']['sid'];
                $quotationDetails->price = $additionalCost;
                $quotationDetails->save();
            }

            // echo $total; die;
            // setting type = 1
            if( $ar['stype']  == 1 || $ar['stype']  == "1") {
                $total += $ar['subsetting']['sprice'];
                $quotationDetails->qid = $qid;
                $quotationDetails->setting = $ar['subsetting']['sname'];
                $quotationDetails->sid = $ar['subsetting']['sid'];
                $quotationDetails->sub_setting = @$ar['subsetting']['ssname'];
                $quotationDetails->ssid = $ar['subsetting']['ssid'] ?? 0;
                $quotationDetails->price = $ar['subsetting']['sprice'];
                $quotationDetails->save();
            }

            // setting type = 3
            if(  $ar['stype']  == 3 ||  $ar['stype']  == "3" ) 
            {
                $total += $ar['subsetting']['ssprice'];
                $quotationDetails->qid = $qid;
                $quotationDetails->setting = $ar['subsetting']['sname'];
                $quotationDetails->sid = $ar['subsetting']['sid'];
                $quotationDetails->price = $ar['subsetting']['ssprice'];
                $quotationDetails->additional_detail = $ar['subsetting']['additional_detail'];
                $quotationDetails->save();
            }

            //setting type = 2 4
            if( $ar['stype']  == 2 || $ar['stype']  == 4 || $ar['stype']  == "2" || $ar['stype']  == "4" ) {
                $calc = 0;

                if($ar['subsetting']['sid'] == 9) {
                    $calc = $carving + $height;
                }

                else if($ar['subsetting']['sid'] == 11)  {
                    $calc = $carving + $height + $color;
                }

                else if( $ar['subsetting']['sid'] == 19 || $ar['subsetting']['sid'] == 17 || $ar['subsetting']['sid'] == 18 ) {
                    $calc = $height + $width;
                }

                else if( $ar['subsetting']['sid'] == 13 || $ar['subsetting']['sid'] == 14) {
                    $calc = $width + $depth;
                }

                $total += $calc;
                $quotationDetails->qid = $qid;
                $quotationDetails->setting = $ar['subsetting']['sname'];
                $quotationDetails->sid = $ar['subsetting']['sid'];
                $quotationDetails->sub_setting = @$ar['subsetting']['ssname'];
                $quotationDetails->ssid = $ar['subsetting']['ssid'] ?? 0;
                $quotationDetails->price = $calc;
                $quotationDetails->save();
            }

            //setting type = 6
            if( $ar['stype']  == 6 || $ar['stype']  == "6" ) {

                $quotationDetails->qid = $qid; 
                $quotationDetails->setting = $ar['subsetting']['sname'];
                $quotationDetails->sid = $ar['subsetting']['sid'];
                $quotationDetails->additional_detail = $ar['subsetting']['additional_detail'];
                $quotationDetails->save();
            }

            //setting type = 7
            if( $ar['stype']  == 7 || $ar['stype']  == "7" ) {
                $calc = 0;
                $calc = ($ar['subsetting']['additional_detail'] * $ar['subsetting']['sprice']);

                $total += $calc;
                $quotationDetails->qid = $qid;  
                $quotationDetails->setting = $ar['subsetting']['sname'];
                $quotationDetails->sid = $ar['subsetting']['sid'];
                $quotationDetails->price = $calc;
                $quotationDetails->additional_detail = $ar['subsetting']['additional_detail'];
                $quotationDetails->save();
            }
         
        }
        
        //calc
        $quotation = Quotation::where('id', $qid)->first();
        $customer  = Customer::where( 'id', $quotation->cid )->first();
        $mobile = $customer->mobile_no;
        $pdf = $quotation->pdf;
        $discount_amount = (((float)$total * (float)$discount) / 100);
        $sub_total = $total - $discount_amount;
        $cgst_amount = (((float)$sub_total * (float)$cgst) / 100);
        $sgst_amount = (((float)$sub_total * (float)$sgst) / 100);
        $igst_amount = (((float)$sub_total * (float)$igst) / 100);
        $payable_amount = $sub_total + $cgst_amount + $sgst_amount + $igst_amount;
        
        $quotation->total = $total;
        $quotation->sub_total = $sub_total;
        $quotation->cgst = $cgst_amount;
        $quotation->sgst = $sgst_amount;
        $quotation->igst = $igst_amount;
        $quotation->payable_amount = $payable_amount;
        $quotation->save();
        
        commonDownloadQuotation($qid);
        
        sendMail($qid);

        $link = $this->shorten($qid, $pdf);
        $msg = "Dear customer, here is your quotation for refurbishing your old furniture with 7Hills Furniture Refurbishing company. ".$link;
        sendMobileSMS($mobile, $msg);

        return response()->json(["message" => 'PDF generated successfully.'], 200);
    }
    // commonDownloadQuotation($qid);

    //     return response()->json(["message" => 'Calculation done successfully.', "quotation" => $quotation->payable_amount, "qid" => $qid, "pdf" => $pdf, "mobile" => $mobile], 200);
    // }

    // public function sendSMSMail($qid, $pdf, $mobile) {

    //     sendMail($qid);

    //     $link = $this->shorten($qid, $pdf);
    //     $msg = "Dear Customer, Here is your quotation for refurbishing your old furniture with 7Hill Furniture Refurbishing company. ".$link;

    //     $this->shortURLSendMessage($mobile, $msg);

    //     return response()->json(["message" => 'PDF generated successfully.'], 200);
    // }

    public function shorten($id, $pdf) {
        $n = 8;
        $characters = '3qwertyu52iopasdfghjklmn6bvcxz7ZAQWSXCD0ERFVBGTYHN4MJUIKLOP189';
        $randomString = '';

        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        $originalLink = 'http://7hills.peacockindia.in/storage/app/public/reports/'.$pdf;
        $newLink = 'http://7hills.peacockindia.in/'.$randomString;

        // $insert = DB::table('links')->insert([
        //     'original_link' => $originalLink,
        //     'short_link' => $newLink
        // ]);
        $insert = new Links();
        $insert->qid = $id;
        $insert->original_link = $originalLink;
        $insert->short_link = $newLink;
        $insert->save();

        return $newLink;
        // try {
        //     if($insert == false) {
        //         throw New Exception("Error Occurred! Try Again.");
        //     } else {
        //         $JSON_string['status'] = 800;
        //         $JSON_string['msg'] = "$newLink";
        //         return response($JSON_string);
        //     }
        // } catch(Exception $e){
        //     $Json_Data['status'] = 801;
        //     $Json_Data['msg'] = $e->getMessage();
        // }
    }

    public function shortURLSendMessage($mobile, $msg)
    {
        return sendMobileSMS( $mobile, $msg );
        // $curl = curl_init();
        // curl_setopt_array($curl, array(
        // CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?authkey=326374AKdXHEwpoNWi5fcb6d08P1&mobiles=".$mobile."&country=91&message=".$msg."&sender=TAMIRA&",
        // CURLOPT_RETURNTRANSFER => true,
        // CURLOPT_ENCODING => '',
        // CURLOPT_MAXREDIRS => 10,
        // CURLOPT_TIMEOUT => 0,
        // CURLOPT_FOLLOWLOCATION => true,
        // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        // CURLOPT_CUSTOMREQUEST => 'GET',
        // CURLOPT_HTTPHEADER => array(
        //     'Cookie: AWSALB=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA; AWSALBCORS=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA'
        // ),
        // ));

        // $response = curl_exec($curl);
        
        // curl_close($curl);
    }

    public function imageStore( Request $request ) {
        if($request->hasFile('product_image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('product_image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('product_image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('product_image')->storeAs('public/product', $fileNameToStore);
        }
        else{
            $fileNameToStore = 'noimage.jpg';
        }

        return response()->json(["message" => 'image uploaded successfully', "product_image" => $fileNameToStore], 200);
    }
}