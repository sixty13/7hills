<?php

namespace App\FlowDash\Http\Controllers\Api\staff;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Settings;
use App\Models\Staff;
use Illuminate\Support\Facades\Auth;
use App\Models\SubSettings;
use Illuminate\Support\Facades\DB;

class SettingsApiController extends Controller
{
    public function show() {

        // dd(auth()->guard('staff-api')->user()->id);
        $settingArr = Settings::with('subsettings')->where( ['active' => 1] )->get();
        // $settingArr = [];
        // foreach( $setting as $k=>$ar )
        // {
        //     $settingArr[] = $ar;
        //     $sub_setting = SubSettings::where('setting_id', $ar->id)->get();
        //     if( !empty( $sub_setting ) )
        //     {
        //         foreach( $sub_setting as $j=>$vr )
        //         {
        //             $settingArr[][] = $vr;
        //         }
        //     }
        // }
        return response()->json( [ "result" => $settingArr ] );
    }
}

// ALTER TABLE `settings` ADD `setting_type` TINYINT(1) NOT NULL COMMENT '0: with subsettings and price\r\n1: with default price\r\n2: subsettings with name only\r\n3: subsettings with name, price and text box \r\n4: with no subsetting or default price \r\n5: textbox 6: textarea' AFTER `price`; 
// UPDATE `settings` SET `setting_type` = '0' WHERE `settings`.`id` = 1; 
// UPDATE `settings` SET `setting_type` = '0' WHERE `settings`.`id` = 2; 
// UPDATE `settings` SET `setting_type` = '0' WHERE `settings`.`id` = 3; 
// UPDATE `settings` SET `setting_type` = '0' WHERE `settings`.`id` = 4; 
// UPDATE `settings` SET `setting_type` = '0' WHERE `settings`.`id` = 5; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 6; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 7; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 8; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 9; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 10; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 11; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 12; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 13; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 14; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 15; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 16; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 17; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 18; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 19; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 20; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 21; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 22; 
// UPDATE `settings` SET `setting_type` = '1' WHERE `settings`.`id` = 23; 
