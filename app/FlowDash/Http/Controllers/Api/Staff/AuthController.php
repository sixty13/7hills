<?php

namespace App\FlowDash\Http\Controllers\Api\staff;

use App\Http\Controllers\Controller;
use App\Http\Resources\LoginResource;
use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;
// use App\Http\Resources\LoginResource;
// use App\Http\Resources\AccountResource;
// use App\Http\Resources\MeResource;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
// use App\Models\Guest_members;
// use App\Models\Customer;
use Facebook\WebDriver\Cookie;

class AuthController extends Controller
{
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['mobile_number', 'password']);
        if (!$token = auth()->guard('staff-api')->attempt($credentials)){
            return response()->json(['error' => ['message' => 'Invalid mobile number or password combination.', 'dev_message' => 'Invalid mobile number or password combination.']], 200);
        }
        
        $deviceID = request( ['device_id'] );
        $notification_token = request( ['notification_token'] );
        $customer_id = auth()->guard('staff-api')->user()->id;
        
        return new LoginResource( ['access_token' => $token, 'message' => 'Logged in successfully.', 'currentLoginUser' => auth()->guard('staff-api')->user()] );
    }

    // public function adminLogin()
    // {
    //     $credentials = request(['mobile_number', 'password']);

    //     if (!$token = auth()->guard('api')->attempt($credentials))
    //         return response()->json(['error' => ['message' => 'Invalid mobile number or password combination.', 'dev_message' => 'Invalid mobile number or password combination.']], 200);
        
    //     return response(['access_token' => $token, 'message' => 'Logged in successfully.', 'dev_message' => 'Logged in successfully.', 'currentLoginUser' => auth()->guard('api')->user()]);
    // }

    // /**
    //  * Get the authenticated User.
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function me(Request $request)
    // {
    //     $currentLoginUser =  auth()->guard('customer-api')->user();

    //     if ($request->has('device_id') && $request->device_id) {
    //         $currentLoginUser->device_id = $request->device_id;
    //         $currentLoginUser->save();
    //     }

    //     return new MeResource(['currentLoginUser' => $currentLoginUser]);
    // }

    // /**
    //  * Log the user out (Invalidate the token).
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    public function logout()
    {
		auth()->logout();
        return response()->json(['message' => 'Logged out successfully.'], 200);
    }

    // /**
    //  * Refresh a token.
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // public function refresh()
    // {
    //     return $this->respondWithToken(auth()->refresh());
    // }

    // /**
    //  * Get the token array structure.
    //  *
    //  * @param  string $token
    //  *
    //  * @return \Illuminate\Http\JsonResponse
    //  */
    // protected function respondWithToken($token)
    // {
    //     return response()->json([
    //         'access_token' => $token,
    //         'token_type' => 'bearer',
    //     ]);
    // }
}
