<?php

namespace App\FlowDash\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    public function updateAccount(Request $request) {

        $id = auth()->user()->id;
        $user = USER::where('id', $id)->first();
        // dd($request->all());
        if($request->has('old_password') && $request->has('password')) {
            if (Hash::check($request->old_password, $user->password)) {
                $user->password = Hash::make($request->password);
                $user->save();
                
                return redirect('/home')->with('status', 'Profile updated successfully.'); 
            }
        } else {
            throw ValidationException::withMessages(['old_password' => 'Password can not be empty, please try again.']);
        }

            
        throw ValidationException::withMessages(['old_password' => 'Old password doesn not match with your existing password, please try again.']);
    }
}
