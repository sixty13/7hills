<?php

namespace App\FlowDash\Http\Controllers;

use App\Http\Controllers\Controller;
use App\FlowDash\Http\Middleware\SetDefaultLayoutForUrls;
use Illuminate\Http\Request;
use App\Models\Settings;
use App\Models\SubSettings;
use App\Models\GST;
use DB;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subSettings = SubSettings::all();
        $settings = Settings::all();
        $gst = GST::all();
        return view('vendor.flowdash.settings.index', compact('subSettings', 'settings', 'gst') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->gst_id) {
            // echo "<pre>";
            // print_r($request->all());
            // die;
            $gst = GST::find($request->gst_id);
            $gst->percentage = $request->percent;
            if( !$request->has('checkbox') )
                $gst->active = 0;
            else $gst->active = 1;

            $gst->save();
        }

        if(!empty($request->name)) {
            $setting = Settings::find($request->parent_id);
            if( !$request->has('checkbox') )
                $setting->active = 0;
            else $setting->active = 1;

            $setting->selection = 0;
            if($request->inlineFormRole == "multiple")
                $setting->selection = 1;

            $setting->save();

            for($i = 0; $i < count($request->name); $i++) {
                if( isset($request->id) && ( $i + 1 ) <= count($request->id) ) {
                    if(DB::table('subsettings')->where('name', $request->name)){
                        $subSetting = SubSettings::find($request->id[$i]);
                    }
                }
                else {
                    $subSetting = new SubSettings();
                }
                if(!empty($request->name[$i]))
                {
                    if($request->parent_id == 23)
                    $subSetting->name = $request->name[$i]."+";
                    else
                    $subSetting->name = $request->name[$i];

                    $subSetting->price = $request->price[$i];
                    $subSetting->setting_id = $request->parent_id;
                    $subSetting->save();
                }
            }
        }
        else if( empty($request->name) ) {
            $setting = Settings::find($request->parent_id);
            if( !empty($request->price) ) {
                $setting->price = $request->price;
            }

            if( !$request->has('checkbox') )
                $setting->active = 0;
            else $setting->active = 1;

            $setting->save();
        }

        SubSettings::where(['setting_id' => $request->parent_id, 'delete_at' => 1])->delete();

        return response()->json([ 'status' => 200, 'msg' => 'Service updated successfully.']);
    }

        /**
     * @Function:        <startQueryLog>
     * @Author:          Gautam Kakadiya
     * @Created On:      <27-02-2020>
     * @Last Modified By:
     * @Last Modified:
     * @Description:     <This methode start query log>
     */
    function startQueryLog()
    {
        DB::enableQueryLog();
    }

    /**
     * @Function:        <displayQueryResult>
     * @Author:          Gautam Kakadiya
     * @Created On:      <27-02-2020>
     * @Last Modified By:GAUTAM KAKADIYA
     * @Last Modified:
     * @Description:     <This methode create result array>
     */
    function displayQueryResult()
    {
        $query = DB::getQueryLog();
        pr($query);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function toDelete($id) {
        SubSettings::where('id', $id)->update(['delete_at' => 1]);
        return response()->json(['status' => 200, 'msg' => 'Temporarily moved to bin.']);
    }

    public function toRevert() {
        SubSettings::where('delete_at', 1)->update(['delete_at' => 0]);
    }
}
