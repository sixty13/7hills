<?php

namespace App\FlowDash\Http\Controllers;

use App\Http\Controllers\Controller;
use App\FlowDash\Http\Middleware\SetDefaultLayoutForUrls;
use Illuminate\Http\Request;
use App\Models\Staff;
use App\Models\GST;
use App\Models\QuotationDetails;
use App\Models\Quotation;
use App\Models\Customer;
use Dompdf\Dompdf;
use Dompdf\Options;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Pagination\Paginator;

class StaffsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Paginator::useBootstrap();
        $staffs = Staff::orderBy('created_at', 'desc')->paginate(10);
        return view('vendor.flowdash.users.index', compact('staffs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'designation' => 'required',
            'email' => 'required|email|unique:staff,email,'.$request->id,
            'mobile_no' => 'required',
            'allowed_discount' => 'required'
            ]); 
            
            $msg = "Staff created succesfully";

            if(  $request->id !== null ) {
                $staff = Staff::find( $request->id );
                $msg = "Staff updated succesfully";
            }
            else{
                $staff = new Staff();
            }
            
        // dd($request->all());
        $staff->first_name = $request->first_name;
        $staff->last_name = $request->last_name;
        $staff->designation = $request->designation;
        $staff->email = $request->email;
        $staff->mobile_number = $request->mobile_no;
        $staff->allowed_discount = $request->allowed_discount;
        $staff->save();

        return redirect('/user')->with('msg', $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = Staff::find($id);
        return response()->json(['staff' => $staff]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $staff = Staff::destroy($id);
        $msg = "Staff deleted successfully.";
        return redirect('/user')->with('msg', $msg);
    }

    public function downloadQuotationPDFRegenerate(Request $request, $id) {
        $quotation = Quotation::where('id', $id)->first();

        $cgst = GST::select('percentage')->where('id', 1)->first();
        $sgst = GST::select('percentage')->where('id', 2)->first();
        $igst = GST::select('percentage')->where('id', 3)->first();

        $disc_amount = (($quotation->sub_total * $request->sp_disc) / 100);
        $sub_total = ($quotation->sub_total - $disc_amount);
        $cgst_amount = (($sub_total * $cgst->percentage) / 100);
        $sgst_amount = (($sub_total * $sgst->percentage) / 100);
        $igst_amount = (($sub_total * $igst->percentage) / 100);

        $quotation->sub_total = $sub_total;
        $quotation->cgst = $cgst_amount;
        $quotation->sgst = $sgst_amount;
        $quotation->igst = $igst_amount;
        $quotation->special_discount += $request->sp_disc;
        $quotation->payable_amount = $sub_total + $cgst_amount + $sgst_amount + $igst_amount;
        $quotation->save();

        commonDownloadQuotation($id);

        return response()->json([ 'status' => 200, 'msg' => 'Special discount applied, PDF regenerated successfully.', 'discount' => $quotation->special_discount, 'payable_amount' => $quotation->payable_amount]);
    } 

    public function resendPDF($id) {
        sendMail($id);

        $quotation = Quotation::where('id', $id)->first();
        $quotation->resend = '1';
        $quotation->save();

        return response()->json(['status' => 200, 'msg' => 'PDF resend successfully.', 'resend' => 'RESEND' ]);
    }
}
