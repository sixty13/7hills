<?php

use App\Models\Quotation;
use App\Models\Customer;
use App\Models\QuotationDetails;
use Barryvdh\DomPDF\Facade as PDF;
use Dompdf\Dompdf;
use Dompdf\Options;
use Mailgun\Mailgun;
use App\Traits\SendSMSAndVerifyOTP;

if (!function_exists('title')) {
  function title($title = null)
  {
    if (!empty($title)) {
      return $title . " - " . config('flowdash.brand');
    }
    
    return config('flowdash.brand');
  }
}

if (!function_exists('activeClass')) {
  function activeClass($route, $activeClass = 'active')
  {
    return request()->routeIs($route) ? $activeClass : '';
  }
}
// use SendSMSAndVerifyOTP;

if (!function_exists('arrayToObject')) {
  function arrayToObject($d) {
    if (is_array($d)) {
      /*
      * Return array converted to object
      * Using __FUNCTION__ (Magic constant)
      * for recursive call
      */
      return (object) array_map(__FUNCTION__, $d);
    }
    else {
      // Return object
      return $d;
    }
  }
}

function commonDownloadQuotation($id=1) {
  $quotation = Quotation::with('customer', 'staff')->where( 'id', $id )->first();
  $rework = QuotationDetails::where(['qid'=> $id, 'sid' => '22'])->get();
  $repolish = QuotationDetails::where(['qid'=> $id, 'sid' => '11'])->get();
  $width = QuotationDetails::where(['qid'=> $id, 'sid' => '4'])->get();
  $depth = QuotationDetails::where(['qid'=> $id, 'sid' => '5'])->get();
  $height = QuotationDetails::where(['qid'=> $id, 'sid' => '2'])->get();

  // path to store pdf file
  $pdf_path = public_path('storage/reports/'.$quotation->pdf);

  // return view( 'vendor.flowdash.quotations.pdf', compact('quotation', 'rework', 'repolish', 'width', 'depth', 'height') );

  // die;
  $html_content = view( 'vendor.flowdash.quotations.pdf', compact('quotation', 'rework', 'repolish', 'width', 'depth', 'height') );

  // converting file into pdf and saving into directory
  PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])
  ->loadHtml($html_content)
  ->setPaper('a4', 'landscape')->save($pdf_path);

}

function sendMail($id)
{

  $quotation = Quotation::where( 'id', $id )->first();
  $customer  = Customer::where( 'id', $quotation->cid )->first();

  // First, instantiate the SDK with your API credentials
  $mg = Mailgun::create('72259a9852b4a2232b699f5c88f86663-985b58f4-6ed91756'); // For US servers
  // $mg = Mailgun::create('72259a9852b4a2232b699f5c88f86663-985b58f4-6ed91756', 'https://api.eu.mailgun.net'); // For EU servers

  // Now, compose and send your message.
  // $mg->messages()->send($domain, $params);
  $mg->messages()->send('peacockindia.in', [
    'from'    => '7hills@peacockindia.in',
    'to'      => $customer->email,
    'subject' => '7hills Inspection App',
    'text'    => ' Dear Customer, Here is your quotation for refurbishing your old furniture with 7Hill Furniture Refurbishing company.',
    'attachment' => [['filePath'=> public_path('storage/reports/'.$quotation->pdf), 'filename'=>$quotation->pdf]],
  ]);

}

function sendInvoiceSMS($id) {

  $quotation = Quotation::where( 'id', $id )->first();
  $customer  = Customer::where( 'id', $quotation->cid )->first();

  $mobile = $customer->mobile_no;
  $msg = "Dear Customer, Here is your quotation for refurbishing your old furniture with 7Hill Furniture Refurbishing company.<Short URL>";
  // $curl = curl_init();
  // curl_setopt_array($curl, array(
  // CURLOPT_URL => "http://trans.kapsystem.com/api/v5/index.php?method=sms&message=".$msg."&to=".$mobile."&sender=PEAIND&api_key=A5171e688fca5cb3f82d316cecaf3728d&entity_id=1601255160748540110",
  // CURLOPT_RETURNTRANSFER => true,
  // CURLOPT_ENCODING => '',
  // CURLOPT_MAXREDIRS => 10,
  // CURLOPT_TIMEOUT => 0,
  // CURLOPT_FOLLOWLOCATION => true,
  // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  // CURLOPT_CUSTOMREQUEST => 'GET',
  // CURLOPT_HTTPHEADER => array(
  //     'Cookie: AWSALB=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA; AWSALBCORS=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA'
  //   ),
  // ));

  // $response = curl_exec($curl);
  
  // curl_close($curl);
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => "http://trans.kapsystem.com/api/v5/index.php?method=sms&message=".$msg."&to=".$mobile."&sender=PEAIND&api_key=A5171e688fca5cb3f82d316cecaf3728d&entity_id=1601255160748540110",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_HTTPHEADER => array(
      'Cookie: AWSALB=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA; AWSALBCORS=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA'
    ),
  ));

  $response = curl_exec($curl);
  
  curl_close($curl);
}

function storeProductImage($id=1) {
  if($request->hasFile('cover_image')){
    // Get filename with the extension
    $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
    // Get just filename
    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
    // Get just ext
    $extension = $request->file('cover_image')->getClientOriginalExtension();
    // Filename to store
    $fileNameToStore= $filename.'_'.time().'.'.$extension;
    // Upload Image
    $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
  }
  else{
      $fileNameToStore = 'noimage.jpg';
  }
}

function sendMobileSMS( $mobile, $msg )
{
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_URL => "http://trans.kapsystem.com/api/v5/index.php?method=sms&message=".$msg."&to=".$mobile."&sender=PEAIND&api_key=A5171e688fca5cb3f82d316cecaf3728d&entity_id=1601255160748540110",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
    CURLOPT_HTTPHEADER => array(
      'Cookie: AWSALB=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA; AWSALBCORS=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA'
    ),
  ));

  $response = curl_exec($curl);
  
  curl_close($curl);
}