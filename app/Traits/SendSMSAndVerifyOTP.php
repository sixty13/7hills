<?php

namespace App\Traits;
use App\Models\MobileVerification;
use App\Models\User;
use Carbon\Carbon;
use App\Mail\MailOrderDetailToOpd;
use App\Models\Customer;
use Illuminate\Support\Facades\Mail;
use Mailgun\Mailgun;

trait SendSMSAndVerifyOTP
{
    protected function sendMessage($mobile, $msg)
    {
        // return sendMobileSMS( $mobile, $msg );
        // dd($mobile, $msg);
        $curl = curl_init();
        curl_setopt_array($curl, array(
        // CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?authkey=326374AKdXHEwpoNWi5fcb6d08P1&mobiles=".$mobile."&country=91&message=".$msg."&sender=TAMIRA&",
        CURLOPT_URL => "http://trans.kapsystem.com/api/v5/index.php?method=sms&message=".$msg."&to=".$mobile."&sender=PEAIND&api_key=A5171e688fca5cb3f82d316cecaf3728d&entity_id=1601255160748540110",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Cookie: AWSALB=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA; AWSALBCORS=wcqEBhMJufeqzfpN4Q92Z21NPl2jTRm1LTdGnQ8vMeSg7RRrmLORBogWk7GT+aoRm6nIx7v0AWfQkr4b0pQGPMGXk25ruKgicAWBhmyrc3aUx3WWKhLNY0GPKcEA'
        ),
        ));

        $response = curl_exec($curl);
        
        curl_close($curl);
    }

    protected function sendMail($email, $msg){
        // First, instantiate the SDK with your API credentials
        $mg = Mailgun::create('72259a9852b4a2232b699f5c88f86663-985b58f4-6ed91756'); // For US servers
        // $mg = Mailgun::create('72259a9852b4a2232b699f5c88f86663-985b58f4-6ed91756', 'https://api.eu.mailgun.net'); // For EU servers
        
        // Now, compose and send your message.
        // $mg->messages()->send($domain, $params);
        $mg->messages()->send('peacockindia.in', [
            'from'    => 'brad@peacockindia.in',
            'to'      => $email,
            'subject' => '7hills One Time Password',
            'text'    => $msg,
            // 'attachment' => ['filePath'=> public_path('storage/reports/'.$quotation->pdf), 'filename'=>$quotation->pdf],
            ]);
            
            // dd($mg);
    }
    
    public function checkMessageSendStatus( $job="" )
    {
        $userName = getField( 'configuration' , 'config_key', 'config_value', 'MSG_USER_NAME' );
        $password = getField( 'configuration' , 'config_key', 'config_value', 'MSG_USER_PASSWORD' );
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, "http://smslogin.pcexpert.in/api/mt/GetDelivery?user=$userName&password=$password&jobid=$job");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        
        $head = curl_exec($ch);
        curl_close($ch);
        
        return json_decode($head);
    }

    public function verifyMobileVerificationCode($mobileNumber, $verificationCode) {
        $verificationMobileRecord = MobileVerification::where('mobile_number', $mobileNumber)->where('code', $verificationCode)->where('verified', 0)->first();//->whereNull('customer_id');

        if (!$verificationMobileRecord) {
            return false;
        }

        $verificationMobileRecord->verified = 1;
        $verificationMobileRecord->save();
        return true;
    }

    public function verifyMobileVerificationCodeForForgotPassword($mobileNumber, $verificationCode) {
        $verificationMobileRecord = MobileVerification::where('mobile_number', $mobileNumber)->where('code', $verificationCode)->where('verified', 0)->whereNotNull('customer_id')->first();

        if (!$verificationMobileRecord) {
            return false;
        }

        $verificationMobileRecord->verified = 1;
        $verificationMobileRecord->save();
        return true;
    }

    public function verifyMobileVerificationCodeForUpdateMobileNumber($mobileNumber, $verificationCode) {
        $verificationMobileRecord = MobileVerification::where('mobile_number', $mobileNumber)->where('code', $verificationCode)->where('verified', 0)->where('customer_id', auth()->guard('customer-api')->user()->id)->first();

        if (!$verificationMobileRecord) {
            return false;
        }

        $verificationMobileRecord->verified = 1;
        $verificationMobileRecord->save();
        return $verificationMobileRecord;
    }

    public function sendOrderStatusMessagesAndMail($order) 
    {
        if ($order->order_status == 'accept') 
        {
            $this->sendOrderAcceptNotifications($order);
        } 
        else if ($order->order_status == 'packaging') 
        {
            $this->sendOrderPackagingNotifications($order);
        }
        else if ($order->order_status == 'out_for_delivery') 
        {
            $this->sendOrderOutForDeliveryNotifications($order);
        } 
        else if ($order->order_status == 'delivered') 
        {
            $this->sendOrderDeliveredNotifications($order);
        }
        else if ($order->order_status == 'canceled') 
        {
            $this->sendOrderCanceledNotifications($order);
        }

        return true;
    }

    public function sendOrderAcceptNotifications($order) 
    {
        $messageForCustomer = 'Your order from Villezone with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been accepted. You can expect delivery by '.Carbon::parse($order->expected_delivery_date)->format('d-M-Y').' between '.$order->delivery_time_slot.'. To track your order status go to Villezone application > My Orders';

        $messageForManagement = 'Order with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been accepted. expect delivery by '.Carbon::parse($order->expected_delivery_date)->format('d-M-Y').' between '.$order->delivery_time_slot.'.';// For more information click here: '.config('villezone.website_url').'/operational-department/order/view/'.$order->id;

        $this->sendMessage($order->customer->mobile_number, $messageForCustomer);

        //send pushnotification on android mobile
        $deviceIds = Customer::where( 'id', $order->customer->id )->get()->pluck('notification_token')->toArray();//whereNotNull('notification_token')->
        $this->pushNotificationAndroid( 'Your order from Villezone with order id '.$order->order_key, $messageForCustomer, $deviceIds );

        $operationDepartment = User::where('user_group_id',8)->first();
        $this->sendMessage($operationDepartment->mobile_number, $messageForManagement);

    }

    public function sendOrderPackagingNotifications($order) 
    {
        $messageForCustomer = 'Your order from Villezone with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been processed. Delivered shortly to your door step. For Track your order status go to Villezone Application > My Orders';

        $messageForManagement = 'Order with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been processed. Please pick-up order on time, delivery date '.Carbon::parse($order->expected_delivery_date)->format('d-M-Y').' & '.$order->delivery_time_slot.' time slot. For more information click here: '.config('villezone.website_url').'/delivery-boy/order/view/'.$order->id;

        $this->sendMessage($order->customer->mobile_number, $messageForCustomer);

        //send pushnotification on android mobile
        $deviceIds = Customer::where( 'id', $order->customer->id )->get()->pluck('notification_token')->toArray();//whereNotNull('notification_token')->
//         pr($deviceIds);die;
        $this->pushNotificationAndroid( 'Your order from Villezone with order id '.$order->order_key, $messageForCustomer, $deviceIds );

        $this->sendMessage($order->deliveryboy->mobile_number, $messageForManagement);
    }

    public function sendOrderOutForDeliveryNotifications($order) 
    {
        $messageForCustomer = 'Your order from Villezone with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been out for delivery. Contact our delivery person. Name: '.$order->deliveryboy->name.', Mobile No: '.$order->deliveryboy->mobile_number;

//         $messageForManagement = 'Order with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been out for delivery. For more information click here: '.config('villezone.website_url').'/admin/order/view/'.$order->id;

        $this->sendMessage($order->customer->mobile_number, $messageForCustomer);

        //send pushnotification on android mobile
        $deviceIds = Customer::where( 'id', $order->customer->id )->get()->pluck('notification_token')->toArray();//whereNotNull('notification_token')->
        $this->pushNotificationAndroid( 'Your order from Villezone with order id '.$order->order_key, $messageForCustomer, $deviceIds );

//         $admins = User::where('user_group_id', 1)->get();
//         foreach ($admins as $admin) {
//             $this->sendMessage($admin->mobile_number, $messageForManagement);
//         }
    }

    public function sendOrderDeliveredNotifications($order) 
    {
        $messageForCustomer = 'Your order from Villezone with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been delivered to you. Please give your precious feedback from Villezone application > My Orders, We are waiting for your next order. Thank you';

        $messageForManagement = 'Order with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been delivered to customer. For more information click here: ';

        $this->sendMessage($order->customer->mobile_number, $messageForCustomer);

        //send pushnotification on android mobile
        $deviceIds = Customer::where( 'id', $order->customer->id )->get()->pluck('notification_token')->toArray();//whereNotNull('notification_token')->
        $this->pushNotificationAndroid( 'Your order from Villezone with order id '.$order->order_key, $messageForCustomer, $deviceIds );

        $operationDepartment = User::where('user_group_id', 8)->first();
        if( !empty( $operationDepartment ) )
        {
            $this->sendMessage($operationDepartment->mobile_number, $messageForManagement.config('villezone.website_url').'/operational-department/order/view/'.$order->id);
            Mail::to($operationDepartment->email)->queue(new MailOrderDetailToOpd($order));
        }
    }

    public function sendOrderCanceledNotifications($order) 
    {
        $messageForCustomer = 'Your order from Villezone with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been canceled, for any order related query contact us from Villezone application > Contact Us, We are waiting for your next order. Thank you';

        $messageForManagement = 'Order with order id '.$order->order_key.' worth Rs. '.$order->total_with_delivery_charge.' has been canceled, For more information click here: ';

        $this->sendMessage($order->customer->mobile_number, $messageForCustomer);

        //send pushnotification on android mobile
        $deviceIds = Customer::where( 'id', $order->customer->id )->get()->pluck('notification_token')->toArray();//whereNotNull('notification_token')->
        $this->pushNotificationAndroid( 'Your order from Villezone with order id '.$order->order_key, $messageForCustomer, $deviceIds );
        
        $operationDepartment = User::where('user_group_id', 8)->first();
        if(! empty($operationDepartment))
            $this->sendMessage($operationDepartment->mobile_number, $messageForManagement.config('villezone.website_url').'/operational-department/order/view/'.$order->id);

//         $admins = User::where('user_group_id', 1)->get();
//         foreach ($admins as $admin) {
//             $this->sendMessage($admin->mobile_number, $messageForManagement.config('villezone.website_url').'/admin/order/view/'.$order->id);
//         }
    }

    function pushNotificationAndroid( $title="Notification", $message="", $deviceIds=array()){

        if( isEmptyArr( $deviceIds ) )
        {
            return true;
//             $deviceIds = Customer::whereNotNull('notification_token')->get()->pluck('notification_token')->toArray();
        }

        $url = 'https://fcm.googleapis.com/fcm/send';
    
        $apiKey = fireBaseServerKey();
            
        $fields = array (
            'registration_ids' => $deviceIds,
            'data' => array (
                "message" => $message,
                "title" => $title
            ),
            'notification' => array (
                "message" => $message,
                "title" => $title
            )
        );
    
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$apiKey
        );
                    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
    
    function pushAdminNotificationAndroid( $title="Notification", $message=""){
        
        $deviceIds = Customer::whereNotNull('notification_token')->get()->pluck('notification_token')->toArray();
        
        $url = 'https://fcm.googleapis.com/fcm/send';
        
        $apiKey = fireBaseServerKey();
        
        $fields = array (
            'registration_ids' => $deviceIds,
            'data' => array (
                "message" => $message,
                "title" => $title
            ),
            'notification' => array (
                "message" => $message,
                "title" => $title
            )
        );
        
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$apiKey
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
    
}
