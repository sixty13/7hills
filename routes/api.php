<?php

use App\FlowDash\Http\Controllers\Api\Staff\StaffApiController;
use App\FlowDash\Http\Controllers\Api\Staff\AuthController;
use App\FlowDash\Http\Controllers\Api\staff\EstimationApiController;
use App\FlowDash\Http\Controllers\Api\staff\SettingsApiController;
use App\FlowDash\Http\Controllers\SettingsController;
use App\FlowDash\Http\Controllers\Api\staff\CustomerApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'middleware' => 'api',
    'namespace' => 'Api\Staff'
], function ($router) {
    Route::group([
        'prefix' => 'auth'
    ], function ($router) {
        Route::post('login', [AuthController::class, 'login']);
        // Route::get('logout',[AuthController::class, 'logout']);
        Route::post('verify/user', [StaffApiController::class, 'verifyCustomer']);
        Route::post('verify/mobile-number', [StaffApiController::class, 'verifyMobile']);
        Route::post('verify/resend-otp', [StaffApiController::class, 'resendOTP']);
        // Route::post('create/customer', 'CustomerApiController@createCustomer');
        // Route::post('forgot/password/verify/mobile-number', 'CustomerApiController@verifyMobileForForgotPassword');
        // Route::post('forgot/password/verify/code', 'CustomerApiController@verifyCodeForForgotPassword');
        // Route::get('get-customer', 'CustomerApiController@getCustomerData');
    });

    Route::group([
        'prefix' => 'staff'
    ], function() {
        Route::get('/settings',[SettingsApiController::class, 'show']);
    });

    Route::group([
        'prefix' => 'customer'
    ], function() {
        Route::post('/store',[CustomerApiController::class, 'store']);
    });

    Route::group([
        'prefix' => 'estimation'
    ], function() {
        Route::get('/send-sms-mail/qid/pdf/mobile', [EstimationApiController::class, 'sendSMSMail']);
        Route::post('/image/store', [EstimationApiController::class, 'imageStore']);
        Route::post('/calc', [EstimationApiController::class, 'calculation']);
    });

    Route::get('logout',[AuthController::class, 'logout']);
});
