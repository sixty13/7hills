<?php $__env->startSection('content'); ?>

<div class="page__header">
  <div class="container page__heading-container">
    <div class="page__heading d-flex align-items-center">
      <div class="flex">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item active">Settings</li>
          </ol>
        </nav>
        <h1 class="m-0">SETTINGS</h1>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="page__container" id="settings">

    <form id="full_carving" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Full Carving</strong></p>
              <input type="hidden" name="parent_id" value="1">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Carving Type</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-carving px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-carving">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 1): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Carving Type" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-carving px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[0]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_full_carving" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_full_carving"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[0]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary full-carving save-setting" type="submit" data-id="full_carving"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="seat_height" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Seat Height</strong></p>
              <input type="hidden" name="parent_id" value="2">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Height( in inches )</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-seat-height px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-seat-height">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 2): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Seat Height" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-seat-height px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[1]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_seat_height" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_seat_height"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[1]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary seat-height save-setting" type="submit" data-id="seat_height"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
    
    
    <form id="back_height" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Back Height</strong></p>
              <input type="hidden" name="parent_id" value="3">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Height( in inches )</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-back-height px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-back-height">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 3): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Back Height" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-back-height px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[2]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_back_height" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_back_height"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[2]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary back-height save-setting" type="submit" data-id="back_height"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="seat_width" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Seat Width</strong></p>
              <input type="hidden" name="parent_id" value="4">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Width( in inches )</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-width px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-width">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 4): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Seat Width" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-width px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[3]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_add_widtht" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_add_width"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[3]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary seat-width save-setting" type="submit" data-id="seat_width"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="seat_depth" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Seat Depth</strong></p>
              <input type="hidden" name="parent_id" value="5">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Depth( in inches )</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-depth px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-depth">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 5): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Seat Depth" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-depth px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[4]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_add_depth" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_add_depth"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[4]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary seat-depth save-setting" type="submit" data-id="seat_depth"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="frame_shaking" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Frame Shaking</strong></p>
              <input type="hidden" name="parent_id" value="6">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row">
                <div class="col-md-2">
                  <div>
                    <label for="inlineFormRole">Enable</label>
                  </div>
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[5]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_frame_shaking" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_frame_shaking"></label>
                  </div>
                </div>
                <div class="col-md-4" id="add-depth">
                  <label>Price</label>
                  <div class="form-group">
                    <input type="number" name="price" class="form-control" value="<?php echo $settings[5]->price == 0 ? "" : $settings[5]->price ?>" placeholder="Enter Price">
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary frame_shaking save-setting" type="submit" data-id="frame_shaking"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="cracks_in_frame" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">No of Cracks in Frame</strong></p>
              <input type="hidden" name="parent_id" value="7">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row">
                <div class="col-md-2">
                  <div>
                    <label for="inlineFormRole">Enable</label>
                  </div>
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[6]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_no_cracks" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_no_cracks"></label>
                  </div>
                </div>
                <div class="col-md-4" id="add-depth">
                  <label>Price(Per No. of Cracks)</label>
                  <div class="form-group">
                    <input type="number" name="price" value="<?php echo $settings[6]->price == 0 ? "" : $settings[6]->price ?>" class="form-control" placeholder="Enter Price">
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary cracks_in_frame save-setting" type="submit" data-id="cracks_in_frame"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="no_of_breakages" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">No of Breakages</strong></p>
              <input type="hidden" name="parent_id" value="8">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row">
                <div class="col-md-2">
                  <div>
                    <label for="inlineFormRole">Enable</label>
                  </div>
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[7]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_breakages" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_breakages"></label>
                  </div>
                </div>
                <div class="col-md-4" id="add-depth">
                  <label>Price(Per No. of Breakages)</label>
                  <div class="form-group">
                    <input type="number" name="price" value="<?php echo $settings[7]->price == 0 ? "" : $settings[7]->price ?>" class="form-control" placeholder="Enter Price">
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary no_of_breakages save-setting" type="submit" data-id="no_of_breakages"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="missing_parts" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Missing Parts</strong></p>
              <input type="hidden" name="parent_id" value="20">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">No. of Missing Part</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-missing-parts px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-missing-parts">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 20): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter No. of Missing Parts" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-missing-parts px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[19]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_missing_parts" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_missing_parts"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[19]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary missing-parts save-setting" type="submit" data-id="missing_parts"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="repolish_required" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Repolish required</strong></p>
              <input type="hidden" name="parent_id" value="9">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <label>Enable</label>
              <div class="row">
                <div class="col-md-6" id="add-depth">
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[8]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_repolish" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_repolish"></label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary repolish_required save-setting" type="submit" data-id="repolish_required"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="colors_to_change" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Colors to be channged</strong></p>
              <input type="hidden" name="parent_id" value="10">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Colors</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-color px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-color">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 10): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Color" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-color px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[9]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_add_color" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_add_color"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[9]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary colors-to-change save-setting" type="submit" data-id="colors_to_change"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="polish_type" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Polish Type required</strong></p>
              <input type="hidden" name="parent_id" value="11">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Polish Type</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-polish px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-polish">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 11): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Polish Type" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-cover-material px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[10]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_add_polish" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_add_polish"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[10]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary polish-type save-setting" type="submit" data-id="polish_type"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="reubber_cap" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Rubber Cap Required</strong></p>
              <input type="hidden" name="parent_id" value="12">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row">
                <div class="col-md-2">
                  <div>
                    <label for="inlineFormRole">Enable</label>
                  </div>
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[11]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_rubber_cap" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_rubber_cap"></label>
                  </div>
                </div>
                <div class="col-md-4" id="add-depth">
                  <label>Price</label>
                  <div class="form-group">
                    <input type="number" name="price" value="<?php echo $settings[11]->price == 0 ? "" : $settings[11]->price ?>" class="form-control" placeholder="Enter Price">
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary reubber_cap save-setting" type="submit" data-id="reubber_cap"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="seat_ply" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Seat Ply to be changed</strong></p>
              <input type="hidden" name="parent_id" value="13">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <label>Enable</label>
              <div class="row">
                <div class="col-md-6" id="add-depth">
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[12]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_seat_ply" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_seat_ply"></label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary seat_ply save-setting" type="submit" data-id="seat_ply"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="seat_foam" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Seat Foam  to be changed</strong></p>
              <input type="hidden" name="parent_id" value="14">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <label>Enable</label>
              <div class="row">
                <div class="col-md-6" id="add-depth">
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[13]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_seat_foam" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_seat_foam"></label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary seat_foam save-setting" type="submit" data-id="seat_foam"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="seat_cover" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Seat Cover to be changed</strong></p>
              <input type="hidden" name="parent_id" value="15">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Seat Cover Options</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-seat-cover px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-seat-cover">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 15): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Seat Cover Type" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-seat-cover px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[14]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_seat_cover" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_seat_cover"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[14]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary seat-cover save-setting" type="submit" data-id="seat_cover"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="non_woven" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Non Woven for bottom</strong></p>
              <input type="hidden" name="parent_id" value="16">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row">
                <div class="col-md-2">
                  <div>
                    <label for="inlineFormRole">Enable</label>
                  </div>
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[15]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_non_woven" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_non_woven"></label>
                  </div>
                </div>
                <div class="col-md-4" id="add-depth">
                  <label>Price</label>
                  <div class="form-group">
                    <input type="number" name="price" value="<?php echo $settings[15]->price == 0 ? "" : $settings[15]->price ?>" class="form-control" placeholder="Enter Price">
                  </div>
                </div>
                <div class="form-group col-md-6">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary non_woven save-setting" type="submit" data-id="non_woven"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="back_ply" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Back Ply to be changed</strong></p>
              <input type="hidden" name="parent_id" value="17">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <label>Enable</label>
              <div class="row">
                <div class="col-md-6" id="add-depth">
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[16]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_back_ply" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_back_ply"></label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary back_ply save-setting" type="submit" data-id="back_ply"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="back_foam" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Back Foam to be changed</strong></p>
              <input type="hidden" name="parent_id" value="18">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <label>Enable</label>
              <div class="row">
                <div class="col-md-6" id="add-depth">
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[17]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_back_foam" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_back_foam"></label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary back_foam save-setting" type="submit" data-id="back_foam"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="back_cover" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Back Cover to be Changed</strong></p>
              <input type="hidden" name="parent_id" value="19">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Back Cover Type</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-back-cover px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-back-cover">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 19): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Back Cover Type" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-carving px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[18]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_back_covers" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_back_covers"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[18]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary back-cover save-setting" type="submit" data-id="back_cover"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    
    <form id="cover_material" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Cover material</strong></p>
              <input type="hidden" name="parent_id" value="21">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row mb-2">
                <label class="col-md-11">Material</label>
                <div class="col-md-1">
                  <button class="btn btn-primary btn-add-cover-material px-2 " style="margin: 0px 0px 0px -56px;"><i class="fas fa-plus-circle"></i></button>
                </div>
               </div> 
              <div class="">
                <div class="carving-container" id="add-cover-material">
                  <?php for($i = 0; $i < count($subSettings); $i++): ?>
                    <?php if($subSettings[$i]->setting_id == 21): ?>
                      <div class="row">
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="hidden" name="id[]" value=<?php echo e($subSettings[$i]->id); ?>>
                            <input type="text" class="form-control" name="name[]" placeholder="Enter Cover Material" value="<?php echo e($subSettings[$i]->name); ?>">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="form-group">
                            <input type="number" class="form-control" name="price[]" placeholder="Enter Price" value="<?php echo e($subSettings[$i]->price); ?>">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <button class="btn btn-danger btn-remove-cover-material px-2" data-id="<?php echo e($subSettings[$i]->id); ?>"><i class="fas fa-minus-circle"></i></button>
                            
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php endfor; ?>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div>
                      <label for="inlineFormRole">Enable</label>
                    </div>
                    <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                      <input <?php echo $settings[20]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_cover_material" class="custom-control-input">
                      <label class="custom-control-label" for="subscribe_cover_material"></label>
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="inlineFormRole">Selective Choice</label>
                      <select id="inlineFormRole" name="inlineFormRole" class="custom-select w-50 d-block">
                        <option value="single">Single</option>
                        <option value="multiple" <?php echo $settings[20]->selection == 1 ? 'selected' : "" ?>>Multiple</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="text-right">
                  <button id="save" class="btn btn-primary cover-material save-setting" type="submit" data-id="cover_material"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                  <button class="btn btn-danger">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="user_specific" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Any Specific requirement from Customer</strong></p>
              <input type="hidden" name="parent_id" value="22">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <label>Enable</label>
              <div class="row">
                <div class="col-md-6" id="add-depth">
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[21]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_user_specific" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_user_specific"></label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary user_specific save-setting" type="submit" data-id="user_specific"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="additional_cost" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">Other Additional Cost</strong></p>
              <input type="hidden" name="parent_id" value="23">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <label>Enable</label>
              <div class="row">
                <div class="col-md-6" id="add-depth">
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $settings[22]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_additional_cost" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_additional_cost"></label>
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary additional_cost save-setting" type="submit" data-id="additional_cost"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div> 
    </form>

    <form id="cgst" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">CGST</strong></p>
              <input type="hidden" name="gst_id" value="1">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row">
                <div class="col-md-2">
                  <div>
                    <label for="inlineFormRole">Enable</label>
                  </div>
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $gst[0]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_cgst" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_cgst"></label>
                  </div>
                </div>
                <div class="col-md-4" id="add-cgst">
                  <label>Percentage(%)</label>
                  <div class="form-group">
                    <input type="number" name="percent" value="<?php echo $gst[0]->percentage == 0 ? "" : $gst[0]->percentage ?>" class="form-control" placeholder="Enter CGST">
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary cgst save-setting" type="submit" data-id="cgst"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="sgst" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">SGST</strong></p>
              <input type="hidden" name="gst_id" value="2">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row">
                <div class="col-md-2">
                  <div>
                    <label for="inlineFormRole">Enable</label>
                  </div>
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $gst[1]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_sgst" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_sgst"></label>
                  </div>
                </div>
                <div class="col-md-4" id="add-sgst">
                  <label>Percentage(%)</label>
                  <div class="form-group">
                    <input type="number" name="percent" value="<?php echo $gst[1]->percentage == 0 ? "" : $gst[1]->percentage ?>" class="form-control" placeholder="Enter SGST">
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary sgst save-setting" type="submit" data-id="sgst"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <form id="igst" onsubmit="return false;">
      <div>
        <div class="card card-form">
          <div class="row no-gutters">
            <div class="col-lg-4 card-body">
              <p><strong class="headings-color">IGST</strong></p>
              <input type="hidden" name="gst_id" value="3">
            </div>
            <div class="col-lg-8 card-form__body card-body">
              <div class="row">
                <div class="col-md-2">
                  <div>
                    <label for="inlineFormRole">Enable</label>
                  </div>
                  <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                    <input <?php echo $gst[2]->active == 1 ? 'checked' : "" ?> type="checkbox" name="checkbox" id="subscribe_igst" class="custom-control-input">
                    <label class="custom-control-label" for="subscribe_igst"></label>
                  </div>
                </div>
                <div class="col-md-4" id="add-igst">
                  <label>Percentage(%)</label>
                  <div class="form-group">
                    <input type="number" name="percent" value="<?php echo $gst[2]->percentage == 0 ? "" : $gst[2]->percentage ?>" class="form-control" placeholder="Enter IGST">
                  </div>
                </div>
                <div class="form-group">
                  <div class="custom-control mr-1">
                    
                  </div>
                </div>
              </div>
              <div class="text-right">
                <button id="save" class="btn btn-primary igst save-setting" type="submit" data-id="igst"><span class="d-none" id="loader"><i class="fa fa-spinner fa-spin"></i></span>Save</button>
                <button class="btn btn-danger">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

  </div>
</div>
<button id="snackbutton" onclick="myFunction()" hidden>Show Snackbar</button>

<div id="snackbar"><span id="display_msg"></span></div>

<script>
  function myFunction() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }

  $(window).on("load", function() {
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-revert',
        type: 'GET',
        success: function(data){},
      });
  });

  $(document).ready( function(){

    $("body").on("click", ".btn-add-carving", function(){
      $(".car-add").addClass("d-none");
      var element = [];
      var rootElement = document.createElement('div');
      rootElement.classList.add("row");
      var html = `
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter Carving Type">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-carving px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-carving');
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-carving", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data){},
      });

    });

    $("body").on("click", ".btn-add-missing-parts", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter No. of Missing Parts">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-missing-parts px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-missing-parts');
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-missing-parts", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      console.log("id", id);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data){},
      });

    });

    $("body").on("click", ".btn-add-cover-material", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter Cover Material">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-cover-material px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-cover-material');
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-cover-material", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      console.log("id", id);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data){},
      });

    });

    $("body").on("click", ".btn-add-seat-height", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter Seat Height">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-seat-height px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-seat-height');
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-seat-height", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data){}
        
      });

    });

    $("body").on("click", ".btn-add-back-height", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter Back Height">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-back-height px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-back-height');
      container.appendChild(rootElement);
    });
    
    $("body").on("click", ".btn-remove-back-height", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data)
        {

        },
      });

    });

    $("body").on("click", ".btn-add-width", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter Seat Width">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-width px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-width');
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-width", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data)
        {
          console.log(data);
        },
      });

    });

    $("body").on("click", ".btn-add-depth", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter Seat Depth">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-depth px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-depth');
      container.appendChild(rootElement);
    });
    
    $("body").on("click", ".btn-remove-depth", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data)
        {
          console.log(data);
        },
      });
      
    });

    $("body").on("click", ".btn-add-color", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter Color">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-color px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-color');
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-color", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data)
        {
          console.log(data);
        },
      });

    });

    $("body").on("click", ".btn-add-seat-cover", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                    <input type="text" class="form-control" name="name[]" placeholder="Enter Seat Cover Type">
                </div>
              </div>
              <div class="col-md-5">
                <div class="form-group">
                  <input type="number" class="form-control" name="price[]" placeholder="Enter Price">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-seat-cover px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-seat-cover');
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-seat-cover", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data)
        {
          console.log(data);
        },
      });

    });

    $("body").on("click", ".btn-add-polish", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <input type="hidden" name="price[]" value="0">
                  <input type="text" class="form-control" name="name[]" placeholder="Enter Polish Type">
                </div>
              </div>
              <div class="col-md-5">
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-polish px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-polish');
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-polish", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data){},
      });

    });

    $("body").on("click", ".btn-add-back-cover", function(){
      var element = [];
      var rootElement = document.createElement('div');
      var html = `<div class="row">
              <div class="col-md-5">
                <div class="form-group">
                  <input type="hidden" name="price[]" value="0">
                  <input type="text" class="form-control" name="name[]" placeholder="Enter Back Cover Type">
                </div>
              </div>
              <div class="col-md-5">
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <button class="btn btn-danger btn-remove-back-cover px-2"><i class="fas fa-minus-circle"></i></button>
                </div>
              </div>
            </div>`;
      element.push(html);
      rootElement.innerHTML = element;
      var container = document.querySelector('#add-back-cover') ;
      container.appendChild(rootElement);
    });

    $("body").on("click", ".btn-remove-back-cover", function(){
      var id = $(this).attr('data-id');
      $(this).parent().parent().parent().remove();
      if(id == undefined || id == "") return true;
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/to-delete/'+id,
        type: 'GET',
        success: function(data){
          console.log("here", data);
          $("#display_msg").text(data.msg);
          $('#snackbutton').click();
        },
      });
    });

    $(document).on("click", ".save-setting", function() {
      var id = $(this).attr('data-id');
      var formData = $("#"+id).serialize();
      $("#loader").removeClass("d-none");
      $("#save").attr("disabled", true);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $.ajax({
        url: url+'/setting/update',
        type: 'POST',
        data: formData,
        success: function(data){
          $("#loader").addClass("d-none");
          $("#save").attr("disabled", false);
          $("#display_msg").text(data.msg);
          $('#snackbutton').click();
        },
      });
    });
  });
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('head'); ?>
<!-- Dropzone -->
<link type="text/css" href="<?php echo e(mix('css/vendor/dropzone.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<!-- Dropzone -->
<script src="<?php echo e(mix('vendor/dropzone.min.js')); ?>" defer></script>
<script src="<?php echo e(mix('js/dropzone.js')); ?>" defer></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("flowdash::layouts.fluid", ['title' => 'USERS'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/settings/index.blade.php ENDPATH**/ ?>