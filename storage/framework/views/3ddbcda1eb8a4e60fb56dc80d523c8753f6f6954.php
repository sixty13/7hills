<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>" dir="<?php echo e(config('flowdash.rtl') ? 'rtl' : 'ltr'); ?>">
    <head>
        <base href = "<?php echo e(url('/')); ?>" />
        <?php echo $__env->make('flowdash::partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </head>

<?php
    $layout = 'fluid';
    $navbarContainerClass = $navbarContainerClass ?? 'container-fluid';
    $drawerAlign = 'end';
    $dashboards_menu = request()->routeIs(['home', 'analytics', 'quick-access', 'staff', 'ecommerce']);
    $apps_menu = request()->routeIs(['app-trello', 'app-projects', 'app-activities', 'app-fullcalendar', 'app-chat', 'app-email', 'app-browse-courses', 'app-course', 'app-lesson']);
    $pages_menu = request()->routeIs(['companies', 'stories', 'discussions', 'product-listing', 'payout', 'invoice', 'pricing', 'edit-account', 'blank', 'profile', 'digital-product', 'login', 'register']);
    $components_menu = request()->routeIs(['ui-buttons', 'ui-alerts', 'ui-avatars', 'ui-modals', 'ui-charts', 'ui-icons', 'ui-forms', 'ui-range-sliders', 'ui-datetime', 'ui-tables', 'ui-tabs', 'ui-loaders', 'ui-drag', 'ui-pagination', 'ui-vector-maps']);
?>

<body class="layout-fluid layout-sticky-subnav <?php echo e($bodyClass ?? ''); ?>">

    <?php echo $__env->make('flowdash::partials.preloader', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <!-- Header Layout -->
    <div class="mdk-header-layout js-mdk-header-layout">

        <!-- Header -->
        <div id="header" class="mdk-header js-mdk-header <?php echo e($headerClass ?? 'mb-0'); ?>" data-fixed>
            <div class="mdk-header__content">
                <?php echo $__env->make('flowdash::partials.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
        <!-- // END Header -->

        <!-- Header Layout Content -->
        <div class="mdk-header-layout__content page <?php echo e($pageContentClass ?? ''); ?>">
            <?php if($submenu ?? true !== false): ?>
                <div class="page__header page__header-nav">
                    <div class="<?php echo e($containerClass); ?> page__container">
                        <?php echo $__env->make('flowdash::partials.submenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(auth()->guard()->check()): ?>
                <?php if($user_header ?? true !== false): ?>
                    <?php echo $__env->make('flowdash::partials.user-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
            <?php endif; ?>


            <?php echo $__env->yieldContent('content'); ?>
        </div>
        <!-- // END Header Layout Content -->

    </div>
    <!-- // END Header Layout -->

    <?php echo $__env->make('flowdash::partials.drawer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    

    <?php echo $__env->make('flowdash::partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldContent('footer'); ?>
</body>
</html>
<?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/layouts/fluid.blade.php ENDPATH**/ ?>