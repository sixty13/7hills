<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- CSRF Token -->
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    #snackbar {
      visibility: hidden;
      min-width: 250px;
      margin-left: -125px;
      background-color: rgb(51, 156, 2);
      color: #fff;
      text-align: center;
      border-radius: 2px;
      padding: 16px;
      position: fixed;
      z-index: 1;
      left: 50%;
      bottom: 30px;
      font-size: 17px;
    }
    
    #snackbar.show {
      visibility: visible;
      -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
      animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }
    
    @-webkit-keyframes fadein {
      from {bottom: 0; opacity: 0;} 
      to {bottom: 30px; opacity: 1;}
    }
    
    @keyframes  fadein {
      from {bottom: 0; opacity: 0;}
      to {bottom: 30px; opacity: 1;}
    }
    
    @-webkit-keyframes fadeout {
      from {bottom: 30px; opacity: 1;} 
      to {bottom: 0; opacity: 0;}
    }
    
    @keyframes  fadeout {
      from {bottom: 30px; opacity: 1;}
      to {bottom: 0; opacity: 0;}
    }
</style>

<script>
    var url = "<?php echo e(url('/')); ?>";
</script>

<title>7hills</title>

<!-- Fonts -->



<!-- Prevent the demo from appearing in search engines -->
<meta name="robots" content="noindex">

<!-- Perfect Scrollbar -->
<link type="text/css" href="<?php echo e(asset('public/css/vendor/perfect-scrollbar.css?v='.time())); ?>" rel="stylesheet">

<!-- Material Design Icons -->


<!-- Font Awesome Icons -->
<link type="text/css" href="<?php echo e(asset('public/css/vendor/fontawesome.css?v='.time())); ?>" rel="stylesheet">

<!-- App CSS -->
<link type="text/css" href="<?php echo e(asset('public/css/app.css?v='.time())); ?>" rel="stylesheet">

<!-- Dropzone -->
<link type="text/css" href="<?php echo e(asset('public/css/vendor/dropzone.css?v='.time())); ?>" rel="stylesheet">

<script>
    var token = '<?php echo e(csrf_token()); ?>';
    var url = '<?php echo e(url("/")); ?>';
</script>

<?php echo $__env->yieldContent('head'); ?>

<script src="<?php echo e(asset('public/js/app.js?v='.time())); ?>" defer></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
<script src="<?php echo e(asset('public/vendor/all.min.js')); ?>" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.js" integrity="sha512-Y+cHVeYzi7pamIOGBwYHrynWWTKImI9G78i53+azDb1uPmU1Dz9/r2BLxGXWgOC7FhwAGsy3/9YpNYaoBy7Kzg==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.css" integrity="sha512-8D+M+7Y6jVsEa7RD6Kv/Z7EImSpNpQllgaEIQAtqHcI0H6F4iZknRj0Nx1DCdB+TwBaS+702BGWYC0Ze2hpExQ==" crossorigin="anonymous" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.js" integrity="sha512-zlWWyZq71UMApAjih4WkaRpikgY9Bz1oXIW5G0fED4vk14JjGlQ1UmkGM392jEULP8jbNMiwLWdM8Z87Hu88Fw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-toast-plugin/1.3.2/jquery.toast.min.css" integrity="sha512-wJgJNTBBkLit7ymC6vvzM1EcSWeM9mmOu+1USHaRBbHkm6W9EgM0HY27+UtUaprntaYQJF75rc8gjxllKs5OIQ==" crossorigin="anonymous" /><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/partials/header.blade.php ENDPATH**/ ?>