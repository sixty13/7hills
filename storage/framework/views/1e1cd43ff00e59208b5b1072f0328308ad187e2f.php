<div class="layout-login-centered-boxed__form card">
  <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-5 navbar-light">
    <?php echo e($logo); ?>

  </div>

  <?php echo e($slot); ?>

</div>
<?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/jetstream/components/authentication-card.blade.php ENDPATH**/ ?>