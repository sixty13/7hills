<?php $__env->startSection('content'); ?>

<div class="<?php echo e($containerClass ?? 'container'); ?> page__container">
  

  <div class="row">
    <div class="col-lg-12">
      
      <div class="card">
        <div class="card-header bg-white d-flex align-items-center">
          <h4 class="card-header__title flex m-0">Current Sales</h4>
          <div
            data-toggle="flatpickr" 
            data-flatpickr-wrap="true"
            data-flatpickr-static="true" 
            data-flatpickr-mode="range" 
            data-flatpickr-alt-format="d/m/Y" 
            data-flatpickr-date-format="d/m/Y">
            <a href="javascript:void(0)" class="link-date" data-toggle>13/03/2018 <span class="text-muted mx-1">to</span> 20/03/2018</a>
            <input class="flatpickr-hidden-input" type="hidden" value="13/03/2018 to 20/03/2018" data-input>
          </div>
        </div>
        <div class="card-body text-muted">
          <div class="chart" style="height: calc(248px);">
            <canvas id="earningsTrafficChart">
              <span style="font-size: 1rem;"><strong>Website Traffic / Earnings</strong> area chart goes here.</span>
            </canvas>
          </div>
        </div>
      </div>

    </div>
    

    </div>
  </div>

  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
<!-- Flatpickr -->
<link type="text/css" href="<?php echo e(asset('public/css/vendor/flatpickr.css')); ?>" rel="stylesheet">
<link type="text/css" href="<?php echo e(asset('public/css/vendor/flatpickr-airbnb.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<!-- Global Settings -->
<script src="<?php echo e(asset('public/js/settings.js')); ?>" defer></script>

<!-- Flatpickr -->
<script src="<?php echo e(asset('public/vendor/flatpickr/flatpickr.min.js')); ?>" defer></script>
<script src="<?php echo e(asset('public/js/flatpickr.js')); ?>" defer></script>

<!-- Moment.js -->
<script src="<?php echo e(asset('public/vendor/moment.min.js')); ?>" defer></script>
<script src="<?php echo e(asset('public/vendor/moment-range.js')); ?>" defer></script>

<!-- Chart.js -->
<script src="<?php echo e(asset('public/vendor/Chart.min.js')); ?>" defer></script>
<script src="<?php echo e(asset('public/js/charts.js')); ?>" defer></script>
<script src="<?php echo e(asset('public/js/chartjs-rounded-bar.js')); ?>" defer></script>

<!-- Chart.js Samples -->
<script src="<?php echo e(asset('public/js/page.dashboard.js')); ?>" defer></script>
<script src="<?php echo e(asset('public/js/progress-charts.js')); ?>" defer></script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make("flowdash::layouts.fluid", [
  'title' => 'Dashboard',
  'breadcrumb' => [[
    'title' => 'Dashboard'
  ]]
  // 'new_button_label' => 'New Report'
], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/home.blade.php ENDPATH**/ ?>