<?php if(auth()->guard()->check()): ?>
  <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
    <?php echo csrf_field(); ?>
  </form>
<?php endif; ?>

<!-- Scripts -->
<script src="<?php echo e(asset('public/js/manifest.js')); ?>"></script>
<script src="<?php echo e(asset('public/js/vendor.js')); ?>"></script>
<?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/partials/footer.blade.php ENDPATH**/ ?>