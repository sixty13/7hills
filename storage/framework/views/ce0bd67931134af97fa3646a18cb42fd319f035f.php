<button <?php echo e($attributes->merge(['type' => 'submit', 'class' => 'btn btn-primary'])); ?>>
    <?php echo e($slot); ?>

</button>
<?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/jetstream/components/button.blade.php ENDPATH**/ ?>