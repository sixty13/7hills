<div class="navbar navbar-expand-sm navbar-main <?php echo e($navbarClass ?? ''); ?> pr-0" style="background-color: #F18231" id="navbar" data-primary>
  <div class="<?php echo e($navbarContainerClass ?? ($containerClass ?? 'container')); ?> <?php echo e($layout !== 'fixed' ? 'p-0' : ''); ?>">

    <!-- Navbar toggler -->
    <?php if(count(array_intersect([$layout ?? ''], ['fluid', 'mini'])) > 0): ?>
      <button class="navbar-toggler navbar-toggler-custom navbar-toggler-right d-block" type="button" data-toggle="sidebar">
        <span class="material-icons">apps</span>
      </button>
    <?php else: ?>
      <button class="navbar-toggler navbar-toggler-right d-block d-md-none" type="button" data-toggle="sidebar">
        <span class="navbar-toggler-icon"></span>
      </button>
    <?php endif; ?>

    <!-- Navbar Brand -->
    
      
      <img src=<?php echo e(asset('public/storage/logo/7Hill_Logo.jpg')); ?> style="width: 9%;" >
      
        
    </a>

    <?php if($layout === 'mini'): ?>
    <div class="navbar-collapse collapse flex">
      <?php echo $__env->make('flowdash::partials.navbar-menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <?php endif; ?>

    

    

    <ul class="nav navbar-nav d-none d-sm-flex border-left navbar-height align-items-center">
        <!-- Authentication Links -->
        <?php if(auth()->guard()->guest()): ?>
            <div class="nav-item <?php echo e(activeClass('pricing')); ?>">
                <a class="nav-link" href="<?php echo e(route('pricing')); ?>"><?php echo e(__('Pricing')); ?></a>
            </div>
            <div class="nav-item <?php echo e(activeClass(['login', 'welcome'])); ?>">
                <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
            </div>
            <?php if(Route::has('register')): ?>
                <div class="nav-item <?php echo e(activeClass('register')); ?>">
                    <a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
                </div>
            <?php endif; ?>
        <?php else: ?>
        <li class="nav-item dropdown">
            <a href="#account_menu" class="nav-link dropdown-toggle" data-toggle="dropdown" data-caret="false">
              <span class="mr-1 d-flex-inline">
                <span class="text-light">Admin</span>
              </span>
              <img src=<?php echo e(asset('public/storage/logo/noimage1.png')); ?> class="img-thumbnail" width="32" alt="Frontted">
            </a>
            <div id="account_menu" class="dropdown-menu dropdown-menu-right">
              <div class="dropdown-item-text dropdown-item-text--lh">
                <div><strong>Admin</strong></div>
              </div>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item <?php echo e(activeClass('home')); ?>" href="<?php echo e(route('home')); ?>">
                <i class="fas fa-home"></i>
                Dashboard
              </a>
              
              <a class="dropdown-item <?php echo e(activeClass('edit-account')); ?>" href="<?php echo e(route('edit-account')); ?>">
                <i class="fas fa-pen"></i>
                edit account
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i>
                logout
              </a>
            </div>
        </li>
        <?php endif; ?>
    </ul>

  </div>
</div><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/partials/navbar.blade.php ENDPATH**/ ?>