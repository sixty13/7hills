<?php $__env->startSection('content'); ?>
<div class="page__header">
    <div class="container page__heading-container">
        <div class="page__heading d-flex align-items-center">
            <div class="flex">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Reports</li>
                    </ol>
                </nav>
                <h1 class="m-0">REPORTS</h1>
            </div>
            
        </div>
    </div>
</div>
<div class="container page__container">
    <div class="card card-form">
        <div class="row no-gutters">
            <div class="col-lg-12 card-form__body">
                <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values="[&quot;js-lists-values-employee-name&quot;]">
                    <?php //dd($quotations[0]->payable_amount); ?>
                    <table class="table mb-0 thead-border-top-0">
                        <thead>
                            <tr>
                                <th>SNO</th>
                                <th>F.NAME</th>
                                <th>L.NAME</th>
                                <th>EMAIL</th>
                                <th>MOBILE NO</th>
                                <th>QUOTATION</th>
                                <th>St.DIS.</th>
                                <th>SA DIS.</th>
                                <?php if($user->user_type == 1): ?>
                                    <th style="text-align: center;">DISC</th>
                                    <th style="text-align: center;">PDF GEN</th> 
                                <?php endif; ?>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if( count($quotations) > 0): ?>
                            <?php $__currentLoopData = $quotations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$quotation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <?php //echo "<pre>"; print_r($quotation->payable_amount); ?>
                                <td name="sno"><?php echo e($k + 1); ?></td>
                                <td name="firestName"><?php echo e($quotation->customer->first_name); ?></td>
                                <td name="lastName"><?php echo e($quotation->customer->last_name); ?></td>
                                <td name="email"><?php echo e($quotation->customer->email); ?></td>
                                <td name="mobileNo"><small><?php echo e($quotation->customer->mobile_no); ?></small></td>
                                <td name="quotation" id="quotation_<?php echo e($quotation->id); ?>"><?php echo e($quotation->payable_amount); ?></td>
                                <td name="staff_disc"><?php echo e($quotation->discount); ?></td>
                                <td name="sa_disc" id="sa_disc_<?php echo e($quotation->id); ?>"><?php echo e($quotation->special_discount); ?></td>
                                <?php if($user->user_type == 1): ?>
                                    <form id="sp_discount"  onsubmit="return false;">
                                        <td name="special_discount"><input type="number" style="width: 60px;" name="sp_disc" id="<?php echo e($quotation->id); ?>_sp_disc" value="0" class="sa_disc_<?php echo e($k); ?>"></td>
                                        <td name="pdf_gen"><button type="submit" id="regenerate_<?php echo e($quotation->id); ?>" class="btn btn-primary regenerate" data-id="<?php echo e($quotation->id); ?>"><span class="d-none" id="loader_<?php echo e($quotation->id); ?>"><i class="fa fa-spinner fa-spin"></i></span><span id="regenerate_button_<?php echo e($quotation->id); ?>">REGENERATE</span></button></td>
                                    </form>
                                <?php endif; ?>
                                <td><button type="submit" id="resend_<?php echo e($quotation->id); ?>" data-id="<?php echo e($quotation->id); ?>" class="btn btn-primary resend"><span class="d-none" id="resend_loader_<?php echo e($quotation->id); ?>"><i class="fa fa-spinner fa-spin"></i></span><span id="resend_button_<?php echo e($quotation->id); ?>"><?php echo e($quotation->resend == '1' ? 'RESEND' : 'SEND'); ?></span></button></td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                            <tr>
                                <td colspan="<?php echo $user->user_type == 1 ? 9 : 7  ?>" style="text-align: center;">
                                    No Report found
                                </td>
                            </tr>
                            <?php endif; ?> 
                        </tbody>
                    </table>
                    <div class="col-md-12">
                        <?php if( count($quotations) > 0): ?>
                            <?php echo e($quotations->links()); ?>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button id="snackbutton" hidden onclick="myFunction()">Show Snackbar</button>

<div id="snackbar"><span id="display_msg"></span></div>


<script>
    function myFunction() {
        var x = document.getElementById("snackbar");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }

    $('.regenerate').on('click', function() {
        var id = $(this).attr('data-id');
        var disc = $("#"+id+"_sp_disc").val();
        if(disc == 0) {
            return true;
        }

        $("#loader_"+id).removeClass("d-none");
        $("#regenerate_button_"+id).addClass("d-none");
        $("#regenerate_"+id).attr("disabled", true);
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: url+'/user/pdf/regenerate/'+id+'?sp_disc='+disc,
            type: 'GET',
            success: function(data)
            {
                console.log("data", data);
                $("#loader_"+id).addClass("d-none");
                $("#regenerate_button_"+id).removeClass("d-none");
                $("#regenerate_"+id).removeAttr("disabled", false);
                $("#quotation_"+id).text(data.payable_amount.toFixed(2));
                $("#sa_disc_"+id).text(data.discount);
                $("input[name='sp_disc']").val('0');
                $("#display_msg").text(data.msg);
                $('#snackbutton').click();
            },
        });
    });

    $('.resend').on('click', function() {
            console.log("herer");
            var id = $(this).attr('data-id');

            $("#resend_loader_"+id).removeClass("d-none");
            $("#resend_button_"+id).addClass("d-none");
            $("#resend_"+id).attr("disabled", true);

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
            url: url+'/user/pdf/resend/'+id,
            type: 'GET',
            success: function(data)
            {
                $("#resend_loader_"+id).addClass("d-none");
                $("#resend_button_"+id).removeClass("d-none");
                $("#resend_"+id).removeAttr("disabled", false);
                $("#resend_button_"+id).text(data.resend);
                $("#display_msg").text(data.msg);
                $('#snackbutton').click();
            },
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("flowdash::layouts.fluid", ['title' => 'USERS'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/admin-report/index.blade.php ENDPATH**/ ?>