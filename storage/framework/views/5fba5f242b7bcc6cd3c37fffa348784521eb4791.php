<li class="sidebar-menu-item <?php echo e(activeClass('home')); ?>">
  <a class="sidebar-menu-button" href="<?php echo e(route('home')); ?>">
    <span class="sidebar-menu-text">Default</span>
  </a>
</li>
<li class="sidebar-menu-item <?php echo e(activeClass('analytics')); ?>">
  <a class="sidebar-menu-button" href="<?php echo e(route('analytics')); ?>">
    <span class="sidebar-menu-text">Analytics</span>
  </a>
</li>
<li class="sidebar-menu-item <?php echo e(activeClass('staff')); ?>">
  <a class="sidebar-menu-button" href="<?php echo e(route('staff')); ?>">
    <span class="sidebar-menu-text">Staff</span>
  </a>
</li>
<li class="sidebar-menu-item <?php echo e(activeClass('ecommerce')); ?>">
  <a class="sidebar-menu-button" href="<?php echo e(route('ecommerce')); ?>">
    <span class="sidebar-menu-text">E-commerce</span>
  </a>
</li>
<li class="sidebar-menu-item <?php echo e(activeClass('quick-access')); ?>">
  <a class="sidebar-menu-button" href="<?php echo e(route('quick-access')); ?>">
    <span class="sidebar-menu-text">Quick Access</span>
  </a>
</li><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/partials/dashboards-sidebar-menu.blade.php ENDPATH**/ ?>