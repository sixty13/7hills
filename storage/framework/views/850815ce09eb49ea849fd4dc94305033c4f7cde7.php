<?php if($layout === 'fluid'): ?>
<div class="page__header">
<?php endif; ?>


<?php if($title != 'USERS'): ?>
  <div class="<?php echo e($containerClass ?? 'container'); ?> page__heading-container">
    <div class="page__heading d-flex align-items-center">
      <div class="flex">

        <?php if($breadcrumb ?? []): ?>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb mb-0">
            <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
            <?php $__currentLoopData = $breadcrumb; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="breadcrumb-item <?php if($loop->last): ?> active <?php endif; ?>">
              <?php if($loop->last): ?>
                <?php echo e($item['title']); ?>

              <?php else: ?>
                <a href="<?php echo e($item['slug'] ?? ''); ?>"><?php echo e($item['title']); ?></a>
              <?php endif; ?>
            </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </ol>
        </nav>
        <?php endif; ?>

        <h1 class="m-0"><?php echo e($title ?? ''); ?></h1>
      </div>
      <?php if($title == 'USERS'): ?>
      <a href="<?php echo e(url($new_button_slug ?? '/')); ?>" class="btn btn-<?php echo e($new_button_class ?? 'success'); ?> ml-3" <?php echo ($new_button_toggle ?? null) ? 'data-toggle="' . $new_button_toggle . '"' : ''; ?> <?php echo ($new_button_target ?? null) ? 'data-target="' . $new_button_target . '"' : ''; ?>>
        <?php if(isset($new_button_icon_label)): ?>
          <i class="material-icons icon-16pt <?php echo e($new_button_icon_class ?? ''); ?> mr-1"><?php echo e($new_button_icon_label ?? 'settings'); ?></i>
        <?php endif; ?>
        <?php echo e($new_button_label ?? 'New Report'); ?>

      </a>
      <?php endif; ?>
    </div>
</div>
<?php endif; ?>

<?php if($layout === 'fluid'): ?>
</div> <!-- // END page__header -->
<?php endif; ?><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/partials/user-header.blade.php ENDPATH**/ ?>