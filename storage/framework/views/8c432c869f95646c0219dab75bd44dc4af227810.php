<?php $__env->startSection('content'); ?>

<div class="page__header">
    <div class="container page__heading-container">
        <div class="page__heading d-flex align-items-center">
            <div class="flex">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </nav>
                <h1 class="m-0">USERS</h1>
            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal" data-whatever="@mdo" onclick="$('#createModal').find('input:not([type=hidden]),textarea,select').val('');$('#btn_name').text('Add');">Add staff</button>
            <button type="button" id="staff_create_update" class="d-none btn btn-primary" data-toggle="modal" data-target="#createModal" data-whatever="@mdo">Add staff</button>
        </div>
    </div>
</div>
<div class="container page__container">
    <div class="card card-form">
        <div class="row no-gutters">
            <div class="col-lg-12 card-form__body">
                <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values="[&quot;js-lists-values-employee-name&quot;]">
                    <table class="table mb-0 thead-border-top-0">
                        <thead>
                            <tr>
                                <th>SNO</th>
                                <th>FIRST NAME</th>
                                <th>LAST NAME</th>
                                <th>EMAIL</th>
                                <th>DESIGNATION</th>
                                <th>MOBILE NUMBER</th>
                                <th>DISCOUNT(%)</th>
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if( count($staffs) > 0): ?>
                            <?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$staff): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td name="id"><?php echo e($k + 1); ?></td>
                                <td name="firestName"><?php echo e($staff->first_name); ?></td>
                                <td name="lastName"><?php echo e($staff->last_name); ?></td>
                                <td name="email"><?php echo e($staff->email); ?></td>
                                <td name="designation"><span class="badge badge-warning"><?php echo e($staff->designation); ?></span></td>
                                <td name="mobileNo"><small><?php echo e($staff->mobile_number); ?></small></td>
                                <td name="discount"><?php echo e($staff->allowed_discount); ?></td>
                                <td id="edit" name="edit" style="display: ruby-base-container;">
                                    <button type="button" object="<?php echo e($staff); ?>" class="btn btn-primary update-staff-details" style="margin-bottom: 5px;" ><i class="fas fa-pen" style="color: #fff;"></i></button>&nbsp;&nbsp;
                                    <form action="<?php echo e(url('/user/delete/'.$staff->id)); ?>" method="post">
                                        <?php echo csrf_field(); ?>
                                        <button type="submit" class="btn btn-danger" style="margin-bottom: 5px;"><i class="fas fa-trash" style="color: #fff;"></i></button>
                                    </form>
                                </td>
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="7" style="text-align: center;">
                                        No Staff record found
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                    <?php if( count($staffs) > 0): ?>
                    <?php echo e($staffs->links()); ?>

                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->startSection('footer'); ?>

<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form action="<?php echo e(url('/user/store')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="id" id="staff_id" value="">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">First name</label>
                        <input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter First name" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Last name</label>
                        <input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter Last name" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Designation</label>
                        <input type="text" name="designation" class="form-control" id="designation" placeholder="Enter Designation" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">E-mail Id</label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter E-mail Id" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Mobile No.</label>
                        <input type="number" name="mobile_no" class="form-control" id="mobile_no" placeholder="Enter Mobile number" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Allowed discount (in %)</label>
                        <input type="number" name="allowed_discount" class="form-control" id="allowed_discount" placeholder="Enter Allowed discount" value="" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="btn_name">Add</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div id="snackbar">
    <?php if(\Session::has('msg')): ?>
        <span id="display_msg"><?php echo \Session::get('msg'); ?></span>
    <?php endif; ?>
</div>


<?php $__env->stopSection(); ?>
<script>
    function myFunction() {
        var x = document.getElementById("snackbar");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }

    $(document).ready( function(){
        <?php if(\Session::has('msg')): ?>
            myFunction();
        <?php endif; ?>

        $(".update-staff-details").on("click", function(){
            var object = $(this).attr('object');
            object = JSON.parse(object);
            $("#staff_id").val(object.id);
            $("#first_name").val(object.first_name);
            $("#last_name").val(object.last_name);
            $("#email").val(object.email);
            $("#mobile_no").val(object.mobile_number);
            $("#designation").val(object.designation);
            $("#allowed_discount").val(object.allowed_discount);
            $("#btn_name").text("Update");
            $("#staff_create_update").click();
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("flowdash::layouts.fluid", ['title' => 'USERS'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/users/index.blade.php ENDPATH**/ ?>