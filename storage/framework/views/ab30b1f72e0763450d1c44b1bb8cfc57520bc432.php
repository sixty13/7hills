<?php $__env->startSection('content'); ?>
<div class="<?php echo e($containerClass ?? 'container'); ?> page__container">
  <form action="<?php echo e(url('/edit-account')); ?>" method="post">
    <?php echo csrf_field(); ?>
    <div class="card card-form">
      <div class="row no-gutters">
        <div class="col-lg-4 card-body">
          <p><strong class="headings-color">Basic Information</strong></p>
          <p class="text-muted">Edit your account details.</p>
        </div>
        <div class="col-lg-8 card-form__body card-body">
          <div class="row">
            <div class="col">
              <div class="form-group col-lg-6">
                <label for="fname">Name</label>
                <input id="fname" type="text" class="form-control" placeholder="Enter name" value="<?php echo e($user->name); ?>" required>
              </div>
            </div>
            
          </div>
          
        </div>
      </div>
    </div>

    <div class="card card-form">
      <div class="row no-gutters">
        <div class="col-lg-4 card-body">
          <p><strong class="headings-color">Update Your Password</strong></p>
          <p class="text-muted">Change your password.</p>
        </div>
        <div class="col-lg-8 card-form__body card-body">
          <div class="form-group">
            <label for="opass">Old Password</label>
            <input style="width: 270px;" id="opass" type="password" name="old_password" class="form-control" placeholder="Old password">
          </div>
          <div class="form-group">
            <label for="npass">New Password</label>
            <input style="width: 270px;" id="npass" type="password" name="password" class="form-control" placeholder="New password">
          </div>
          <div class="form-group">
            <label for="cpass">Confirm Password</label>
            <input style="width: 270px;" id="cpass" type="password" class="form-control" placeholder="Confirm password">
          </div>
        </div>
      </div>
    </div>

    

    <div class="text-right mb-5">
      <button type="submit" class="btn btn-success">Save</button>
    </div>
  </form>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('head'); ?>
<!-- Dropzone -->
<link type="text/css" href="<?php echo e(mix('css/vendor/dropzone.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
<!-- Dropzone -->
<script src="<?php echo e(mix('vendor/dropzone.min.js')); ?>" defer></script>
<script src="<?php echo e(mix('js/dropzone.js')); ?>" defer></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("flowdash::layouts.fluid", [
  'title' => 'Edit Account',
  'breadcrumb' => [[
    'title' => 'Account'
  ]],
  'new_button_label' => false
], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/7hills/web/7hills.peacockindia.in/public_html/resources/views/vendor/flowdash/edit-account.blade.php ENDPATH**/ ?>