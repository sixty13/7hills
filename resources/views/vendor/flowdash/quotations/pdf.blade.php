{{-- <!DOCTYPE html>
<html>

<head>
    <title>Quatation</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <style type="text/css">
        body{ width: 90%; margin: 15px auto; font-family: 'Nunito', sans-serif; font-size: 14px;}
        @media print {
            .print-btn{display: none;}
        }            
        .d-none { display: none; }
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
            overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
            font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0pky{text-align:left;vertical-align:top;}
        table { width: 100%; }
        .text-center { text-align: center !important; }
        .text-right { text-align: right !important; };
    </style>
</head>

<body>
    <table class="tg">
        <tbody>
            <tr>
                <td class="tg-0pky text-center">
                    <img src="{{ asset('public/storage/logo/7Hill_Logo.jpg') }}" style="width: 113px; height: 70px;">
                </td>
                <td class="tg-0pky text-center" colspan="5">
                    <p>7Hill Furniture Refurbishing</p>
                    <p>(A Division of Hevea Furniture & Interiors Pvt Ltd)</p>
                </td>
            </tr>
            <tr>
                <td class="tg-0pky" rowspan="4" style="width: 10%;">
                    <p>Customer Name: {{$quotation->customer->first_name}} {{$quotation->customer->last_name}}</p>
                    <p>Contact Number: {{$quotation->customer->mobile_no}}</p>
                    <p>Address: {{$quotation->customer->address}}</p>
                </td>
                <td class="tg-0pky text-center" rowspan="4" colspan="2" style="width: 70%; vertical-align: inherit;">
                    <p>QUOTATION FOR FURNITURE REFURBISHING SERVICES</p>
                </td>
                <td class="tg-0pky" style="width: 10%;">
                    <p>Enquiry ID</p>
                </td>
                <td class="tg-0pky" style="width: 10%;"></td>
            </tr>
            <tr>
                <td class="tg-0pky">
                    <p>Quote Number</p>
                </td>
                <td class="tg-0pky"></td>
            </tr>
            <tr>
                <td class="tg-0pky" rowspan="2">
                    <p>Date</p>
                </td>
                <td class="tg-0pky" rowspan="2">{{ date('d-m-Y', strtotime($quotation->updated_at)) }}</td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td class="tg-0pky text-center" style="width: 10%;">No</td>
                <td class="tg-0pky" style="width: 40%;">Service</td>
                <td class="tg-0pky" style="width: 20%;">Sub Service</td>
                <td class="tg-0pky" style="width: 15%">Additional Details</td>
                <td class="tg-0pky" style="width: 15%;">Price</td>
            </tr>
            @foreach ($quotation->quotationDetails as $k=>$ar)
            <tr>
                <td style="text-align: center" style="width: 10%;">{{ $k + 1 }}</td>
                <td style="text-align: left" style="width: 40%;">{{ $ar->setting }}</td>
                <td style="text-align: left" style="width: 20%;">{{ $ar->sub_setting ?? '-' }}</td>
                <td style="text-align: left" style="width: 20%;">{{ $ar->additional_detail ?? '-' }}</td>
                <td style="text-align: left" style="width: 10%;">{{ $ar->price }}</td>
            </tr>
@endforeach
<tr>
    <td class="tg-0pky text-right" colspan="4">Discount Value ({{ $quotation->discount }}%)</td>
    <td class="tg-0pky text-center">
        {{ $quotation->discount == 0 ? "0" : (($quotation->total * $quotation->discount) / 100) }}</td>
</tr>
<tr>
    <td class="tg-0pky text-right" colspan="4">Special Discount</td>
    <td class="tg-0pky text-center">
        {{ $quotation->special_discount == 0 ? "0" : ( $quotation->total - $quotation->sub_total ) }}</td>
</tr>
<tr>
    <td class="tg-0pky text-right" colspan="4">Total Quotation Value</td>
    <td class="tg-0pky text-center">{{ $quotation->sub_total }}</td>
</tr>
<tr>
    <td class="tg-0pky" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Please
        Note</td>
    <td class="tg-0pky" colspan="4" style="border-left: 0px; border-top: 0px; border-bottom: 0px;"></td>
</tr>
<tr>
    <td class="tg-0pky" style="border-right: 0px; border-top: 0px; border-bottom: 0px;">
        Payment Terms :</td>
    <td class="tg-0pky" colspan="4" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">50%
        Advance along with PO, Balance 50% Payment after Inspection at our Factory and before Delivery.</td>
</tr>
<tr>
    <td class="tg-0pky" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Delivery
        Period :</td>
    <td class="tg-0pky" colspan="4" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">2 to 3
        Weeks from the Date of Advance Received.</td>
</tr>
<tr>
    <td class="tg-0pky" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Transport
        :</td>
    <td class="tg-0pky" colspan="4" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">Free
        pickup of Old furniure & Delivery of Refurbished furniture.</td>
</tr>
<tr>
    <td class="tg-0pky" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Payment :
    </td>
    <td class="tg-0pky" colspan="4" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">Payment
        in Favour of Hevea Furniture & Interiors Pvt.Ltd</td>
</tr>
<tr>
    <td class="tg-0pky" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Our Bank
        Details :</td>
    <td class="tg-0pky" colspan="4" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">Karnataka
        Bank Ltd., Catherdral Road Branch, Chennai - 600 086,
        A/c. No. 4647000700012801(OD Account) - IFSC Code - KARB0000464.</td>
</tr>
<tr>
    <td class="tg-0pky" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Validity
        of Quotation :</td>
    <td class="tg-0pky" colspan="4" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">One Month
        from the Date of Quotation.</td>
</tr>
<tr>
    <td class="tg-0pky" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px solid #fff;">
        GSTIN:</td>
    <td class="tg-0pky" colspan="4" style="border-left: 0px; border-top: 0px; border-bottom: 0px solid #fff;">
        33AAACH1878A1Z2</td>
</tr>
</tbody>
</table>
<table style="border-bottom: 1px solid rgb(0, 0, 0)er-left: 1px solid #000; border-right: 1px solid #000; border-top: 0px solid #fff;">
    <tbody>
        <tr>
            <td class="tg-0pky text-center" style="width: 40%;">
                <img src="{{ asset('public/storage/logo/hevea.png') }}">
                <!--style="width: 544px; height:60px; "-->
            </td>
            <td class="tg-0pky" style="width: 60%;">
                1/183, Sri Selvavinayagar Nagar, Payasampakkam Village,
                Red Hills, Chennai, Tamil Nadu 600052
                Email: customercare@7hillfurnrefurbish.com | Phone: +91 73388 44717, +91 73388 55757
            </td>
        </tr>
    </tbody>
</table>

</body>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

</html> --}}

<!DOCTYPE html>
<html>

<head>
    <title>Quatation</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
          overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
          font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
        </style>
</head>

<body>
    <table class="tg" style="width: 100%;">
        <tbody>
            <tr>
                <td class="tg-0pky text-center" colspan="2">
                    <img src="{{ asset('public/storage/logo/7Hill_Logo.jpg') }}" style="width: 113px; height: 70px;">
                </td>
                <td class="tg-0pky text-center" colspan="7">
                    <p>7Hill Furniture Refurbishing</p>
                    <p>(A Division of Hevea Furniture & Interiors Pvt Ltd)</p>
                </td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="3" rowspan="4">
                    <p>Customer Name: {{$quotation->customer->first_name}} {{$quotation->customer->last_name}}</p>
                    <p>Contact Number: {{$quotation->customer->mobile_no}}</p>
                    <p>Address: {{$quotation->customer->address}}</p>
                </td>
                <td class="tg-0pky text-center" colspan="4" rowspan="4" >
                    <p>QUOTATION FOR FURNITURE REFURBISHING SERVICES</p>
                </td>
                <td class="tg-0pky">
                    <p>Enquiry ID</p>
                </td>
                <td class="tg-0pky"></td>
            </tr>
            <tr>
                <td class="tg-0pky">
                    <p>Quote Number</p>
                </td>
                <td class="tg-0pky"></td>
            </tr>
            <tr>
                <td class="tg-0pky" rowspan="2">
                    <p>Date</p>
                </td>
                <td class="tg-0pky" rowspan="2">{{ date('d-m-Y', strtotime($quotation->updated_at)) }}</td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td class="tg-0pky" style="width: 4%;">S No</td>
                <td class="tg-0pky">Product to be <br /> Refurbished</td>
                <td class="tg-0pky">Product Dimensions <br /> with Picture <br />  (in MM)W x D x H</td>
                <td class="tg-0pky">Rework Process</td>
                <td class="tg-0pky">Finish <br /> (Repolish or Re-Upholstery)</td>
                <td class="tg-0pky">Packing Type</td>
                <td class="tg-0pky">Qty</td>
                <td class="tg-0pky">Hevea Price</td>
                <td class="tg-0pky">Total Hevea Price</td>
            </tr>
            <?php //dd($repolish->all()); ?> 
            <tr>
                <td class="tg-0pky">1</td>
                <td class="tg-0pky">{{ $quotation->product == '1' ? "Dining Chair" : "Dining Table" }}</td>
                <td class="tg-0pky"> <img src="{{ asset('public/storage/product/'.$quotation->image) }}" style="width: 50px;" alt="">({{$width[0]->sub_setting ?? '0'}} x {{$depth[0]->sub_setting ?? '0'}} x {{$height[0]->sub_setting ?? '0'}}) </td>
                <td class="tg-0pky">{{ $rework[0]->additional_detail ?? 'No Specific Requirment' }}</td>
                <td class="tg-0pky">{{ $repolish[0]->sub_setting ?? '' }}</td>
                <td class="tg-0pky">Wrapping</td>
                <td class="tg-0pky"></td>
                <td class="tg-0pky">{{ $quotation->total }}</td>
                <td class="tg-0pky">{{ $quotation->total }}</td>
            </tr>
            <tr>
                <td class="tg-0pky text-right" colspan="8">Discount Value ({{ $quotation->discount }}%)</td>
                <td class="tg-0pky ">{{ $quotation->discount == 0 ? "0" : (($quotation->total * $quotation->discount) / 100) }}</td>
            </tr>
            <tr>
                <td class="tg-0pky text-right" colspan="8">Special Discount</td>
                <td class="tg-0pky">{{ $quotation->special_discount == 0 ? "0" : ( $quotation->total - $quotation->sub_total ) }}</td>
            </tr>
            <tr>
                <td class="tg-0pky text-right" colspan="8">Total Quotation Value</td>
                <td class="tg-0pky">{{ $quotation->payable_amount }}</td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="2" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;"><b>Please Note </b></td>
                <td class="tg-0pky" colspan="7" style="border-left: 0px; border-top: 0px; border-bottom: 0px;"></td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="2" style="border-right: 0px; border-top: 0px; border-bottom: 0px;">Payment Terms :</td>
                <td class="tg-0pky" colspan="7" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">50% Advance along with PO, Balance 50% Payment after Inspection at our Factory and before Delivery.</td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="2" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Delivery Period :</td>
                <td class="tg-0pky" colspan="7" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">2 to 3 Weeks from the Date of Advance Received.</td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="2" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Transport :</td>
                <td class="tg-0pky" colspan="7" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">Free pickup of Old furniure & Delivery of Refurbished furniture.</td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="2" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Payment :</td>
                <td class="tg-0pky" colspan="7" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">Payment in Favour of Hevea Furniture & Interiors Pvt.Ltd</td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="2" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Our Bank Details :</td>
                <td class="tg-0pky" colspan="7" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">Karnataka Bank Ltd., Catherdral Road Branch, Chennai - 600 086, A/c. No. 4647000700012801(OD Account) - IFSC Code - KARB0000464.</td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="2" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px;">Validity of Quotation :</td>
                <td class="tg-0pky" colspan="7" style="border-left: 0px; border-top: 0px; border-bottom: 0px;">One Month from the Date of Quotation.</td>
            </tr>
            <tr>
                <td class="tg-0pky" colspan="2" style="border-right: 0px solid; border-top: 0px; border-bottom: 0px solid #fff;">GSTIN:</td>
                <td class="tg-0pky" colspan="7" style="border-left: 0px; border-top: 0px; border-bottom: 0px solid #fff;">33AAACH1878A1Z2</td>
            </tr>
        </tbody>
    </table>
    <table style="border-bottom: 1px solid rgb(0, 0, 0)er-left: 1px solid #000; border-right: 1px solid #000; border-top: 0px solid #fff;">
        <tbody>
            <tr>
                <td class="tg-0pky text-center" style="width: 40%;">
                    <img src="{{ asset('public/storage/logo/hevea.png') }}">
                    <!--style="width: 544px; height:60px; "-->
                </td>
                <td class="tg-0pky" style="width: 60%;">
                    1/183, Sri Selvavinayagar Nagar, Payasampakkam Village,
                    Red Hills, Chennai, Tamil Nadu 600052
                    Email: customercare@7hillfurnrefurbish.com | Phone: +91 73388 44717, +91 73388 55757
                </td>
            </tr>
        </tbody>
    </table>

</body>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>

</html>