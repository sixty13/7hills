@auth
  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
  </form>
@endauth

<!-- Scripts -->
<script src="{{ asset('public/js/manifest.js') }}"></script>
<script src="{{ asset('public/js/vendor.js') }}"></script>
