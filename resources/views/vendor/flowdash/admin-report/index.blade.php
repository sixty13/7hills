@extends("flowdash::layouts.fluid", ['title' => 'USERS'])

@section('content')
<div class="page__header">
    <div class="container page__heading-container">
        <div class="page__heading d-flex align-items-center">
            <div class="flex">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0">
                        <li class="breadcrumb-item"><a href="{{ url('/') }}"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Reports</li>
                    </ol>
                </nav>
                <h1 class="m-0">REPORTS</h1>
            </div>
            {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal" data-whatever="@mdo" onclick="$('#createModal').find('input:not([type=hidden]),textarea,select').val('');$('#btn_name').text('Add');">Add staff</button>
            <button type="button" id="staff_create_update" class="d-none btn btn-primary" data-toggle="modal" data-target="#createModal" data-whatever="@mdo">Add staff</button> --}}
        </div>
    </div>
</div>
<div class="container page__container">
    <div class="card card-form">
        <div class="row no-gutters">
            <div class="col-lg-12 card-form__body">
                <div class="table-responsive border-bottom" data-toggle="lists" data-lists-values="[&quot;js-lists-values-employee-name&quot;]">
                    <?php //dd($quotations[0]->payable_amount); ?>
                    <table class="table mb-0 thead-border-top-0">
                        <thead>
                            <tr>
                                <th>SNO</th>
                                <th>F.NAME</th>
                                <th>L.NAME</th>
                                <th>EMAIL</th>
                                <th>MOBILE NO</th>
                                <th>QUOTATION</th>
                                <th>St.DIS.</th>
                                <th>SA DIS.</th>
                                @if ($user->user_type == 1)
                                    <th style="text-align: center;">DISC</th>
                                    <th style="text-align: center;">PDF GEN</th> 
                                @endif
                                <th>ACTIONS</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if( count($quotations) > 0)
                            @foreach($quotations as $k=>$quotation)
                            <tr>
                                <?php //echo "<pre>"; print_r($quotation->payable_amount); ?>
                                <td name="sno">{{ $k + 1 }}</td>
                                <td name="firestName">{{ $quotation->customer->first_name }}</td>
                                <td name="lastName">{{  $quotation->customer->last_name }}</td>
                                <td name="email">{{  $quotation->customer->email }}</td>
                                <td name="mobileNo"><small>{{  $quotation->customer->mobile_no }}</small></td>
                                <td name="quotation" id="quotation_{{$quotation->id}}">{{ $quotation->payable_amount }}</td>
                                <td name="staff_disc">{{ $quotation->discount }}</td>
                                <td name="sa_disc" id="sa_disc_{{$quotation->id}}">{{ $quotation->special_discount }}</td>
                                @if ($user->user_type == 1)
                                    <form id="sp_discount"  onsubmit="return false;">
                                        <td name="special_discount"><input type="number" style="width: 60px;" name="sp_disc" id="{{ $quotation->id }}_sp_disc" value="0" class="sa_disc_{{$k}}"></td>
                                        <td name="pdf_gen"><button type="submit" id="regenerate_{{ $quotation->id }}" class="btn btn-primary regenerate" data-id="{{ $quotation->id }}"><span class="d-none" id="loader_{{ $quotation->id }}"><i class="fa fa-spinner fa-spin"></i></span><span id="regenerate_button_{{ $quotation->id }}">REGENERATE</span></button></td>
                                    </form>
                                @endif
                                <td><button type="submit" id="resend_{{ $quotation->id }}" data-id="{{ $quotation->id }}" class="btn btn-primary resend"><span class="d-none" id="resend_loader_{{ $quotation->id }}"><i class="fa fa-spinner fa-spin"></i></span><span id="resend_button_{{ $quotation->id }}">{{ $quotation->resend == '1' ? 'RESEND' : 'SEND' }}</span></button></td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="<?php echo $user->user_type == 1 ? 9 : 7  ?>" style="text-align: center;">
                                    No Report found
                                </td>
                            </tr>
                            @endif 
                        </tbody>
                    </table>
                    <div class="col-md-12">
                        @if( count($quotations) > 0)
                            {{ $quotations->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<button id="snackbutton" hidden onclick="myFunction()">Show Snackbar</button>

<div id="snackbar"><span id="display_msg"></span></div>

{{-- <link href="{{ asset('public/css/jquery.toast.min.css') }}" rel="stylesheet" /> --}}
<script>
    function myFunction() {
        var x = document.getElementById("snackbar");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }

    $('.regenerate').on('click', function() {
        var id = $(this).attr('data-id');
        var disc = $("#"+id+"_sp_disc").val();
        if(disc == 0) {
            return true;
        }

        $("#loader_"+id).removeClass("d-none");
        $("#regenerate_button_"+id).addClass("d-none");
        $("#regenerate_"+id).attr("disabled", true);
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            url: url+'/user/pdf/regenerate/'+id+'?sp_disc='+disc,
            type: 'GET',
            success: function(data)
            {
                console.log("data", data);
                $("#loader_"+id).addClass("d-none");
                $("#regenerate_button_"+id).removeClass("d-none");
                $("#regenerate_"+id).removeAttr("disabled", false);
                $("#quotation_"+id).text(data.payable_amount.toFixed(2));
                $("#sa_disc_"+id).text(data.discount);
                $("input[name='sp_disc']").val('0');
                $("#display_msg").text(data.msg);
                $('#snackbutton').click();
            },
        });
    });

    $('.resend').on('click', function() {
            console.log("herer");
            var id = $(this).attr('data-id');

            $("#resend_loader_"+id).removeClass("d-none");
            $("#resend_button_"+id).addClass("d-none");
            $("#resend_"+id).attr("disabled", true);

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
            url: url+'/user/pdf/resend/'+id,
            type: 'GET',
            success: function(data)
            {
                $("#resend_loader_"+id).addClass("d-none");
                $("#resend_button_"+id).removeClass("d-none");
                $("#resend_"+id).removeAttr("disabled", false);
                $("#resend_button_"+id).text(data.resend);
                $("#display_msg").text(data.msg);
                $('#snackbutton').click();
            },
        });
    });
</script>
@endsection