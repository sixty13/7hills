@extends("flowdash::layouts.blank", [
  'bodyClass' => 'layout-login'
])

@section('content')
{{-- <x-guest-layout> --}}
<div class="layout-login__overlay layout-login" ></div>
<div class="layout-login__form bg-white" data-perfect-scrollbar style="margin-left: auto;">    
    <x-jet-authentication-card>
        <x-slot name="logo">
            {{-- <x-jet-authentication-card-logo /> --}}
            <img src='{{ asset('public/storage/logo/7Hill_Logo.jpg') }}' style="width: 100%;">  
        </x-slot>

        {{-- <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-5 navbar-light">
        </div> --}}
        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="alert alert-soft-success">
                <strong class="text-body">{{ session('status') }}</strong>
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email') ?? '' " required autofocus placeholder="{{ __('Enter your email') }}" />
            </div>

            <div class="form-group">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" placeholder="{{ __('Enter your password') }}" />
            </div>

            <div class="form-group d-flex align-items-center">
                <div class="flex">
                    <input class="mr-1" type="checkbox" name="remember" id="remember_me" {{ old('remember') ? 'checked' : '' }}>
                    <label class="form-check-label text-label" for="remember_me">
                        {{ __('Remember me') }}
                    </label>
                </div>
                <div>
                    @if (Route::has('password.request'))
                        <a class="text-body text-underline" href="{{ route('password.request') }}">
                            {{ __('Forgot your password?') }}
                        </a>
                    @endif
                </div>
            </div>

            <div class="d-flex flex-column text-center">
                <p>
                    <x-jet-button class="btn btn-primary">
                        {{ __('Login') }}
                    </x-jet-button>
                </p>
            </div>
        </form>
    </x-jet-authentication-card>
</div>
{{-- </x-guest-layout> --}}
@endsection
