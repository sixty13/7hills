-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 24, 2021 at 09:29 AM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `7hills_inspection_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `mobile_no` bigint(10) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `sid` int(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `customers`
--

TRUNCATE TABLE `customers`;
--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `email`, `mobile_no`, `address`, `sid`, `created_at`, `updated_at`) VALUES
(1, 'vrunda ', 'parekh', 'vrunda@sixty13.com', 9898989898, 'test', 12, '2021-02-11 11:07:17', '2021-02-11 11:07:17'),
(2, 'dimpi01', 'gandhi01', 'customer@gmail.com', 9876543210, 'tsts101', 13, '2021-02-16 09:54:20', '2021-02-16 09:54:20'),
(3, 'Vid', 'Test', 'vidtest@gmail.com', 7458963210, 'shhsjwozjsjwmmwpqpskxjdnrm', 13, '2021-02-16 10:09:16', '2021-02-16 10:09:16'),
(4, 'hshsbs', 'gssbbs', 'isjshshs@gmail.com', 6491046645, 'hu<jhshs', 13, '2021-02-17 05:59:01', '2021-02-17 05:59:01'),
(5, 'gshsh', 'gdshhs', 'gandhidimpi123@gmail.com', 9876543210, 'dhdhdjdj', 13, '2021-02-17 06:13:46', '2021-02-17 06:13:46'),
(6, 'hdhd', 'hdhdh', 'gajjsk@gmail.com', 9876543210, 'gshshshh', 13, '2021-02-17 06:33:15', '2021-02-17 06:33:15'),
(7, 'fuffuh', 'cjvjjj', 'gandhidimpi123@gmail.com', 9876543210, 'hhnxnnxnxxjk', 14, '2021-02-17 07:47:18', '2021-02-17 07:47:18'),
(8, 'ggfhhf', 'bdhdndjj', 'gandhidimpi123@gmail.com', 9876543210, 'hdhdhsjsisis', 14, '2021-02-17 07:51:22', '2021-02-17 07:51:22'),
(9, 'yy', 'fgggghh', 'gandhidimpi123@gmail.com', 9876543210, 'ghahahahah', 14, '2021-02-17 07:53:51', '2021-02-17 07:53:51'),
(10, 'ggggh', 'hhhhh', 'gandhidimpi123@gmail.com', 9876543210, 'dddddddd hhghj', 14, '2021-02-17 08:02:37', '2021-02-17 08:02:37'),
(11, 'hdhhd', 'behjsj', 'gandhidimpi123@gmail.com', 9846543210, 'vehjejsk', 14, '2021-02-17 08:07:19', '2021-02-17 08:07:19'),
(12, 'hhzh', 'hddhhsh', 'gandhidimpi123@gmail.com', 9876543210, 'dudjjdjdj', 13, '2021-02-17 08:23:05', '2021-02-17 08:23:05'),
(13, 'hdhdh', 'gsdhu', 'gandhidimpi123@gmail.com', 9876543210, 'hshshsjj', 13, '2021-02-17 09:01:26', '2021-02-17 09:01:26'),
(14, 'sbbzv', 'ushsv', 'owbdbdb', 31820204976, 'jehebrjsjsola', 13, '2021-02-17 09:36:55', '2021-02-17 09:36:55'),
(15, 'sbbzv', 'ushsv', 'owbdbdb', 31820204976, 'jehebrjsjsola', 13, '2021-02-17 09:49:09', '2021-02-17 09:49:09'),
(16, 'sbbzv', 'ushsv', 'owbdbdb', 31820204976, 'jehebrjsjsola', 13, '2021-02-17 09:57:50', '2021-02-17 09:57:50'),
(17, 'bhdhe', 'bdbdh', 'ghs@gmail.com', 9876543210, 'hshshsj', 13, '2021-02-17 10:20:50', '2021-02-17 10:20:50'),
(18, 'bdbhd', 'hshsj', 'gandhidimpi123@gmail.com', 9876543210, 'jsjsjsjk', 13, '2021-02-17 10:56:38', '2021-02-17 10:56:38'),
(19, 'ghshs', 'gshsh', 'gandhidimpi123@gmail.com', 9876543210, 'bhsjjsj', 13, '2021-02-17 11:00:24', '2021-02-17 11:00:24'),
(20, 'dffh', 'gHhH', 'test@gmail.com', 9876543210, 'vHbNNnjjbbb', 14, '2021-02-18 04:38:15', '2021-02-18 04:38:15'),
(21, 'ggggg', 'vvvhnjn', 'gss@gmail.com', 9876543210, 'fgghjkk', 14, '2021-02-18 04:48:40', '2021-02-18 04:48:40'),
(22, 'hhh', 'vgh', 'gHhJ', 9876543210, 'vzhhz', 14, '2021-02-18 05:00:34', '2021-02-18 05:00:34'),
(23, 'mohit', 'patel', 'fjdy@gmail.com', 9586407439, 'test add', 14, '2021-02-18 05:12:09', '2021-02-18 05:12:09'),
(24, 'ggg', 'vvv', 'vhjj@gmail.com', 9876543210, 'ggvh', 14, '2021-02-18 05:46:23', '2021-02-18 05:46:23'),
(25, 'shzh', 'hxhdhxh', 'agaga@fndf.com', 9865325689, 'hgzhdhshsh', 14, '2021-02-18 06:29:16', '2021-02-18 06:29:16'),
(26, 'hzzhhz', 'zhzhzhj', 'gandhidimpi123@gmail.com', 9876543210, 'bznznznzjz', 14, '2021-02-18 07:54:56', '2021-02-18 07:54:56'),
(27, 'gautam', 'kakadiya', 'gautam@gmail.com', 9586407439, 'test add', 11, '2021-02-18 10:03:53', '2021-02-18 10:03:53'),
(28, 'gautam', 'kakadiya', 'gautam@gmail.com', 9586407439, 'test add', 11, '2021-02-18 10:31:27', '2021-02-18 10:31:27'),
(29, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 10:38:23', '2021-02-18 10:38:23'),
(30, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 10:41:21', '2021-02-18 10:41:21'),
(31, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 10:43:23', '2021-02-18 10:43:23'),
(32, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 10:47:39', '2021-02-18 10:47:39'),
(33, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 10:47:53', '2021-02-18 10:47:53'),
(34, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 10:48:14', '2021-02-18 10:48:14'),
(35, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 10:53:14', '2021-02-18 10:53:14'),
(36, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 10:53:36', '2021-02-18 10:53:36'),
(37, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 11:05:43', '2021-02-18 11:05:43'),
(38, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 11:05:59', '2021-02-18 11:05:59'),
(39, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 11:07:11', '2021-02-18 11:07:11'),
(40, 'Dimpi', 'Gandhi', 'gandhidimpi123@gmail.com', 9876543210, 'hah hah bdjdj', 14, '2021-02-18 11:07:36', '2021-02-18 11:07:36'),
(41, 'gshshf', 'xvxbxn', 'gandhidimpi123@gmail.com', 9876543210, 'xbbzbdbdbdb', 14, '2021-02-18 12:22:55', '2021-02-18 12:22:55'),
(42, 'zhhsj', 'vzbzh', 'gandhidimpi123@gmail.com', 9876543210, 'gzhhznaj', 14, '2021-02-18 12:38:45', '2021-02-18 12:38:45'),
(43, 'zhhsj', 'vzbzh', 'gandhidimpi123@gmail.com', 9876543210, 'gzhhznaj', 14, '2021-02-18 12:39:22', '2021-02-18 12:39:22'),
(44, 'bbb', 'vvb', 'dimpi@gmail.com', 9876543210, 'bznzn', 14, '2021-02-19 07:53:14', '2021-02-19 07:53:14'),
(45, 'hxhzh', 'bzbxhxh', 'gandhidimpi123@gmail.com', 9876543210, 'vxbxhzhzjsjsjsjkk', 14, '2021-02-23 04:15:27', '2021-02-23 04:15:27'),
(46, 'hxhzh', 'bzbxhxh', 'gandhidimpi123@gmail.com', 9876543210, 'vxbxhzhzjsjsjsjkk', 14, '2021-02-23 04:16:03', '2021-02-23 04:16:03'),
(47, 'bxbxbn', 'xhhxxhh', 'gandhidimpi123@gmail.com', 9876543210, 'bxbxbxbxbxbnnn', 14, '2021-02-23 04:24:21', '2021-02-23 04:24:21'),
(48, 'ghh', 'vhhjk', 'gggg@gmail.com', 9876543210, 'bxhxhxhxhzhj', 14, '2021-02-23 04:54:21', '2021-02-23 04:54:21'),
(49, 'bdbdhs', 'vzvshsh', 'vzbzbn@gmail.com', 9876543210, 'bxbxbznznz', 14, '2021-02-23 05:36:16', '2021-02-23 05:36:16'),
(50, 'hxbx', 'bxbdb', 'gandhidimpi123@gmail.com', 9876543210, 'bxnxnxn', 14, '2021-02-23 06:16:13', '2021-02-23 06:16:13'),
(51, 'vxbzbz', 'bbzzb', 'gab@gmail.com', 9876543210, 'bxbxbxbbbbn', 14, '2021-02-23 06:30:45', '2021-02-23 06:30:45'),
(52, 'ggh', 'vhh', 'gandhidimpi123@gmail.com', 9876543210, 'vxhxh', 14, '2021-02-23 06:32:32', '2021-02-23 06:32:32'),
(53, 'bdh', 'vdhdh', 'ganan@gmail.com', 9876543210, 'hznsn', 14, '2021-02-23 08:20:46', '2021-02-23 08:20:46'),
(54, 'xnxnx', 'xbbxb', 'bxbxnn@gmail.com', 9876543210, 'bzbzbzn', 14, '2021-02-23 09:13:26', '2021-02-23 09:13:26'),
(55, 'bxbxb', 'hxhxjx', 'gandhidimpi123@gmail.com', 9876543210, 'bxbxxbn', 14, '2021-02-23 09:29:33', '2021-02-23 09:29:33'),
(56, 'gsgs', 'hshshs', 'gandhidimpi123@gmail.com', 9876543210, 'jjdjdk', 14, '2021-02-23 10:05:41', '2021-02-23 10:05:41'),
(57, 'xbxbbx', 'bxxnn', 'nxjx', 9876543210, 'xbxnn', 14, '2021-02-23 10:30:57', '2021-02-23 10:30:57'),
(58, 'gautam', 'kakdiya', 'kakdiya.gautam288@gmail.com', 9428854599, 'test address', 14, '2021-02-23 11:22:01', '2021-02-23 11:22:01'),
(59, 'mohit', 'patel', 'mohitmangukia@gmail.com', 9586407439, 'test', 14, '2021-02-23 11:33:01', '2021-02-23 11:33:01'),
(60, 'mohit', 'mangukia', 'mohitmangukia@gmail.com', 9586407439, 'test add', 14, '2021-02-23 11:43:33', '2021-02-23 11:43:33');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `failed_jobs`
--

TRUNCATE TABLE `failed_jobs`;
-- --------------------------------------------------------

--
-- Table structure for table `gst`
--

CREATE TABLE `gst` (
  `id` int(11) NOT NULL,
  `gst` varchar(191) NOT NULL,
  `percentage` int(3) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: de active\r\n1: active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `gst`
--

TRUNCATE TABLE `gst`;
--
-- Dumping data for table `gst`
--

INSERT INTO `gst` (`id`, `gst`, `percentage`, `active`, `created_at`, `updated_at`) VALUES
(1, 'CGST', 5, 1, '2021-02-14 07:16:25', '2021-02-14 02:07:07'),
(2, 'SGST', 12, 1, '2021-02-14 07:22:41', '2021-02-14 07:22:41'),
(3, 'IGST', 3, 1, '2021-02-14 07:22:41', '2021-02-14 07:22:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2018_08_08_100000_create_telescope_entries_table', 1),
(5, '2019_08_19_000000_create_failed_jobs_table', 1),
(6, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(7, '2020_11_03_103054_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_verification`
--

CREATE TABLE `mobile_verification` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `mobile_number` varchar(50) CHARACTER SET utf8 NOT NULL,
  `code` int(11) NOT NULL,
  `verified` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `mobile_verification`
--

TRUNCATE TABLE `mobile_verification`;
--
-- Dumping data for table `mobile_verification`
--

INSERT INTO `mobile_verification` (`id`, `customer_id`, `mobile_number`, `code`, `verified`, `created_at`, `updated_at`) VALUES
(24, NULL, '9428854599', 158261, 0, '2021-02-19 12:43:51', '2021-02-19 12:43:51'),
(11, NULL, '9865986598', 674381, 1, '2021-02-16 11:25:47', '2021-02-16 11:25:47'),
(28, NULL, '9876543210', 514113, 1, '2021-02-22 09:48:40', '2021-02-22 09:48:40'),
(21, NULL, '9994172111', 698131, 0, '2021-02-19 10:35:41', '2021-02-19 10:35:41'),
(150, NULL, '9725662777', 694692, 1, '2021-02-23 10:30:42', '2021-02-23 10:30:42'),
(63, NULL, '7405280035', 935649, 0, '2021-02-23 09:35:56', '2021-02-23 09:35:56'),
(93, NULL, '9033733578', 948816, 0, '2021-02-23 09:59:01', '2021-02-23 09:59:01'),
(153, NULL, '9586407439', 198638, 1, '2021-02-23 11:42:55', '2021-02-23 11:42:55');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `personal_access_tokens`
--

TRUNCATE TABLE `personal_access_tokens`;
--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 2, 'staff-api', '01c4b078f9d5e5284de5b6eef85c31312aa584a872a620fe17d8d213b6713e1d', '[\"*\"]', NULL, '2021-02-06 03:58:19', '2021-02-06 03:58:19'),
(2, 'App\\Models\\User', 2, 'staff-api', '14cb319475f29e4974c4e3969b24b6ca69cc4af2759f7ed22738abd1631dcc8b', '[\"*\"]', NULL, '2021-02-06 04:00:12', '2021-02-06 04:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL COMMENT 'customer id',
  `sid` int(11) NOT NULL COMMENT 'staff id',
  `total` int(11) NOT NULL,
  `sub_total` int(11) NOT NULL,
  `cgst` float(11,2) NOT NULL,
  `sgst` float(11,2) NOT NULL,
  `igst` float(11,2) NOT NULL,
  `payable_amount` float(11,2) NOT NULL,
  `discount` int(11) NOT NULL,
  `special_discount` int(3) NOT NULL DEFAULT '0',
  `product` int(11) NOT NULL COMMENT '0: dining table\r\n\r\n1: dining chair',
  `image` varchar(200) DEFAULT NULL,
  `pdf` varchar(191) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `quotation`
--

TRUNCATE TABLE `quotation`;
--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `cid`, `sid`, `total`, `sub_total`, `cgst`, `sgst`, `igst`, `payable_amount`, `discount`, `special_discount`, `product`, `image`, `pdf`, `created_at`, `updated_at`) VALUES
(2, 27, 11, 7370, 328, 16.38, 39.32, 9.83, 393.23, 20, 284, 1, '4e19b79b-1127-4a7b-9582-61d64d2156068672448736538481947_1614072426.jpg', '271613642985.pdf', '2021-02-18 10:09:45', '2021-02-24 09:18:24'),
(17, 47, 14, 600, 540, 27.00, 64.80, 16.20, 648.00, 10, 0, 0, NULL, '471614054795.pdf', '2021-02-23 04:33:15', '2021-02-23 04:33:15'),
(18, 50, 14, 0, 0, 0.00, 0.00, 0.00, 0.00, 10, 0, 0, '501614061009', '501614061009.pdf', '2021-02-23 06:16:49', '2021-02-23 06:16:49'),
(19, 54, 14, 580, 522, 26.10, 62.64, 15.66, 626.40, 10, 0, 0, '4e19b79b-1127-4a7b-9582-61d64d2156068672448736538481947_1614072426.jpg', '541614072514.pdf', '2021-02-23 09:28:34', '2021-02-23 09:28:34'),
(20, 56, 14, 130, 117, 5.85, 14.04, 3.51, 140.40, 10, 0, 0, 'ac5cd0c1-0e92-4f2a-a0b8-4577a94a2efd2903991556459830410_1614074758.jpg', '561614074764.pdf', '2021-02-23 10:06:04', '2021-02-23 10:06:04'),
(21, 56, 14, 130, 117, 5.85, 14.04, 3.51, 140.40, 10, 0, 0, 'ac5cd0c1-0e92-4f2a-a0b8-4577a94a2efd2903991556459830410_1614074758.jpg', '561614074767.pdf', '2021-02-23 10:06:07', '2021-02-23 10:06:07'),
(22, 57, 14, 600, 540, 27.00, 64.80, 16.20, 648.00, 10, 0, 0, '96a50fc0-8e6a-4c04-8878-06f7ac2e9c993544021616262489303_1614076285.jpg', '571614076288.pdf', '2021-02-23 10:31:28', '2021-02-23 10:31:28'),
(23, 58, 14, 0, 0, 0.00, 0.00, 0.00, 0.00, 10, 0, 0, '58fbe87a-014a-4520-9607-2a03e6496f9a365362696846751939_1614079361.jpg', '581614079369.pdf', '2021-02-23 11:22:49', '2021-02-23 11:22:49'),
(24, 59, 14, 1830, 1647, 82.35, 197.64, 49.41, 1976.40, 10, 0, 1, 'bd21301a-e3d2-40c9-a00a-59b222723a84336512621937966341_1614080004.jpg', '591614080016.pdf', '2021-02-23 11:33:36', '2021-02-23 11:33:36'),
(26, 60, 14, 0, 0, 0.00, 0.00, 0.00, 0.00, 5, 0, 0, '93724557-52c7-4b6c-b629-059f422b84a54023869673858016447_1614080656.jpg', '601614080665.pdf', '2021-02-23 11:44:25', '2021-02-23 11:44:25');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details`
--

CREATE TABLE `quotation_details` (
  `id` int(11) NOT NULL,
  `qid` int(3) NOT NULL COMMENT 'quotation id',
  `setting` varchar(191) NOT NULL,
  `sid` int(3) NOT NULL,
  `sub_setting` varchar(191) DEFAULT NULL,
  `ssid` int(3) NOT NULL DEFAULT '0',
  `additional_detail` varchar(500) DEFAULT NULL,
  `price` int(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `quotation_details`
--

TRUNCATE TABLE `quotation_details`;
--
-- Dumping data for table `quotation_details`
--

INSERT INTO `quotation_details` (`id`, `qid`, `setting`, `sid`, `sub_setting`, `ssid`, `additional_detail`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 'Full Carving', 1, 'Larged legs', 48, NULL, 500, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(2, 1, 'Seat Height', 2, '30', 5, NULL, 605, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(3, 1, 'Back Height', 3, '25', 6, NULL, 50, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(4, 1, 'Seat Width', 4, '30', 9, NULL, 60, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(5, 1, 'Seat Depth', 5, '25', 13, NULL, 30, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(6, 1, 'Frame Shaking', 6, NULL, 0, NULL, 100, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(7, 1, 'No of Cracks in Frame', 7, NULL, 0, '5', 300, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(8, 1, 'No of Breakages', 8, NULL, 0, '6', 120, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(9, 1, 'Repolish required', 9, NULL, 0, NULL, 1105, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(10, 1, 'Colors to be channged', 10, 'blue', 26, NULL, 60, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(11, 1, 'Polish Type required', 11, 'Exterior PU', 19, NULL, 1165, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(12, 1, 'Rubber Cap Required', 12, NULL, 0, NULL, 70, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(13, 1, 'Seat Ply to be changed', 13, NULL, 0, NULL, 90, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(14, 1, 'Seat Foam to be changed', 14, NULL, 0, NULL, 90, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(15, 1, 'Seat Cover to be changed', 15, 'Leather', 22, NULL, 500, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(16, 1, 'Non Woven for bottom', 16, NULL, 0, NULL, 30, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(17, 1, 'Back Ply to be changed', 17, NULL, 0, NULL, 665, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(18, 1, 'Back Foam to be changed', 18, NULL, 0, NULL, 665, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(19, 1, 'Back Cover to be Changed', 19, 'Fabric', 23, NULL, 665, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(20, 1, 'Cover material', 21, 'PU', 46, NULL, 150, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(21, 1, 'Any Specific requirement', 22, NULL, 0, 'test requirment', 0, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(22, 1, 'Other Additional Cost', 23, NULL, 0, NULL, 50, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(23, 1, 'Missing Parts', 20, NULL, 0, 'missing parts', 300, '2021-02-18 10:08:40', '2021-02-18 10:08:40'),
(24, 2, 'Full Carving', 1, 'Larged legs', 48, NULL, 500, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(25, 2, 'Seat Height', 2, '30', 5, NULL, 605, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(26, 2, 'Back Height', 3, '25', 6, NULL, 50, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(27, 2, 'Seat Width', 4, '30', 9, NULL, 60, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(28, 2, 'Seat Depth', 5, '25', 13, NULL, 30, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(29, 2, 'Frame Shaking', 6, NULL, 0, NULL, 100, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(30, 2, 'No of Cracks in Frame', 7, NULL, 0, '5', 300, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(31, 2, 'No of Breakages', 8, NULL, 0, '6', 120, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(32, 2, 'Repolish required', 9, NULL, 0, NULL, 1105, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(33, 2, 'Colors to be channged', 10, 'blue', 26, NULL, 60, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(34, 2, 'Polish Type required', 11, 'Exterior PU', 19, NULL, 1165, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(35, 2, 'Rubber Cap Required', 12, NULL, 0, NULL, 70, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(36, 2, 'Seat Ply to be changed', 13, NULL, 0, NULL, 90, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(37, 2, 'Seat Foam to be changed', 14, NULL, 0, NULL, 90, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(38, 2, 'Seat Cover to be changed', 15, 'Leather', 22, NULL, 500, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(39, 2, 'Non Woven for bottom', 16, NULL, 0, NULL, 30, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(40, 2, 'Back Ply to be changed', 17, NULL, 0, NULL, 665, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(41, 2, 'Back Foam to be changed', 18, NULL, 0, NULL, 665, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(42, 2, 'Back Cover to be Changed', 19, 'Fabric', 23, NULL, 665, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(43, 2, 'Cover material', 21, 'PU', 46, NULL, 150, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(44, 2, 'Any Specific requirement', 22, NULL, 0, 'test requirment', 0, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(45, 2, 'Other Additional Cost', 23, NULL, 0, NULL, 50, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(46, 2, 'Missing Parts', 20, NULL, 0, 'missing parts', 300, '2021-02-18 10:09:45', '2021-02-18 10:09:45'),
(47, 3, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-18 10:53:26', '2021-02-18 10:53:26'),
(48, 3, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-18 10:53:26', '2021-02-18 10:53:26'),
(49, 4, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 11:20:04', '2021-02-18 11:20:04'),
(50, 4, 'Seat Foam to be changed', 14, 'NULL', 0, NULL, 50, '2021-02-18 11:20:04', '2021-02-18 11:20:04'),
(51, 5, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 11:27:08', '2021-02-18 11:27:08'),
(52, 5, 'Seat Foam to be changed', 14, 'NULL', 0, NULL, 50, '2021-02-18 11:27:08', '2021-02-18 11:27:08'),
(53, 6, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 11:34:37', '2021-02-18 11:34:37'),
(54, 6, 'Seat Foam to be changed', 14, 'NULL', 0, NULL, 50, '2021-02-18 11:34:37', '2021-02-18 11:34:37'),
(55, 7, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 11:34:42', '2021-02-18 11:34:42'),
(56, 7, 'Seat Foam to be changed', 14, 'NULL', 0, NULL, 50, '2021-02-18 11:34:42', '2021-02-18 11:34:42'),
(57, 8, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 11:48:15', '2021-02-18 11:48:15'),
(58, 8, 'Seat Foam to be changed', 14, 'NULL', 0, NULL, 50, '2021-02-18 11:48:15', '2021-02-18 11:48:15'),
(59, 9, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 11:53:19', '2021-02-18 11:53:19'),
(60, 9, 'Seat Foam to be changed', 14, 'NULL', 0, NULL, 50, '2021-02-18 11:53:19', '2021-02-18 11:53:19'),
(61, 10, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 11:53:24', '2021-02-18 11:53:24'),
(62, 10, 'Seat Foam to be changed', 14, 'NULL', 0, NULL, 50, '2021-02-18 11:53:24', '2021-02-18 11:53:24'),
(63, 11, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-18 11:59:22', '2021-02-18 11:59:22'),
(64, 11, 'Back Height', 3, '25', 6, NULL, 50, '2021-02-18 11:59:22', '2021-02-18 11:59:22'),
(65, 12, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-18 12:10:30', '2021-02-18 12:10:30'),
(66, 12, 'Back Height', 3, '25', 6, NULL, 50, '2021-02-18 12:10:30', '2021-02-18 12:10:30'),
(67, 13, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-18 12:19:19', '2021-02-18 12:19:19'),
(68, 13, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-18 12:19:19', '2021-02-18 12:19:19'),
(69, 14, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 12:19:44', '2021-02-18 12:19:44'),
(70, 15, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-18 12:21:10', '2021-02-18 12:21:10'),
(71, 15, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-18 12:21:10', '2021-02-18 12:21:10'),
(72, 16, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-18 12:23:02', '2021-02-18 12:23:02'),
(73, 16, 'Back Height', 3, '25', 6, NULL, 50, '2021-02-18 12:23:02', '2021-02-18 12:23:02'),
(74, 17, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 04:33:15', '2021-02-23 04:33:15'),
(75, 17, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-23 04:33:15', '2021-02-23 04:33:15'),
(76, 18, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 06:16:49', '2021-02-23 06:16:49'),
(77, 18, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-23 06:16:49', '2021-02-23 06:16:49'),
(78, 19, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-23 09:28:34', '2021-02-23 09:28:34'),
(79, 19, 'Back Height', 3, '25', 6, NULL, 50, '2021-02-23 09:28:34', '2021-02-23 09:28:34'),
(80, 19, 'Seat Depth', 5, '25', 13, NULL, 30, '2021-02-23 09:28:34', '2021-02-23 09:28:34'),
(81, 20, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 10:06:04', '2021-02-23 10:06:04'),
(82, 20, 'Seat Depth', 5, '25', 13, NULL, 30, '2021-02-23 10:06:04', '2021-02-23 10:06:04'),
(83, 21, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 10:06:07', '2021-02-23 10:06:07'),
(84, 21, 'Seat Depth', 5, '25', 13, NULL, 30, '2021-02-23 10:06:07', '2021-02-23 10:06:07'),
(85, 22, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 10:31:28', '2021-02-23 10:31:28'),
(86, 22, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-23 10:31:28', '2021-02-23 10:31:28'),
(87, 23, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 11:22:49', '2021-02-23 11:22:49'),
(88, 24, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 11:33:36', '2021-02-23 11:33:36'),
(89, 24, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-23 11:33:36', '2021-02-23 11:33:36'),
(90, 24, 'Seat Depth', 5, '25', 13, NULL, 30, '2021-02-23 11:33:36', '2021-02-23 11:33:36'),
(91, 24, 'Polish Type required', 11, 'AC', 17, NULL, 600, '2021-02-23 11:33:36', '2021-02-23 11:33:36'),
(92, 24, 'Seat Cover to be changed', 15, 'Fabric', 20, NULL, 100, '2021-02-23 11:33:36', '2021-02-23 11:33:36'),
(93, 24, 'Back Cover to be Changed', 19, 'Fabric', 23, NULL, 500, '2021-02-23 11:33:36', '2021-02-23 11:33:36'),
(94, 25, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 11:33:40', '2021-02-23 11:33:40'),
(95, 25, 'Seat Height', 2, '25', 4, NULL, 500, '2021-02-23 11:33:40', '2021-02-23 11:33:40'),
(96, 25, 'Seat Depth', 5, '25', 13, NULL, 30, '2021-02-23 11:33:40', '2021-02-23 11:33:40'),
(97, 25, 'Polish Type required', 11, 'AC', 17, NULL, 600, '2021-02-23 11:33:40', '2021-02-23 11:33:40'),
(98, 25, 'Seat Cover to be changed', 15, 'Fabric', 20, NULL, 100, '2021-02-23 11:33:40', '2021-02-23 11:33:40'),
(99, 25, 'Back Cover to be Changed', 19, 'Fabric', 23, NULL, 500, '2021-02-23 11:33:40', '2021-02-23 11:33:40'),
(100, 26, 'Full Carving', 1, 'Carved Legs', 1, NULL, 100, '2021-02-23 11:44:25', '2021-02-23 11:44:25'),
(101, 26, 'Seat Width', 4, '25', 8, NULL, 50, '2021-02-23 11:44:25', '2021-02-23 11:44:25');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `sessions`
--

TRUNCATE TABLE `sessions`;
--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('HM8NAgTydHCtDwbKVtTwvZe6AJUfN9TN3A8ObfVa', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiTTM0NGtONUVBR0dsRnhKOVFXWmlUN3dtOXpTSjFZbjVyYnViRDd2bCI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyNToiaHR0cDovL2Zsb3dkYXNoLnRlc3QvaG9tZSI7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI1OiJodHRwOi8vZmxvd2Rhc2gudGVzdC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1612514168),
('yncBAwxRdkXo6jm7AAhz1nGmoTxuUPsHyEQ8c2Iy', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiQjNIUXYxQTI3V1lVMGc3WnJOYm5vME5oc1d5NTk5YW9HNWhkcjRaVyI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyNToiaHR0cDovL2Zsb3dkYXNoLnRlc3QvaG9tZSI7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI1OiJodHRwOi8vZmxvd2Rhc2gudGVzdC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1612514186);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: de active\r\n1: active',
  `selection` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: single\r\n1: multiple',
  `price` int(6) NOT NULL DEFAULT '0',
  `setting_type` tinyint(1) NOT NULL COMMENT '0: with subsettings and price\r\n1: with default price\r\n2: subsettings with name only\r\n3: subsettings with name, price and text box \r\n4: with no subsetting or default price \r\n5: textbox\r\n6: textarea',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `settings`
--

TRUNCATE TABLE `settings`;
--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `active`, `selection`, `price`, `setting_type`, `created_at`, `updated_at`) VALUES
(1, 'Full Carving', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-04 01:22:36'),
(2, 'Seat Height', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-04 03:12:50'),
(3, 'Back Height', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(4, 'Seat Width', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(5, 'Seat Depth', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(6, 'Frame Shaking', 1, 0, 100, 1, '2021-02-02 12:17:59', '2021-02-04 04:35:30'),
(7, 'No of Cracks in Frame', 1, 0, 60, 7, '2021-02-02 12:17:59', '2021-02-04 02:32:06'),
(8, 'No of Breakages', 1, 0, 20, 7, '2021-02-02 12:17:59', '2021-02-04 02:32:27'),
(9, 'Repolish required', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(10, 'Colors to be channged', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(11, 'Polish Type required', 1, 0, 0, 2, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(12, 'Rubber Cap Required', 1, 0, 70, 1, '2021-02-02 12:17:59', '2021-02-04 02:32:46'),
(13, 'Seat Ply to be changed', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-03 23:31:14'),
(14, 'Seat Foam to be changed', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(15, 'Seat Cover to be changed', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(16, 'Non Woven for bottom', 1, 0, 30, 1, '2021-02-02 12:17:59', '2021-02-04 02:32:53'),
(17, 'Back Ply to be changed', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(18, 'Back Foam to be changed', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(19, 'Back Cover to be Changed', 1, 0, 0, 2, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(21, 'Cover material', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(22, 'Any Specific requirement', 1, 0, 0, 6, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(23, 'Other Additional Cost', 1, 0, 0, 5, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(20, 'Missing Parts', 1, 0, 0, 3, '2021-02-04 11:53:22', '2021-02-04 11:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_esperanto_ci NOT NULL,
  `email` varchar(191) NOT NULL,
  `mobile_number` bigint(10) NOT NULL,
  `password` varchar(200) NOT NULL DEFAULT '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG' COMMENT 'password: 12345678',
  `designation` varchar(191) NOT NULL,
  `allowed_discount` int(3) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `staff`
--

TRUNCATE TABLE `staff`;
--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `first_name`, `last_name`, `email`, `mobile_number`, `password`, `designation`, `allowed_discount`, `created_at`, `updated_at`) VALUES
(11, 'Shreeram', 'VT', 'vtshreeram@gmail.com', 9994172111, '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG', 'Sr.Manager', 20, '2021-02-05 05:41:07', '2021-02-16 07:05:43'),
(14, 'john', 'doe', 'johndoe@gmail.com', 9586407439, '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG', 'Manager', 10, '2021-02-16 11:24:37', '2021-02-23 11:16:21'),
(13, 'Bala', 'Murugan', 'balamurugan@gmail.com', 7405280035, '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG', 'Senior Manager', 10, '2021-02-16 08:07:20', '2021-02-23 06:40:56');

-- --------------------------------------------------------

--
-- Table structure for table `sub_settings`
--

CREATE TABLE `sub_settings` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `price` float(6,2) NOT NULL,
  `setting_id` int(11) NOT NULL COMMENT 'Ref. to settings table',
  `delete_at` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: To not delete\r\n1: To delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `sub_settings`
--

TRUNCATE TABLE `sub_settings`;
--
-- Dumping data for table `sub_settings`
--

INSERT INTO `sub_settings` (`id`, `name`, `price`, `setting_id`, `delete_at`, `created_at`, `updated_at`) VALUES
(1, 'Carved Legs', 100.00, 1, 0, '2021-02-09 00:07:14', '2021-02-09 00:07:14'),
(49, 'xyz', 88.00, 10, 0, '2021-02-16 08:05:05', '2021-02-16 08:05:05'),
(4, '25', 500.00, 2, 0, '2021-02-09 00:07:33', '2021-02-16 08:04:02'),
(5, '30', 605.00, 2, 0, '2021-02-09 00:07:33', '2021-02-16 08:04:02'),
(6, '25', 50.00, 3, 0, '2021-02-09 00:07:42', '2021-02-09 00:07:42'),
(7, '30', 60.00, 3, 0, '2021-02-09 00:07:42', '2021-02-09 00:07:42'),
(13, '25', 30.00, 5, 0, '2021-02-09 00:09:58', '2021-02-09 00:09:58'),
(12, '25', 30.00, 5, 0, '2021-02-09 00:09:58', '2021-02-09 00:09:58'),
(8, '25', 50.00, 4, 0, '2021-02-09 00:08:38', '2021-02-09 00:08:38'),
(9, '30', 60.00, 4, 0, '2021-02-09 00:08:38', '2021-02-09 00:08:38'),
(14, 'red', 100.00, 10, 0, '2021-02-09 00:10:48', '2021-02-09 00:10:48'),
(15, 'white', 200.00, 10, 0, '2021-02-09 00:10:48', '2021-02-09 00:10:48'),
(16, 'green', 50.00, 10, 0, '2021-02-09 00:10:48', '2021-02-09 00:10:48'),
(17, 'AC', 0.00, 11, 0, '2021-02-09 00:11:22', '2021-02-09 00:11:22'),
(18, 'PU', 0.00, 11, 0, '2021-02-09 00:11:22', '2021-02-09 00:11:22'),
(19, 'Exterior PU', 0.00, 11, 0, '2021-02-09 00:11:22', '2021-02-09 00:11:22'),
(20, 'Fabric', 100.00, 15, 0, '2021-02-09 00:14:00', '2021-02-09 00:14:00'),
(21, 'PU', 250.00, 15, 0, '2021-02-09 00:14:00', '2021-02-09 00:14:00'),
(22, 'Leather', 500.00, 15, 0, '2021-02-09 00:14:00', '2021-02-09 00:14:00'),
(23, 'Fabric', 0.00, 19, 0, '2021-02-09 00:14:58', '2021-02-09 00:14:58'),
(24, 'PU', 0.00, 19, 0, '2021-02-09 00:14:58', '2021-02-09 00:14:58'),
(25, 'Leather', 0.00, 19, 0, '2021-02-09 00:14:58', '2021-02-09 00:14:58'),
(26, 'blue', 60.00, 10, 0, '2021-02-09 01:02:46', '2021-02-09 01:02:46'),
(47, 'Leather', 75.00, 21, 0, '2021-02-16 06:24:21', '2021-02-16 06:24:21'),
(46, 'PU', 150.00, 21, 0, '2021-02-16 06:24:21', '2021-02-16 06:24:21'),
(45, 'Fabric', 100.00, 21, 0, '2021-02-16 06:24:21', '2021-02-16 06:24:21'),
(40, '10+', 500.00, 20, 0, '2021-02-10 00:07:00', '2021-02-16 08:04:34'),
(39, '5+', 300.00, 20, 0, '2021-02-10 00:07:00', '2021-02-16 08:04:34'),
(50, 'uye', 900.00, 10, 0, '2021-02-16 08:05:05', '2021-02-16 08:05:05'),
(48, 'Larged legs', 500.00, 1, 0, '2021-02-16 08:03:51', '2021-02-16 08:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_entries`
--

CREATE TABLE `telescope_entries` (
  `sequence` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `telescope_entries`
--

TRUNCATE TABLE `telescope_entries`;
--
-- Dumping data for table `telescope_entries`
--

INSERT INTO `telescope_entries` (`sequence`, `uuid`, `batch_id`, `family_hash`, `should_display_on_index`, `type`, `content`, `created_at`) VALUES
(1, '9297d536-32ca-4ada-874a-7654c2f64437', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from information_schema.tables where table_schema = \'laravel_flowdash\' and table_name = \'migrations\' and table_type = \'BASE TABLE\'\",\"time\":\"1.87\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"ad7d07e5104cadcc6e9623dfc5fefdd8\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(2, '9297d536-56fe-4d54-a36b-b9ac78158efe', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `migrations` (`id` int unsigned not null auto_increment primary key, `migration` varchar(191) not null, `batch` int not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"80.71\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"dca3ddae9e8bbcd341293787e8610ea0\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(3, '9297d536-58d3-4080-a3c2-97e459993e13', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from information_schema.tables where table_schema = \'laravel_flowdash\' and table_name = \'migrations\' and table_type = \'BASE TABLE\'\",\"time\":\"2.84\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"ad7d07e5104cadcc6e9623dfc5fefdd8\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(4, '9297d536-598f-4a8b-9bd6-49ab4f1d5d9c', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select `migration` from `migrations` order by `batch` asc, `migration` asc\",\"time\":\"0.75\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"ed08a59c7f0b8851f0fd2291ca94d5c7\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(5, '9297d536-5b4e-4a3a-b9f5-db9f2ee53890', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select `migration` from `migrations` order by `batch` asc, `migration` asc\",\"time\":\"0.71\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"ed08a59c7f0b8851f0fd2291ca94d5c7\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(6, '9297d536-5f9a-4bef-8d1e-539db1ace749', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select max(`batch`) as aggregate from `migrations`\",\"time\":\"0.91\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"06e60d7b3d1a0c2de504de4e6f27735e\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(7, '9297d536-721f-4fdd-910f-a448f250b9d9', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `users` (`id` bigint unsigned not null auto_increment primary key, `name` varchar(191) not null, `email` varchar(191) not null, `email_verified_at` timestamp null, `password` varchar(191) not null, `remember_token` varchar(100) null, `current_team_id` bigint unsigned null, `profile_photo_path` text null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"42.07\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_000000_create_users_table.php\",\"line\":26,\"hash\":\"ba35d8fcbe526ce71b258c5a3e9f0a82\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(8, '9297d536-83f2-4766-92e7-43f56933cfce', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `users` add unique `users_email_unique`(`email`)\",\"time\":\"45.09\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_000000_create_users_table.php\",\"line\":26,\"hash\":\"0648806a3d18c0f5b81e2257de64675e\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(9, '9297d536-8481-4c33-bfe8-4da667e24381', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2014_10_12_000000_create_users_table\', 1)\",\"time\":\"0.53\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(10, '9297d536-93a3-4bdd-8cb0-592562e7440e', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `password_resets` (`email` varchar(191) not null, `token` varchar(191) not null, `created_at` timestamp null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"37.47\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_100000_create_password_resets_table.php\",\"line\":20,\"hash\":\"68731db34acd59ac6f47053016159dcb\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(11, '9297d536-ae96-466d-9239-2c971373d14a', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `password_resets` add index `password_resets_email_index`(`email`)\",\"time\":\"68.25\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_100000_create_password_resets_table.php\",\"line\":20,\"hash\":\"e47bfd004ad9142afc1b2460960fbe08\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(12, '9297d536-afa2-4daf-a924-893a80ca6f43', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2014_10_12_100000_create_password_resets_table\', 1)\",\"time\":\"1.34\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(13, '9297d536-bff5-4cdc-aecc-c92a37266087', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `users` add `two_factor_secret` text null after `password`, add `two_factor_recovery_codes` text null after `two_factor_secret`\",\"time\":\"38.13\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_200000_add_two_factor_columns_to_users_table.php\",\"line\":24,\"hash\":\"da34b3e9fa65bd6d796e2eaac869bb47\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(14, '9297d536-c07e-462e-a5ce-33e16b55129f', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2014_10_12_200000_add_two_factor_columns_to_users_table\', 1)\",\"time\":\"0.54\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(15, '9297d536-cd77-4d95-abf7-03162db11696', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `telescope_entries` (`sequence` bigint unsigned not null auto_increment primary key, `uuid` char(36) not null, `batch_id` char(36) not null, `family_hash` varchar(191) null, `should_display_on_index` tinyint(1) not null default \'1\', `type` varchar(20) not null, `content` longtext not null, `created_at` datetime null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"28.29\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"8d1ffbefc0996658c0af05e275ad250b\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(16, '9297d536-d4ff-4388-9bf0-ed2c24452b25', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add unique `telescope_entries_uuid_unique`(`uuid`)\",\"time\":\"18.67\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"9fb859ae1faff74c6b9e0b70dfd8eea9\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(17, '9297d536-e535-4535-bbaf-98235adce42a', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add index `telescope_entries_batch_id_index`(`batch_id`)\",\"time\":\"40.52\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"2b075509a9242d6e3f622536c5ccca07\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(18, '9297d536-f3dd-481e-9f5f-c6dba9d6eeac', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add index `telescope_entries_family_hash_index`(`family_hash`)\",\"time\":\"36.55\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"3d25a2a244bd2028dfa0326d3dbf7f4c\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(19, '9297d537-004b-4772-8822-a7ee21650fbe', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add index `telescope_entries_created_at_index`(`created_at`)\",\"time\":\"31.23\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"7352e7f84460fb7ffc450e7ea4de9dc7\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(20, '9297d537-094e-427e-a6e4-97edabbd9079', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add index `telescope_entries_type_should_display_on_index_index`(`type`, `should_display_on_index`)\",\"time\":\"22.18\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"7317a4cad2dfa1a5167548a6acd0b6a5\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(21, '9297d537-16c8-4a04-b039-7d01dcf75e15', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `telescope_entries_tags` (`entry_uuid` char(36) not null, `tag` varchar(191) not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"33.36\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"49a385485c9ea77ced1287c810e06704\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(22, '9297d537-29db-4acc-8c2e-2b018a01396d', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries_tags` add index `telescope_entries_tags_entry_uuid_tag_index`(`entry_uuid`, `tag`)\",\"time\":\"48.22\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"d77cdf5585b51f60954d40e76786e20f\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(23, '9297d537-34c0-4d55-adf3-d4a91b47d468', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries_tags` add index `telescope_entries_tags_tag_index`(`tag`)\",\"time\":\"26.98\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"0bdb35d17e876d6225a7774a2c17647d\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(24, '9297d537-4229-4642-91df-50ba7ec152c8', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries_tags` add constraint `telescope_entries_tags_entry_uuid_foreign` foreign key (`entry_uuid`) references `telescope_entries` (`uuid`) on delete cascade\",\"time\":\"33.74\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"662a818f80a3a9ba2570081fd7a6af2f\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(25, '9297d537-51f9-4316-b343-3caf670ad3f8', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `telescope_monitoring` (`tag` varchar(191) not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"39.79\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"88f0c31d036f95c144b2633daa82c5dd\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(26, '9297d537-525c-4cee-bcb2-4ad96f62ec0d', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2018_08_08_100000_create_telescope_entries_table\', 1)\",\"time\":\"0.38\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(27, '9297d537-5ef5-4179-b2c3-c8ceee36f9ef', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `failed_jobs` (`id` bigint unsigned not null auto_increment primary key, `uuid` varchar(191) not null, `connection` text not null, `queue` text not null, `payload` longtext not null, `exception` longtext not null, `failed_at` timestamp default CURRENT_TIMESTAMP not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"31.09\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_08_19_000000_create_failed_jobs_table.php\",\"line\":24,\"hash\":\"7f1d4016d372055f7fe1458e51a9836b\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(28, '9297d537-716c-4187-b439-be378ca8db90', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `failed_jobs` add unique `failed_jobs_uuid_unique`(`uuid`)\",\"time\":\"46.63\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_08_19_000000_create_failed_jobs_table.php\",\"line\":24,\"hash\":\"f851653a45d1f2394473d70db5636fd3\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(29, '9297d537-71fe-467a-9081-dca88b65e4df', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2019_08_19_000000_create_failed_jobs_table\', 1)\",\"time\":\"0.62\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(30, '9297d537-7d8d-4977-abe6-cab4d90cd61c', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `personal_access_tokens` (`id` bigint unsigned not null auto_increment primary key, `tokenable_type` varchar(191) not null, `tokenable_id` bigint unsigned not null, `name` varchar(191) not null, `token` varchar(64) not null, `abilities` text null, `last_used_at` timestamp null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"27.78\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_12_14_000001_create_personal_access_tokens_table.php\",\"line\":24,\"hash\":\"907b72da0e86de4c80a09064db35e733\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(31, '9297d537-8ea5-4c02-98fb-ef4862d7e1dd', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `personal_access_tokens` add index `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`)\",\"time\":\"43.13\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_12_14_000001_create_personal_access_tokens_table.php\",\"line\":24,\"hash\":\"23e16d13faedc7fd756b258a984d3cad\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(32, '9297d537-a1bf-4f6a-b714-d3e4e2a0707d', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `personal_access_tokens` add unique `personal_access_tokens_token_unique`(`token`)\",\"time\":\"48.10\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_12_14_000001_create_personal_access_tokens_table.php\",\"line\":24,\"hash\":\"6d0025967d6eebfcb6fddf6dcb6ed14c\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(33, '9297d537-a274-4346-8510-834ca147140b', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2019_12_14_000001_create_personal_access_tokens_table\', 1)\",\"time\":\"0.95\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(34, '9297d537-b0a9-4ac7-b348-ee67c2aa249b', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `sessions` (`id` varchar(191) not null, `user_id` bigint unsigned null, `ip_address` varchar(45) null, `user_agent` text null, `payload` text not null, `last_activity` int not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"34.37\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2020_11_03_103054_create_sessions_table.php\",\"line\":23,\"hash\":\"349e60cd698b887ec727cd31918d7b79\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(35, '9297d537-c252-4e64-9a14-1d2286ef733e', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `sessions` add primary key `sessions_id_primary`(`id`)\",\"time\":\"44.42\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2020_11_03_103054_create_sessions_table.php\",\"line\":23,\"hash\":\"331f0d4d4cb505c65ba323a747abef8d\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(36, '9297d537-d856-46d1-88d0-b75d61568f6e', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `sessions` add index `sessions_user_id_index`(`user_id`)\",\"time\":\"55.58\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2020_11_03_103054_create_sessions_table.php\",\"line\":23,\"hash\":\"143e0209095c4f5cecfdd51a11268572\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(37, '9297d537-e829-4ef9-a62f-652d3f33eba8', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `sessions` add index `sessions_last_activity_index`(`last_activity`)\",\"time\":\"39.74\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2020_11_03_103054_create_sessions_table.php\",\"line\":23,\"hash\":\"5102944fa5d480fdd2bbfbfe1a0c03bc\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(38, '9297d537-e8b2-4e3b-8869-324a4f0f8922', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2020_11_03_103054_create_sessions_table\', 1)\",\"time\":\"0.53\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(39, '9297d537-e9c4-49b3-8553-82668639ad9f', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'command', '{\"command\":\"migrate\",\"exit_code\":0,\"arguments\":{\"command\":\"migrate\"},\"options\":{\"database\":null,\"force\":false,\"path\":[],\"realpath\":false,\"schema-path\":null,\"pretend\":false,\"seed\":false,\"step\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(40, '9297d59e-887f-4e0d-b18c-1996c451e0c9', '9297d59e-a8f3-4573-b753-c03a6468a7c3', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"truncate table `users`\",\"time\":\"2.51\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\app\\\\FlowDash\\\\UsersTableSeeder.php\",\"line\":13,\"hash\":\"598ca23f2da534b2d7af82a24c0104ef\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:30:16'),
(41, '9297d59e-a81c-4ef9-81a1-fe6b243686b8', '9297d59e-a8f3-4573-b753-c03a6468a7c3', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `users` (`name`, `email`, `password`, `updated_at`, `created_at`) values (\'Adrian Demian\', \'contact@mosaicpro.biz\', \'y$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG\', \'2021-01-28 04:30:16\', \'2021-01-28 04:30:16\')\",\"time\":\"1.85\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\app\\\\FlowDash\\\\UsersTableSeeder.php\",\"line\":33,\"hash\":\"deb3b16f8b357711b85f0c04a8532ebd\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:30:16'),
(42, '9297d59e-a873-4d14-80b1-326b3d6be7bb', '9297d59e-a8f3-4573-b753-c03a6468a7c3', NULL, 1, 'model', '{\"action\":\"created\",\"model\":\"App\\\\Models\\\\User:1\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:30:16'),
(43, '9297d59e-a8d5-4c23-bab5-e063dd06bad6', '9297d59e-a8f3-4573-b753-c03a6468a7c3', NULL, 1, 'command', '{\"command\":\"db:seed\",\"exit_code\":0,\"arguments\":{\"command\":\"db:seed\"},\"options\":{\"class\":\"App\\\\FlowDash\\\\UsersTableSeeder\",\"database\":null,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 04:30:16'),
(44, '9297d65a-9ea2-41d6-bc5c-30f64470d764', '9297d65a-9f82-4bff-b456-c7371011a41c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:19'),
(45, '9297d65a-9f5c-45a1-bfe7-b3f92ddeb571', '9297d65a-9f82-4bff-b456-c7371011a41c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"*\\/*\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":244,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:19'),
(46, '9297d65a-c592-4f86-935c-cb35fc063c39', '9297d65a-c602-4067-961e-4a0580a28982', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:19'),
(47, '9297d65a-c5de-4580-878e-8219359a474a', '9297d65a-c602-4067-961e-4a0580a28982', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"*\\/*\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":90,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:19'),
(48, '9297d65e-9b9d-4e99-a4e8-a61f1c48707e', '9297d65e-9cbe-4d78-ad73-d57206d85f45', NULL, 1, 'command', '{\"command\":\"key:generate\",\"exit_code\":0,\"arguments\":{\"command\":\"key:generate\"},\"options\":{\"show\":false,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:22'),
(49, '9297d65e-9c91-44f5-953d-33b041aa50e7', '9297d65e-9cbe-4d78-ad73-d57206d85f45', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/execute-solution\",\"method\":\"POST\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\ExecuteSolutionController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\",\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionConfigValueEnabled:enableRunnableSolutions\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"application\\/json\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"content-type\":\"application\\/json\",\"origin\":\"http:\\/\\/192.168.0.28\",\"content-length\":\"82\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":{\"solution\":\"Facade\\\\Ignition\\\\Solutions\\\\GenerateAppKeySolution\",\"parameters\":[]},\"session\":[],\"response_status\":200,\"response\":\"HTML Response\",\"duration\":147,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:22'),
(50, '9297d662-8031-4572-a765-d9b5fe4afb3b', '9297d662-80b1-481a-ba40-4e9a89d1d10a', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:24'),
(51, '9297d662-808b-47a5-89f3-77daee53b035', '9297d662-80b1-481a-ba40-4e9a89d1d10a', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"*\\/*\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":138,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:24'),
(52, '9297d662-c25f-447e-a553-b343667dae08', '9297d662-c305-4a0c-b9bd-2427650062eb', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:25'),
(53, '9297d662-c2d8-4ab6-b6c1-9ee7ad1d23e3', '9297d662-c305-4a0c-b9bd-2427650062eb', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"*\\/*\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":128,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:25'),
(54, '9297dc17-6560-40dc-a045-cedb480c4ed8', '9297dc17-698b-4a71-be2f-250b0b3c0bd7', 'ee54c9d644e4f5a35904d772ed468b36', 1, 'exception', '{\"class\":\"Symfony\\\\Component\\\\Console\\\\Exception\\\\RuntimeException\",\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Input\\\\ArgvInput.php\",\"line\":169,\"message\":\"Too many arguments, expected arguments \\\"command\\\".\",\"context\":null,\"trace\":[{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Input\\\\ArgvInput.php\",\"line\":80},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Input\\\\Input.php\",\"line\":55},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Command\\\\Command.php\",\"line\":217},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\laravel\\\\framework\\\\src\\\\Illuminate\\\\Console\\\\Command.php\",\"line\":121},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Application.php\",\"line\":920},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Application.php\",\"line\":266},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Application.php\",\"line\":142},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\laravel\\\\framework\\\\src\\\\Illuminate\\\\Console\\\\Application.php\",\"line\":93},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\laravel\\\\framework\\\\src\\\\Illuminate\\\\Foundation\\\\Console\\\\Kernel.php\",\"line\":129},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37}],\"line_preview\":{\"160\":\"        \\/\\/ if last argument isArray(), append token to last argument\",\"161\":\"        } elseif ($this->definition->hasArgument($c - 1) && $this->definition->getArgument($c - 1)->isArray()) {\",\"162\":\"            $arg = $this->definition->getArgument($c - 1);\",\"163\":\"            $this->arguments[$arg->getName()][] = $token;\",\"164\":\"\",\"165\":\"        \\/\\/ unexpected argument\",\"166\":\"        } else {\",\"167\":\"            $all = $this->definition->getArguments();\",\"168\":\"            if (\\\\count($all)) {\",\"169\":\"                throw new RuntimeException(sprintf(\'Too many arguments, expected arguments \\\"%s\\\".\', implode(\'\\\" \\\"\', array_keys($all))));\",\"170\":\"            }\",\"171\":\"\",\"172\":\"            throw new RuntimeException(sprintf(\'No arguments expected, got \\\"%s\\\".\', $token));\",\"173\":\"        }\",\"174\":\"    }\",\"175\":\"\",\"176\":\"    \\/**\",\"177\":\"     * Adds a short option value.\",\"178\":\"     *\",\"179\":\"     * @throws RuntimeException When option given doesn\'t exist\"},\"hostname\":\"Hasti6013\",\"occurrences\":1}', '2021-01-28 04:48:22'),
(55, '9297e5ed-cda1-411c-b9b8-50b1abcef117', '9297e5ed-cf1f-4c69-9969-99021ace4e3c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:15:52'),
(56, '9297e5ed-ceee-4f1f-b4b5-0dd19d2b9a3e', '9297e5ed-cf1f-4c69-9969-99021ace4e3c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":219,\"memory\":8,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:15:52'),
(57, '9297e5ee-3e9e-4cf3-89f2-ed6b4c0de7a3', '9297e5ee-3f18-4053-8ce3-0596441abb7c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:15:53'),
(58, '9297e5ee-3ef2-4ff0-b047-52a50a9eee41', '9297e5ee-3f18-4053-8ce3-0596441abb7c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":175,\"memory\":6,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:15:53'),
(59, '9297e751-7232-4c28-968a-84c8708f8c8e', '9297e751-73b7-407e-839a-e2b58f44f086', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:19:45'),
(60, '9297e751-7384-45e2-94c2-f4a1142ad341', '9297e751-73b7-407e-839a-e2b58f44f086', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":364,\"memory\":8,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:19:45'),
(61, '9297e751-a725-4734-9a7c-2c1b1164fc6e', '9297e751-a7a1-4af4-b22f-498511dc0902', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:19:46'),
(62, '9297e751-a77c-4de7-962d-0b4444c889e1', '9297e751-a7a1-4af4-b22f-498511dc0902', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":123,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:19:46'),
(63, '9297efab-4303-438b-ab15-5ecb1a67ea92', '9297efab-439b-46a8-a248-292e971629ca', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:06'),
(64, '9297efab-4374-48d1-a16a-ff22f83e7066', '9297efab-439b-46a8-a248-292e971629ca', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":190,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:06'),
(65, '9297efab-f1e5-47ba-9a8c-493cd2566727', '9297efab-f267-4440-ab78-d552a8b7e534', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:07'),
(66, '9297efab-f240-489f-894e-90a17d305787', '9297efab-f267-4440-ab78-d552a8b7e534', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":125,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:07'),
(67, '9297efaf-4bc1-446c-aab6-64018ea63016', '9297efaf-4d80-4035-bfb5-811bde1849b0', NULL, 1, 'command', '{\"command\":\"key:generate\",\"exit_code\":0,\"arguments\":{\"command\":\"key:generate\"},\"options\":{\"show\":false,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:09'),
(68, '9297efaf-4d3d-4252-a1a4-d556c13c6fb9', '9297efaf-4d80-4035-bfb5-811bde1849b0', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/execute-solution\",\"method\":\"POST\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\ExecuteSolutionController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\",\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionConfigValueEnabled:enableRunnableSolutions\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"content-length\":\"82\",\"accept\":\"application\\/json\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"content-type\":\"application\\/json\",\"origin\":\"http:\\/\\/192.168.0.28\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":{\"solution\":\"Facade\\\\Ignition\\\\Solutions\\\\GenerateAppKeySolution\",\"parameters\":[]},\"session\":[],\"response_status\":200,\"response\":\"HTML Response\",\"duration\":195,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:09'),
(69, '9297efb2-9598-4165-9839-2550572415d5', '9297efb2-9645-4d9c-92e1-ea85a7140a10', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:11'),
(70, '9297efb2-9616-4263-905f-f4a5cdc56082', '9297efb2-9645-4d9c-92e1-ea85a7140a10', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":132,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:11'),
(71, '9297efb2-d1fd-4dc5-858b-2f1497f84ee3', '9297efb2-d280-4991-8f79-7bbb1826c380', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:11');
INSERT INTO `telescope_entries` (`sequence`, `uuid`, `batch_id`, `family_hash`, `should_display_on_index`, `type`, `content`, `created_at`) VALUES
(72, '9297efb2-d259-4339-adb4-2731f14d6005', '9297efb2-d280-4991-8f79-7bbb1826c380', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":116,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:11'),
(73, '9297efb4-49e3-4c30-96d0-b1ccb8377109', '9297efb4-4aab-4ef1-9a6a-6e087ac0646c', NULL, 1, 'command', '{\"command\":\"key:generate\",\"exit_code\":0,\"arguments\":{\"command\":\"key:generate\"},\"options\":{\"show\":false,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:12'),
(74, '9297efb4-4a79-43be-afd7-5d2ece03c172', '9297efb4-4aab-4ef1-9a6a-6e087ac0646c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/execute-solution\",\"method\":\"POST\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\ExecuteSolutionController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\",\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionConfigValueEnabled:enableRunnableSolutions\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"content-length\":\"82\",\"accept\":\"application\\/json\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"content-type\":\"application\\/json\",\"origin\":\"http:\\/\\/192.168.0.28\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":{\"solution\":\"Facade\\\\Ignition\\\\Solutions\\\\GenerateAppKeySolution\",\"parameters\":[]},\"session\":[],\"response_status\":200,\"response\":\"HTML Response\",\"duration\":167,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:12'),
(75, '9297efb8-dc38-4dda-a616-0db71c30d382', '9297efb8-dcd2-4934-ac7d-71cbee59f3e1', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:15'),
(76, '9297efb8-dca5-41c9-a542-f199c4d0095e', '9297efb8-dcd2-4934-ac7d-71cbee59f3e1', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":189,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:15'),
(77, '9297efb9-11ef-4c39-9453-ac3af12d19fe', '9297efb9-1279-4c7f-96cd-86dba3b60954', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:16'),
(78, '9297efb9-124d-4d48-a570-e21c84fa524a', '9297efb9-1279-4c7f-96cd-86dba3b60954', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":128,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:16'),
(79, '9297efbb-e963-475a-9147-53ad23ea51b9', '9297efbb-ea05-437c-93fe-0de80fc97625', NULL, 1, 'command', '{\"command\":\"key:generate\",\"exit_code\":0,\"arguments\":{\"command\":\"key:generate\"},\"options\":{\"show\":false,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:17'),
(80, '9297efbb-e9cf-4526-9bc6-37977109f9a2', '9297efbb-ea05-437c-93fe-0de80fc97625', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/execute-solution\",\"method\":\"POST\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\ExecuteSolutionController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\",\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionConfigValueEnabled:enableRunnableSolutions\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"content-length\":\"82\",\"accept\":\"application\\/json\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"content-type\":\"application\\/json\",\"origin\":\"http:\\/\\/192.168.0.28\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":{\"solution\":\"Facade\\\\Ignition\\\\Solutions\\\\GenerateAppKeySolution\",\"parameters\":[]},\"session\":[],\"response_status\":200,\"response\":\"HTML Response\",\"duration\":129,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:17'),
(81, '9297efbf-7b11-41e2-912e-92839b450b38', '9297efbf-7b90-4d91-8650-aaed0c75a440', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:20'),
(82, '9297efbf-7b6b-4a48-8e01-01b9fe18570a', '9297efbf-7b90-4d91-8650-aaed0c75a440', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":128,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:20'),
(83, '9297efbf-c344-4f57-a4df-9ac2b5201eb7', '9297efbf-c3be-4276-8b0f-584577167a2d', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:20'),
(84, '9297efbf-c398-49b3-a30d-e34428db46e0', '9297efbf-c3be-4276-8b0f-584577167a2d', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":135,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:20'),
(85, '929814ec-c19d-44b8-8a0e-2b6bfb12f851', '929814ec-c346-465e-a019-a8606b07da9c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 07:27:17'),
(86, '929814ec-c313-408f-97b6-4c730abcb9be', '929814ec-c346-465e-a019-a8606b07da9c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":242,\"memory\":8,\"hostname\":\"Hasti6013\"}', '2021-01-28 07:27:17'),
(87, '929814f8-07dd-4801-b905-20ea1326888b', '929814f8-0880-4135-aa68-e2f56899bb2c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 07:27:24'),
(88, '929814f8-0857-49be-a09c-03a94b2b4c8f', '929814f8-0880-4135-aa68-e2f56899bb2c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":197,\"memory\":6,\"hostname\":\"Hasti6013\"}', '2021-01-28 07:27:24'),
(89, '9298177b-f675-4e4a-bd24-5896ec946513', '9298177b-f702-43d7-8b81-3f09583d7e09', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 07:34:26'),
(90, '9298177b-f6de-477d-a833-66f28c7bf2d4', '9298177b-f702-43d7-8b81-3f09583d7e09', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":186,\"memory\":6,\"hostname\":\"Hasti6013\"}', '2021-01-28 07:34:26'),
(91, '9298177c-2c1b-44ce-86b1-2f9a408e398a', '9298177c-2ca3-4656-be41-0214d88a2cfd', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 07:34:26'),
(92, '9298177c-2c7c-46d6-b752-7a6eb2b582a2', '9298177c-2ca3-4656-be41-0214d88a2cfd', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":120,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 07:34:26'),
(93, '92984c3d-116f-4a45-89d5-91809e21bbae', '92984c3d-1341-4912-b683-45210a7c8560', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 10:01:57'),
(94, '92984c3d-1309-49de-b916-2a85b2a8204c', '92984c3d-1341-4912-b683-45210a7c8560', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":229,\"memory\":6,\"hostname\":\"Hasti6013\"}', '2021-01-28 10:01:57'),
(95, '92984c3d-4577-41ce-b3e4-6102a44cb15b', '92984c3d-45fa-485a-90b6-b0d9155c531a', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 10:01:57'),
(96, '92984c3d-45d4-4365-8665-cacd5d19cffa', '92984c3d-45fa-485a-90b6-b0d9155c531a', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":100,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 10:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_entries_tags`
--

CREATE TABLE `telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `telescope_entries_tags`
--

TRUNCATE TABLE `telescope_entries_tags`;
--
-- Dumping data for table `telescope_entries_tags`
--

INSERT INTO `telescope_entries_tags` (`entry_uuid`, `tag`) VALUES
('9297d59e-a873-4d14-80b1-326b3d6be7bb', 'App\\Models\\User:1');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_monitoring`
--

CREATE TABLE `telescope_monitoring` (
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `telescope_monitoring`
--

TRUNCATE TABLE `telescope_monitoring`;
-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG' COMMENT 'Default Password: 12345678',
  `user_type` tinyint(1) NOT NULL COMMENT '0: admin\r\n1: super admin',
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `mobile_number`, `email`, `email_verified_at`, `password`, `user_type`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', NULL, 'superadmin@7hills.com', NULL, '$2y$10$3FR1SSIaIqelUEZDlSxo5OKvK7HshktkDmyK.JxiCrgR5vV/0tcUC', 1, NULL, NULL, 'lEurxUk6uo7DyXcuScsLnfoKQ4E3iLno5IaIaPlSkro1yo79fr58AILp0uyz', NULL, NULL, '2021-01-27 23:00:16', '2021-02-15 13:08:43'),
(2, 'Admin', NULL, 'admin@7hills.com', NULL, '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG', 0, NULL, NULL, 'xNrIthoSYfJqPWz5FXd20tmw07kKbyk3Ka7xxRrMYUMsJaxaiwQhrgTKOi6l', NULL, NULL, '2021-02-05 04:13:16', '2021-02-05 04:13:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `gst`
--
ALTER TABLE `gst`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile_verification`
--
ALTER TABLE `mobile_verification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_details`
--
ALTER TABLE `quotation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_settings`
--
ALTER TABLE `sub_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `telescope_entries`
--
ALTER TABLE `telescope_entries`
  ADD PRIMARY KEY (`sequence`),
  ADD UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`),
  ADD KEY `telescope_entries_batch_id_index` (`batch_id`),
  ADD KEY `telescope_entries_family_hash_index` (`family_hash`),
  ADD KEY `telescope_entries_created_at_index` (`created_at`),
  ADD KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`);

--
-- Indexes for table `telescope_entries_tags`
--
ALTER TABLE `telescope_entries_tags`
  ADD KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  ADD KEY `telescope_entries_tags_tag_index` (`tag`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gst`
--
ALTER TABLE `gst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mobile_verification`
--
ALTER TABLE `mobile_verification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;
--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `quotation_details`
--
ALTER TABLE `quotation_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `sub_settings`
--
ALTER TABLE `sub_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `telescope_entries`
--
ALTER TABLE `telescope_entries`
  MODIFY `sequence` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
