-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 13, 2021 at 06:47 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_flowdash`
--

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details`
--

DROP TABLE IF EXISTS `quotation_details`;
CREATE TABLE IF NOT EXISTS `quotation_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qid` int(3) NOT NULL COMMENT 'quotation id',
  `setting` varchar(191) NOT NULL,
  `sid` int(3) NOT NULL,
  `sub_setting` varchar(191) DEFAULT NULL,
  `ssid` int(3) NOT NULL DEFAULT '0',
  `price` int(6) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `quotation_details`
--

TRUNCATE TABLE `quotation_details`;
--
-- Dumping data for table `quotation_details`
--

INSERT INTO `quotation_details` (`id`, `qid`, `setting`, `sid`, `sub_setting`, `ssid`, `price`, `created_at`, `updated_at`) VALUES
(1, 1, 'Full Carving', 1, 'Carved Legs', 1, 100, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(2, 1, 'Seat Height', 2, '30', 5, 60, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(3, 1, 'Back Height', 3, '30', 7, 60, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(4, 1, 'Seat Width', 4, '25', 8, 50, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(5, 1, 'Seat Depth', 5, '30', 13, 30, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(6, 1, 'Frame Shaking', 6, '0', 0, 100, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(7, 1, 'No of Cracks in Frame', 7, '0', 0, 60, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(8, 1, 'No of Breakages', 8, '0', 0, 20, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(9, 1, 'Repolish required', 9, '0', 0, 160, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(10, 1, 'Colors to be channged', 10, 'red', 14, 100, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(11, 1, 'Polish Type required', 11, 'PU', 18, 260, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(12, 1, 'Rubber Cap Required', 12, '0', 0, 70, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(13, 1, ' 	Seat Ply to be changed', 13, '0', 0, 80, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(14, 1, ' 	Seat Foam to be changed', 14, '0', 0, 80, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(15, 1, 'Seat Cover to be changed', 15, 'PU', 21, 250, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(16, 1, 'Non Woven for bottom to be changed', 16, '0', 0, 30, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(17, 1, 'Back Ply to be changed', 17, '0', 0, 110, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(18, 1, 'Back Foam to be changed', 18, '0', 0, 110, '2021-02-12 04:27:26', '2021-02-12 04:27:26'),
(19, 1, 'Back Cover to be Changed', 19, 'Leather', 25, 110, '2021-02-12 04:27:26', '2021-02-12 04:27:26');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
