-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 10, 2021 at 05:40 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_flowdash`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_esperanto_ci NOT NULL,
  `email` varchar(191) NOT NULL,
  `mobile_no` bigint(10) NOT NULL,
  `quotation` varchar(191) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `customers`
--

TRUNCATE TABLE `customers`;
-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `failed_jobs`
--

TRUNCATE TABLE `failed_jobs`;
-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2018_08_08_100000_create_telescope_entries_table', 1),
(5, '2019_08_19_000000_create_failed_jobs_table', 1),
(6, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(7, '2020_11_03_103054_create_sessions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mobile_verification`
--

DROP TABLE IF EXISTS `mobile_verification`;
CREATE TABLE IF NOT EXISTS `mobile_verification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `mobile_number` varchar(50) CHARACTER SET utf8 NOT NULL,
  `code` int(11) NOT NULL,
  `verified` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Truncate table before insert `mobile_verification`
--

TRUNCATE TABLE `mobile_verification`;
--
-- Dumping data for table `mobile_verification`
--

INSERT INTO `mobile_verification` (`id`, `customer_id`, `mobile_number`, `code`, `verified`, `created_at`, `updated_at`) VALUES
(10, NULL, '9428854599', 516953, 1, '2021-02-06 09:41:30', '2021-02-06 04:11:30'),
(5, NULL, '9586407439', 222860, 0, '2021-02-06 00:57:03', '2021-02-06 00:57:03');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `personal_access_tokens`
--

TRUNCATE TABLE `personal_access_tokens`;
--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 2, 'staff-api', '01c4b078f9d5e5284de5b6eef85c31312aa584a872a620fe17d8d213b6713e1d', '[\"*\"]', NULL, '2021-02-06 03:58:19', '2021-02-06 03:58:19'),
(2, 'App\\Models\\User', 2, 'staff-api', '14cb319475f29e4974c4e3969b24b6ca69cc4af2759f7ed22738abd1631dcc8b', '[\"*\"]', NULL, '2021-02-06 04:00:12', '2021-02-06 04:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sessions_user_id_index` (`user_id`),
  KEY `sessions_last_activity_index` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `sessions`
--

TRUNCATE TABLE `sessions`;
--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('HM8NAgTydHCtDwbKVtTwvZe6AJUfN9TN3A8ObfVa', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiTTM0NGtONUVBR0dsRnhKOVFXWmlUN3dtOXpTSjFZbjVyYnViRDd2bCI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyNToiaHR0cDovL2Zsb3dkYXNoLnRlc3QvaG9tZSI7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI1OiJodHRwOi8vZmxvd2Rhc2gudGVzdC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1612514168),
('yncBAwxRdkXo6jm7AAhz1nGmoTxuUPsHyEQ8c2Iy', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiQjNIUXYxQTI3V1lVMGc3WnJOYm5vME5oc1d5NTk5YW9HNWhkcjRaVyI7czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyNToiaHR0cDovL2Zsb3dkYXNoLnRlc3QvaG9tZSI7fXM6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjI1OiJodHRwOi8vZmxvd2Rhc2gudGVzdC9ob21lIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1612514186);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0: de active\r\n1: active',
  `selection` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: single\r\n1: multiple',
  `price` int(6) NOT NULL DEFAULT '0',
  `setting_type` tinyint(1) NOT NULL COMMENT '0: with subsettings and price\r\n1: with default price\r\n2: subsettings with name only\r\n3: subsettings with name, price and text box \r\n4: with no subsetting or default price \r\n5: textbox\r\n6: textarea',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `settings`
--

TRUNCATE TABLE `settings`;
--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `active`, `selection`, `price`, `setting_type`, `created_at`, `updated_at`) VALUES
(1, 'Full Carving', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-04 01:22:36'),
(2, 'Seat Height', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-04 03:12:50'),
(3, 'Back Height', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(4, 'Seat Width', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(5, 'Seat Depth', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(6, 'Frame Shaking', 1, 0, 100, 1, '2021-02-02 12:17:59', '2021-02-04 04:35:30'),
(7, 'No of Cracks in Frame', 1, 0, 60, 1, '2021-02-02 12:17:59', '2021-02-04 02:32:06'),
(8, 'No of Breakages', 1, 0, 20, 1, '2021-02-02 12:17:59', '2021-02-04 02:32:27'),
(9, 'Repolish required', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(10, 'Colors to be channged', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(11, 'Polish Type required', 1, 0, 0, 2, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(12, 'Rubber Cap Required', 1, 0, 70, 1, '2021-02-02 12:17:59', '2021-02-04 02:32:46'),
(13, 'Seat Ply to be changed', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-03 23:31:14'),
(14, 'Seat Foam to be changed', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(15, 'Seat Cover to be changed', 1, 0, 0, 0, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(16, 'Non Woven for bottom to be changed', 1, 0, 30, 1, '2021-02-02 12:17:59', '2021-02-04 02:32:53'),
(17, 'Back Ply to be changed', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(18, 'Back Foam to be changed', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(19, 'Back Cover to be Changed', 1, 0, 0, 2, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(20, 'Cover material', 1, 0, 0, 4, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(21, 'Any Specific requirement from Customer', 1, 0, 0, 6, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(22, 'Other Additional Cost', 1, 0, 0, 5, '2021-02-02 12:17:59', '2021-02-02 12:17:59'),
(23, 'Missing Parts', 1, 0, 0, 3, '2021-02-04 11:53:22', '2021-02-04 11:53:22');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_esperanto_ci NOT NULL,
  `email` varchar(191) NOT NULL,
  `mobile_number` bigint(10) NOT NULL,
  `password` varchar(200) NOT NULL DEFAULT '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG' COMMENT 'password: 12345678',
  `designation` varchar(191) NOT NULL,
  `allowed_discount` int(3) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `staff`
--

TRUNCATE TABLE `staff`;
--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `first_name`, `last_name`, `email`, `mobile_number`, `password`, `designation`, `allowed_discount`, `created_at`, `updated_at`) VALUES
(11, 'Sixty', '13', 'gautam@sixty13.com', 9428854599, '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG', 'Sr. MAnager', 5, '2021-02-05 05:41:07', '2021-02-05 05:41:07'),
(12, 'mohit', 'mangukia', 'pmohit5291@gmail.com', 9586407439, '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG', 'sr. developer', 10, '2021-02-08 02:10:29', '2021-02-08 02:10:49');

-- --------------------------------------------------------

--
-- Table structure for table `sub_settings`
--

DROP TABLE IF EXISTS `sub_settings`;
CREATE TABLE IF NOT EXISTS `sub_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `price` float(6,2) NOT NULL,
  `setting_id` int(11) NOT NULL COMMENT 'Ref. to settings table',
  `delete_at` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: To not delete\r\n1: To delete',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `sub_settings`
--

TRUNCATE TABLE `sub_settings`;
--
-- Dumping data for table `sub_settings`
--

INSERT INTO `sub_settings` (`id`, `name`, `price`, `setting_id`, `delete_at`, `created_at`, `updated_at`) VALUES
(1, 'Carved Legs', 100.00, 1, 0, '2021-02-09 00:07:14', '2021-02-09 00:07:14'),
(2, 'Turned Legs', 500.00, 1, 0, '2021-02-09 00:07:14', '2021-02-09 00:07:14'),
(3, 'Plain Legs', 100.00, 1, 0, '2021-02-09 00:07:14', '2021-02-09 00:07:14'),
(4, '25', 50.00, 2, 0, '2021-02-09 00:07:33', '2021-02-09 00:07:33'),
(5, '30', 60.00, 2, 0, '2021-02-09 00:07:33', '2021-02-09 00:07:33'),
(6, '25', 50.00, 3, 0, '2021-02-09 00:07:42', '2021-02-09 00:07:42'),
(7, '30', 60.00, 3, 0, '2021-02-09 00:07:42', '2021-02-09 00:07:42'),
(13, '25', 30.00, 5, 0, '2021-02-09 00:09:58', '2021-02-09 00:09:58'),
(12, '25', 30.00, 5, 0, '2021-02-09 00:09:58', '2021-02-09 00:09:58'),
(8, '25', 50.00, 4, 0, '2021-02-09 00:08:38', '2021-02-09 00:08:38'),
(9, '30', 60.00, 4, 0, '2021-02-09 00:08:38', '2021-02-09 00:08:38'),
(14, 'red', 100.00, 10, 0, '2021-02-09 00:10:48', '2021-02-09 00:10:48'),
(15, 'white', 200.00, 10, 0, '2021-02-09 00:10:48', '2021-02-09 00:10:48'),
(16, 'green', 50.00, 10, 0, '2021-02-09 00:10:48', '2021-02-09 00:10:48'),
(17, 'AC', 0.00, 11, 0, '2021-02-09 00:11:22', '2021-02-09 00:11:22'),
(18, 'PU', 0.00, 11, 0, '2021-02-09 00:11:22', '2021-02-09 00:11:22'),
(19, 'Exterior PU', 0.00, 11, 0, '2021-02-09 00:11:22', '2021-02-09 00:11:22'),
(20, 'Fabric', 100.00, 15, 0, '2021-02-09 00:14:00', '2021-02-09 00:14:00'),
(21, 'PU', 250.00, 15, 0, '2021-02-09 00:14:00', '2021-02-09 00:14:00'),
(22, 'Leather', 500.00, 15, 0, '2021-02-09 00:14:00', '2021-02-09 00:14:00'),
(23, 'Fabric', 0.00, 19, 0, '2021-02-09 00:14:58', '2021-02-09 00:14:58'),
(24, 'PU', 0.00, 19, 0, '2021-02-09 00:14:58', '2021-02-09 00:14:58'),
(25, 'Leather', 0.00, 19, 0, '2021-02-09 00:14:58', '2021-02-09 00:14:58'),
(26, 'blue', 60.00, 10, 0, '2021-02-09 01:02:46', '2021-02-09 01:02:46'),
(41, 'grey', 1500.00, 10, 0, '2021-02-10 00:07:31', '2021-02-10 00:07:31'),
(38, 'black', 1000.00, 10, 0, '2021-02-09 01:06:01', '2021-02-09 01:06:01'),
(40, '5+', 500.00, 23, 0, '2021-02-10 00:07:00', '2021-02-10 00:07:00'),
(39, '3+', 300.00, 23, 0, '2021-02-10 00:07:00', '2021-02-10 00:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_entries`
--

DROP TABLE IF EXISTS `telescope_entries`;
CREATE TABLE IF NOT EXISTS `telescope_entries` (
  `sequence` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`sequence`),
  UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`),
  KEY `telescope_entries_batch_id_index` (`batch_id`),
  KEY `telescope_entries_family_hash_index` (`family_hash`),
  KEY `telescope_entries_created_at_index` (`created_at`),
  KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `telescope_entries`
--

TRUNCATE TABLE `telescope_entries`;
--
-- Dumping data for table `telescope_entries`
--

INSERT INTO `telescope_entries` (`sequence`, `uuid`, `batch_id`, `family_hash`, `should_display_on_index`, `type`, `content`, `created_at`) VALUES
(1, '9297d536-32ca-4ada-874a-7654c2f64437', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from information_schema.tables where table_schema = \'laravel_flowdash\' and table_name = \'migrations\' and table_type = \'BASE TABLE\'\",\"time\":\"1.87\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"ad7d07e5104cadcc6e9623dfc5fefdd8\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(2, '9297d536-56fe-4d54-a36b-b9ac78158efe', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `migrations` (`id` int unsigned not null auto_increment primary key, `migration` varchar(191) not null, `batch` int not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"80.71\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"dca3ddae9e8bbcd341293787e8610ea0\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(3, '9297d536-58d3-4080-a3c2-97e459993e13', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select * from information_schema.tables where table_schema = \'laravel_flowdash\' and table_name = \'migrations\' and table_type = \'BASE TABLE\'\",\"time\":\"2.84\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"ad7d07e5104cadcc6e9623dfc5fefdd8\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(4, '9297d536-598f-4a8b-9bd6-49ab4f1d5d9c', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select `migration` from `migrations` order by `batch` asc, `migration` asc\",\"time\":\"0.75\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"ed08a59c7f0b8851f0fd2291ca94d5c7\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(5, '9297d536-5b4e-4a3a-b9f5-db9f2ee53890', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select `migration` from `migrations` order by `batch` asc, `migration` asc\",\"time\":\"0.71\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"ed08a59c7f0b8851f0fd2291ca94d5c7\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(6, '9297d536-5f9a-4bef-8d1e-539db1ace749', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"select max(`batch`) as aggregate from `migrations`\",\"time\":\"0.91\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"06e60d7b3d1a0c2de504de4e6f27735e\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(7, '9297d536-721f-4fdd-910f-a448f250b9d9', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `users` (`id` bigint unsigned not null auto_increment primary key, `name` varchar(191) not null, `email` varchar(191) not null, `email_verified_at` timestamp null, `password` varchar(191) not null, `remember_token` varchar(100) null, `current_team_id` bigint unsigned null, `profile_photo_path` text null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"42.07\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_000000_create_users_table.php\",\"line\":26,\"hash\":\"ba35d8fcbe526ce71b258c5a3e9f0a82\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(8, '9297d536-83f2-4766-92e7-43f56933cfce', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `users` add unique `users_email_unique`(`email`)\",\"time\":\"45.09\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_000000_create_users_table.php\",\"line\":26,\"hash\":\"0648806a3d18c0f5b81e2257de64675e\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(9, '9297d536-8481-4c33-bfe8-4da667e24381', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2014_10_12_000000_create_users_table\', 1)\",\"time\":\"0.53\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(10, '9297d536-93a3-4bdd-8cb0-592562e7440e', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `password_resets` (`email` varchar(191) not null, `token` varchar(191) not null, `created_at` timestamp null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"37.47\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_100000_create_password_resets_table.php\",\"line\":20,\"hash\":\"68731db34acd59ac6f47053016159dcb\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(11, '9297d536-ae96-466d-9239-2c971373d14a', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `password_resets` add index `password_resets_email_index`(`email`)\",\"time\":\"68.25\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_100000_create_password_resets_table.php\",\"line\":20,\"hash\":\"e47bfd004ad9142afc1b2460960fbe08\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(12, '9297d536-afa2-4daf-a924-893a80ca6f43', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2014_10_12_100000_create_password_resets_table\', 1)\",\"time\":\"1.34\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(13, '9297d536-bff5-4cdc-aecc-c92a37266087', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `users` add `two_factor_secret` text null after `password`, add `two_factor_recovery_codes` text null after `two_factor_secret`\",\"time\":\"38.13\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2014_10_12_200000_add_two_factor_columns_to_users_table.php\",\"line\":24,\"hash\":\"da34b3e9fa65bd6d796e2eaac869bb47\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(14, '9297d536-c07e-462e-a5ce-33e16b55129f', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2014_10_12_200000_add_two_factor_columns_to_users_table\', 1)\",\"time\":\"0.54\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(15, '9297d536-cd77-4d95-abf7-03162db11696', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `telescope_entries` (`sequence` bigint unsigned not null auto_increment primary key, `uuid` char(36) not null, `batch_id` char(36) not null, `family_hash` varchar(191) null, `should_display_on_index` tinyint(1) not null default \'1\', `type` varchar(20) not null, `content` longtext not null, `created_at` datetime null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"28.29\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"8d1ffbefc0996658c0af05e275ad250b\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(16, '9297d536-d4ff-4388-9bf0-ed2c24452b25', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add unique `telescope_entries_uuid_unique`(`uuid`)\",\"time\":\"18.67\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"9fb859ae1faff74c6b9e0b70dfd8eea9\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(17, '9297d536-e535-4535-bbaf-98235adce42a', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add index `telescope_entries_batch_id_index`(`batch_id`)\",\"time\":\"40.52\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"2b075509a9242d6e3f622536c5ccca07\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(18, '9297d536-f3dd-481e-9f5f-c6dba9d6eeac', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add index `telescope_entries_family_hash_index`(`family_hash`)\",\"time\":\"36.55\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"3d25a2a244bd2028dfa0326d3dbf7f4c\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(19, '9297d537-004b-4772-8822-a7ee21650fbe', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add index `telescope_entries_created_at_index`(`created_at`)\",\"time\":\"31.23\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"7352e7f84460fb7ffc450e7ea4de9dc7\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(20, '9297d537-094e-427e-a6e4-97edabbd9079', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries` add index `telescope_entries_type_should_display_on_index_index`(`type`, `should_display_on_index`)\",\"time\":\"22.18\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"7317a4cad2dfa1a5167548a6acd0b6a5\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(21, '9297d537-16c8-4a04-b039-7d01dcf75e15', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `telescope_entries_tags` (`entry_uuid` char(36) not null, `tag` varchar(191) not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"33.36\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"49a385485c9ea77ced1287c810e06704\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(22, '9297d537-29db-4acc-8c2e-2b018a01396d', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries_tags` add index `telescope_entries_tags_entry_uuid_tag_index`(`entry_uuid`, `tag`)\",\"time\":\"48.22\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"d77cdf5585b51f60954d40e76786e20f\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(23, '9297d537-34c0-4d55-adf3-d4a91b47d468', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries_tags` add index `telescope_entries_tags_tag_index`(`tag`)\",\"time\":\"26.98\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"0bdb35d17e876d6225a7774a2c17647d\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(24, '9297d537-4229-4642-91df-50ba7ec152c8', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `telescope_entries_tags` add constraint `telescope_entries_tags_entry_uuid_foreign` foreign key (`entry_uuid`) references `telescope_entries` (`uuid`) on delete cascade\",\"time\":\"33.74\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"662a818f80a3a9ba2570081fd7a6af2f\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(25, '9297d537-51f9-4316-b343-3caf670ad3f8', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `telescope_monitoring` (`tag` varchar(191) not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"39.79\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"88f0c31d036f95c144b2633daa82c5dd\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(26, '9297d537-525c-4cee-bcb2-4ad96f62ec0d', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2018_08_08_100000_create_telescope_entries_table\', 1)\",\"time\":\"0.38\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(27, '9297d537-5ef5-4179-b2c3-c8ceee36f9ef', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `failed_jobs` (`id` bigint unsigned not null auto_increment primary key, `uuid` varchar(191) not null, `connection` text not null, `queue` text not null, `payload` longtext not null, `exception` longtext not null, `failed_at` timestamp default CURRENT_TIMESTAMP not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"31.09\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_08_19_000000_create_failed_jobs_table.php\",\"line\":24,\"hash\":\"7f1d4016d372055f7fe1458e51a9836b\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(28, '9297d537-716c-4187-b439-be378ca8db90', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `failed_jobs` add unique `failed_jobs_uuid_unique`(`uuid`)\",\"time\":\"46.63\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_08_19_000000_create_failed_jobs_table.php\",\"line\":24,\"hash\":\"f851653a45d1f2394473d70db5636fd3\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(29, '9297d537-71fe-467a-9081-dca88b65e4df', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2019_08_19_000000_create_failed_jobs_table\', 1)\",\"time\":\"0.62\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:08'),
(30, '9297d537-7d8d-4977-abe6-cab4d90cd61c', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `personal_access_tokens` (`id` bigint unsigned not null auto_increment primary key, `tokenable_type` varchar(191) not null, `tokenable_id` bigint unsigned not null, `name` varchar(191) not null, `token` varchar(64) not null, `abilities` text null, `last_used_at` timestamp null, `created_at` timestamp null, `updated_at` timestamp null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"27.78\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_12_14_000001_create_personal_access_tokens_table.php\",\"line\":24,\"hash\":\"907b72da0e86de4c80a09064db35e733\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(31, '9297d537-8ea5-4c02-98fb-ef4862d7e1dd', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `personal_access_tokens` add index `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`)\",\"time\":\"43.13\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_12_14_000001_create_personal_access_tokens_table.php\",\"line\":24,\"hash\":\"23e16d13faedc7fd756b258a984d3cad\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(32, '9297d537-a1bf-4f6a-b714-d3e4e2a0707d', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `personal_access_tokens` add unique `personal_access_tokens_token_unique`(`token`)\",\"time\":\"48.10\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2019_12_14_000001_create_personal_access_tokens_table.php\",\"line\":24,\"hash\":\"6d0025967d6eebfcb6fddf6dcb6ed14c\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(33, '9297d537-a274-4346-8510-834ca147140b', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2019_12_14_000001_create_personal_access_tokens_table\', 1)\",\"time\":\"0.95\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(34, '9297d537-b0a9-4ac7-b348-ee67c2aa249b', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"create table `sessions` (`id` varchar(191) not null, `user_id` bigint unsigned null, `ip_address` varchar(45) null, `user_agent` text null, `payload` text not null, `last_activity` int not null) default character set utf8mb4 collate \'utf8mb4_unicode_ci\'\",\"time\":\"34.37\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2020_11_03_103054_create_sessions_table.php\",\"line\":23,\"hash\":\"349e60cd698b887ec727cd31918d7b79\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(35, '9297d537-c252-4e64-9a14-1d2286ef733e', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `sessions` add primary key `sessions_id_primary`(`id`)\",\"time\":\"44.42\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2020_11_03_103054_create_sessions_table.php\",\"line\":23,\"hash\":\"331f0d4d4cb505c65ba323a747abef8d\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(36, '9297d537-d856-46d1-88d0-b75d61568f6e', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `sessions` add index `sessions_user_id_index`(`user_id`)\",\"time\":\"55.58\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2020_11_03_103054_create_sessions_table.php\",\"line\":23,\"hash\":\"143e0209095c4f5cecfdd51a11268572\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(37, '9297d537-e829-4ef9-a62f-652d3f33eba8', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"alter table `sessions` add index `sessions_last_activity_index`(`last_activity`)\",\"time\":\"39.74\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\database\\\\migrations\\\\2020_11_03_103054_create_sessions_table.php\",\"line\":23,\"hash\":\"5102944fa5d480fdd2bbfbfe1a0c03bc\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(38, '9297d537-e8b2-4e3b-8869-324a4f0f8922', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `migrations` (`migration`, `batch`) values (\'2020_11_03_103054_create_sessions_table\', 1)\",\"time\":\"0.53\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37,\"hash\":\"f2b8e8e4266db16aec6db940c643eb68\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(39, '9297d537-e9c4-49b3-8553-82668639ad9f', '9297d537-e9fb-4304-af39-0c79b5eb57b9', NULL, 1, 'command', '{\"command\":\"migrate\",\"exit_code\":0,\"arguments\":{\"command\":\"migrate\"},\"options\":{\"database\":null,\"force\":false,\"path\":[],\"realpath\":false,\"schema-path\":null,\"pretend\":false,\"seed\":false,\"step\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 04:29:09'),
(40, '9297d59e-887f-4e0d-b18c-1996c451e0c9', '9297d59e-a8f3-4573-b753-c03a6468a7c3', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"truncate table `users`\",\"time\":\"2.51\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\app\\\\FlowDash\\\\UsersTableSeeder.php\",\"line\":13,\"hash\":\"598ca23f2da534b2d7af82a24c0104ef\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:30:16'),
(41, '9297d59e-a81c-4ef9-81a1-fe6b243686b8', '9297d59e-a8f3-4573-b753-c03a6468a7c3', NULL, 1, 'query', '{\"connection\":\"mysql\",\"bindings\":[],\"sql\":\"insert into `users` (`name`, `email`, `password`, `updated_at`, `created_at`) values (\'Adrian Demian\', \'contact@mosaicpro.biz\', \'y$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG\', \'2021-01-28 04:30:16\', \'2021-01-28 04:30:16\')\",\"time\":\"1.85\",\"slow\":false,\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\app\\\\FlowDash\\\\UsersTableSeeder.php\",\"line\":33,\"hash\":\"deb3b16f8b357711b85f0c04a8532ebd\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:30:16'),
(42, '9297d59e-a873-4d14-80b1-326b3d6be7bb', '9297d59e-a8f3-4573-b753-c03a6468a7c3', NULL, 1, 'model', '{\"action\":\"created\",\"model\":\"App\\\\Models\\\\User:1\",\"hostname\":\"Hasti6013\"}', '2021-01-28 04:30:16'),
(43, '9297d59e-a8d5-4c23-bab5-e063dd06bad6', '9297d59e-a8f3-4573-b753-c03a6468a7c3', NULL, 1, 'command', '{\"command\":\"db:seed\",\"exit_code\":0,\"arguments\":{\"command\":\"db:seed\"},\"options\":{\"class\":\"App\\\\FlowDash\\\\UsersTableSeeder\",\"database\":null,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 04:30:16'),
(44, '9297d65a-9ea2-41d6-bc5c-30f64470d764', '9297d65a-9f82-4bff-b456-c7371011a41c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:19'),
(45, '9297d65a-9f5c-45a1-bfe7-b3f92ddeb571', '9297d65a-9f82-4bff-b456-c7371011a41c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"*\\/*\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":244,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:19'),
(46, '9297d65a-c592-4f86-935c-cb35fc063c39', '9297d65a-c602-4067-961e-4a0580a28982', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:19'),
(47, '9297d65a-c5de-4580-878e-8219359a474a', '9297d65a-c602-4067-961e-4a0580a28982', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"*\\/*\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":90,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:19'),
(48, '9297d65e-9b9d-4e99-a4e8-a61f1c48707e', '9297d65e-9cbe-4d78-ad73-d57206d85f45', NULL, 1, 'command', '{\"command\":\"key:generate\",\"exit_code\":0,\"arguments\":{\"command\":\"key:generate\"},\"options\":{\"show\":false,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:22'),
(49, '9297d65e-9c91-44f5-953d-33b041aa50e7', '9297d65e-9cbe-4d78-ad73-d57206d85f45', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/execute-solution\",\"method\":\"POST\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\ExecuteSolutionController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\",\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionConfigValueEnabled:enableRunnableSolutions\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"application\\/json\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"content-type\":\"application\\/json\",\"origin\":\"http:\\/\\/192.168.0.28\",\"content-length\":\"82\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":{\"solution\":\"Facade\\\\Ignition\\\\Solutions\\\\GenerateAppKeySolution\",\"parameters\":[]},\"session\":[],\"response_status\":200,\"response\":\"HTML Response\",\"duration\":147,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:22'),
(50, '9297d662-8031-4572-a765-d9b5fe4afb3b', '9297d662-80b1-481a-ba40-4e9a89d1d10a', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:24'),
(51, '9297d662-808b-47a5-89f3-77daee53b035', '9297d662-80b1-481a-ba40-4e9a89d1d10a', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"*\\/*\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":138,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:24'),
(52, '9297d662-c25f-447e-a553-b343667dae08', '9297d662-c305-4a0c-b9bd-2427650062eb', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:25'),
(53, '9297d662-c2d8-4ab6-b6c1-9ee7ad1d23e3', '9297d662-c305-4a0c-b9bd-2427650062eb', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.104\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64; rv:84.0) Gecko\\/20100101 Firefox\\/84.0\",\"accept\":\"*\\/*\",\"accept-language\":\"en-US,en;q=0.5\",\"accept-encoding\":\"gzip, deflate\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"connection\":\"keep-alive\",\"cookie\":\"UM_distinctid=177299c5f2021-0c0ce22a471597-4c3f207e-13c680-177299c5f226; CNZZDATA1278817253=194390643-1611307879-%7C1611384448; XSRF-TOKEN=eyJpdiI6IjNFbVRsY3lBMi9SdWhRTGJuYVpTbGc9PSIsInZhbHVlIjoiOHNwb3JrZmM2c3FWalhrVDEyTHprNTRteDE0WDB6NkNiNUxGOUQyWWpXa0ZxSmpLVGk0RUg0TUsyU1FWeFdGV0E3WmNneUpkQi9SUEJEVTIxMGp4UEJrMFFYR1BXVTJOZ25FNmIvM2ZGQ3NNaWxCZ0Z3aEoyeEFOMGFnMXozWGsiLCJtYWMiOiJkNmEyNjY3MDM3OTZmN2U5Nzk3NTFlMThhYmI3YzA2ZmE5YzM0Y2QwYWY4ZDA5ODFlNGY2OTk3YjlhZTRjMDZjIn0%3D; laravel_session=eyJpdiI6Ii9YcG9zT1hKd2pJR1Bmekd1TSsxdnc9PSIsInZhbHVlIjoiS2hBUmFKbmM0b3lrOGlma21yeFUyNXk0bXFSWUprczdVbnBLR0gvdGVuM0NBMHYxSFRrWjJqVFRtd0p6TzBEVFEzRVFWcDA3cG90S0pIc21VNzZzSGh6amhseGNTREQzTlByYmtrK3N5RXJBeHkxaGI3N1puNzBYbk5VdFlrZjgiLCJtYWMiOiI4YTdlYjA1MWU2MmU1Y2M4MmJmY2Y5NjExYzEzZmVhZGZjNzk5YTBmZWI1Yzk3ZjhmOWE4YWYxNGIxZmM0NGEzIn0%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":128,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 04:32:25'),
(54, '9297dc17-6560-40dc-a045-cedb480c4ed8', '9297dc17-698b-4a71-be2f-250b0b3c0bd7', 'ee54c9d644e4f5a35904d772ed468b36', 1, 'exception', '{\"class\":\"Symfony\\\\Component\\\\Console\\\\Exception\\\\RuntimeException\",\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Input\\\\ArgvInput.php\",\"line\":169,\"message\":\"Too many arguments, expected arguments \\\"command\\\".\",\"context\":null,\"trace\":[{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Input\\\\ArgvInput.php\",\"line\":80},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Input\\\\Input.php\",\"line\":55},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Command\\\\Command.php\",\"line\":217},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\laravel\\\\framework\\\\src\\\\Illuminate\\\\Console\\\\Command.php\",\"line\":121},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Application.php\",\"line\":920},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Application.php\",\"line\":266},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\symfony\\\\console\\\\Application.php\",\"line\":142},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\laravel\\\\framework\\\\src\\\\Illuminate\\\\Console\\\\Application.php\",\"line\":93},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\vendor\\\\laravel\\\\framework\\\\src\\\\Illuminate\\\\Foundation\\\\Console\\\\Kernel.php\",\"line\":129},{\"file\":\"E:\\\\server\\\\www\\\\laravel\\\\flowdash-laravel\\\\artisan\",\"line\":37}],\"line_preview\":{\"160\":\"        \\/\\/ if last argument isArray(), append token to last argument\",\"161\":\"        } elseif ($this->definition->hasArgument($c - 1) && $this->definition->getArgument($c - 1)->isArray()) {\",\"162\":\"            $arg = $this->definition->getArgument($c - 1);\",\"163\":\"            $this->arguments[$arg->getName()][] = $token;\",\"164\":\"\",\"165\":\"        \\/\\/ unexpected argument\",\"166\":\"        } else {\",\"167\":\"            $all = $this->definition->getArguments();\",\"168\":\"            if (\\\\count($all)) {\",\"169\":\"                throw new RuntimeException(sprintf(\'Too many arguments, expected arguments \\\"%s\\\".\', implode(\'\\\" \\\"\', array_keys($all))));\",\"170\":\"            }\",\"171\":\"\",\"172\":\"            throw new RuntimeException(sprintf(\'No arguments expected, got \\\"%s\\\".\', $token));\",\"173\":\"        }\",\"174\":\"    }\",\"175\":\"\",\"176\":\"    \\/**\",\"177\":\"     * Adds a short option value.\",\"178\":\"     *\",\"179\":\"     * @throws RuntimeException When option given doesn\'t exist\"},\"hostname\":\"Hasti6013\",\"occurrences\":1}', '2021-01-28 04:48:22'),
(55, '9297e5ed-cda1-411c-b9b8-50b1abcef117', '9297e5ed-cf1f-4c69-9969-99021ace4e3c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:15:52'),
(56, '9297e5ed-ceee-4f1f-b4b5-0dd19d2b9a3e', '9297e5ed-cf1f-4c69-9969-99021ace4e3c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":219,\"memory\":8,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:15:52'),
(57, '9297e5ee-3e9e-4cf3-89f2-ed6b4c0de7a3', '9297e5ee-3f18-4053-8ce3-0596441abb7c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:15:53'),
(58, '9297e5ee-3ef2-4ff0-b047-52a50a9eee41', '9297e5ee-3f18-4053-8ce3-0596441abb7c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":175,\"memory\":6,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:15:53'),
(59, '9297e751-7232-4c28-968a-84c8708f8c8e', '9297e751-73b7-407e-839a-e2b58f44f086', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:19:45'),
(60, '9297e751-7384-45e2-94c2-f4a1142ad341', '9297e751-73b7-407e-839a-e2b58f44f086', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":364,\"memory\":8,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:19:45'),
(61, '9297e751-a725-4734-9a7c-2c1b1164fc6e', '9297e751-a7a1-4af4-b22f-498511dc0902', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:19:46'),
(62, '9297e751-a77c-4de7-962d-0b4444c889e1', '9297e751-a7a1-4af4-b22f-498511dc0902', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":123,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:19:46'),
(63, '9297efab-4303-438b-ab15-5ecb1a67ea92', '9297efab-439b-46a8-a248-292e971629ca', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:06'),
(64, '9297efab-4374-48d1-a16a-ff22f83e7066', '9297efab-439b-46a8-a248-292e971629ca', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":190,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:06'),
(65, '9297efab-f1e5-47ba-9a8c-493cd2566727', '9297efab-f267-4440-ab78-d552a8b7e534', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:07'),
(66, '9297efab-f240-489f-894e-90a17d305787', '9297efab-f267-4440-ab78-d552a8b7e534', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":125,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:07'),
(67, '9297efaf-4bc1-446c-aab6-64018ea63016', '9297efaf-4d80-4035-bfb5-811bde1849b0', NULL, 1, 'command', '{\"command\":\"key:generate\",\"exit_code\":0,\"arguments\":{\"command\":\"key:generate\"},\"options\":{\"show\":false,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:09'),
(68, '9297efaf-4d3d-4252-a1a4-d556c13c6fb9', '9297efaf-4d80-4035-bfb5-811bde1849b0', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/execute-solution\",\"method\":\"POST\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\ExecuteSolutionController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\",\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionConfigValueEnabled:enableRunnableSolutions\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"content-length\":\"82\",\"accept\":\"application\\/json\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"content-type\":\"application\\/json\",\"origin\":\"http:\\/\\/192.168.0.28\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":{\"solution\":\"Facade\\\\Ignition\\\\Solutions\\\\GenerateAppKeySolution\",\"parameters\":[]},\"session\":[],\"response_status\":200,\"response\":\"HTML Response\",\"duration\":195,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:09'),
(69, '9297efb2-9598-4165-9839-2550572415d5', '9297efb2-9645-4d9c-92e1-ea85a7140a10', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:11'),
(70, '9297efb2-9616-4263-905f-f4a5cdc56082', '9297efb2-9645-4d9c-92e1-ea85a7140a10', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":132,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:11'),
(71, '9297efb2-d1fd-4dc5-858b-2f1497f84ee3', '9297efb2-d280-4991-8f79-7bbb1826c380', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:11');
INSERT INTO `telescope_entries` (`sequence`, `uuid`, `batch_id`, `family_hash`, `should_display_on_index`, `type`, `content`, `created_at`) VALUES
(72, '9297efb2-d259-4339-adb4-2731f14d6005', '9297efb2-d280-4991-8f79-7bbb1826c380', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":116,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:11'),
(73, '9297efb4-49e3-4c30-96d0-b1ccb8377109', '9297efb4-4aab-4ef1-9a6a-6e087ac0646c', NULL, 1, 'command', '{\"command\":\"key:generate\",\"exit_code\":0,\"arguments\":{\"command\":\"key:generate\"},\"options\":{\"show\":false,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:12'),
(74, '9297efb4-4a79-43be-afd7-5d2ece03c172', '9297efb4-4aab-4ef1-9a6a-6e087ac0646c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/execute-solution\",\"method\":\"POST\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\ExecuteSolutionController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\",\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionConfigValueEnabled:enableRunnableSolutions\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"content-length\":\"82\",\"accept\":\"application\\/json\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"content-type\":\"application\\/json\",\"origin\":\"http:\\/\\/192.168.0.28\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":{\"solution\":\"Facade\\\\Ignition\\\\Solutions\\\\GenerateAppKeySolution\",\"parameters\":[]},\"session\":[],\"response_status\":200,\"response\":\"HTML Response\",\"duration\":167,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:12'),
(75, '9297efb8-dc38-4dda-a616-0db71c30d382', '9297efb8-dcd2-4934-ac7d-71cbee59f3e1', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:15'),
(76, '9297efb8-dca5-41c9-a542-f199c4d0095e', '9297efb8-dcd2-4934-ac7d-71cbee59f3e1', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":189,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:15'),
(77, '9297efb9-11ef-4c39-9453-ac3af12d19fe', '9297efb9-1279-4c7f-96cd-86dba3b60954', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:16'),
(78, '9297efb9-124d-4d48-a570-e21c84fa524a', '9297efb9-1279-4c7f-96cd-86dba3b60954', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":128,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:16'),
(79, '9297efbb-e963-475a-9147-53ad23ea51b9', '9297efbb-ea05-437c-93fe-0de80fc97625', NULL, 1, 'command', '{\"command\":\"key:generate\",\"exit_code\":0,\"arguments\":{\"command\":\"key:generate\"},\"options\":{\"show\":false,\"force\":false,\"help\":false,\"quiet\":false,\"verbose\":false,\"version\":false,\"ansi\":false,\"no-ansi\":false,\"no-interaction\":false,\"env\":null},\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:17'),
(80, '9297efbb-e9cf-4526-9bc6-37977109f9a2', '9297efbb-ea05-437c-93fe-0de80fc97625', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/execute-solution\",\"method\":\"POST\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\ExecuteSolutionController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\",\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionConfigValueEnabled:enableRunnableSolutions\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"content-length\":\"82\",\"accept\":\"application\\/json\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"content-type\":\"application\\/json\",\"origin\":\"http:\\/\\/192.168.0.28\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":{\"solution\":\"Facade\\\\Ignition\\\\Solutions\\\\GenerateAppKeySolution\",\"parameters\":[]},\"session\":[],\"response_status\":200,\"response\":\"HTML Response\",\"duration\":129,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:17'),
(81, '9297efbf-7b11-41e2-912e-92839b450b38', '9297efbf-7b90-4d91-8650-aaed0c75a440', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:20'),
(82, '9297efbf-7b6b-4a48-8e01-01b9fe18570a', '9297efbf-7b90-4d91-8650-aaed0c75a440', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":128,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:20'),
(83, '9297efbf-c344-4f57-a4df-9ac2b5201eb7', '9297efbf-c3be-4276-8b0f-584577167a2d', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:20'),
(84, '9297efbf-c398-49b3-a30d-e34428db46e0', '9297efbf-c3be-4276-8b0f-584577167a2d', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.30\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9\",\"cookie\":\"XSRF-TOKEN=eyJpdiI6InBjbTIxeE9FR1hTVnlURkFBVmVmM1E9PSIsInZhbHVlIjoiYlhkNDdRR3hQcXdUQVB5VkJNSkEwMCs2bllPUHRIaHV1YVhzR2htbDZTbUxTWFIwVlR3U3RGNnkrS0xQNGZFekdBTGRQaXM1QUhUaTMySDhJZXRpSnpZZzFOOFhld0t6VjJaUDc2VWwyY3dcL2UxcytzUVNXUkZsaDJ3dmw4R0F0IiwibWFjIjoiMGE0YTE2NjhiZGQ0ODRjYjczYzM4Y2JiYzBkNGEwNGZmMzdmNjkyZGM0ZmYwYTEyM2EyMzdhOTFmMGNkMmU0NiJ9; villezone_session=eyJpdiI6Ikt1WkxmaGFvMGg1K2NoaitRcVJaelE9PSIsInZhbHVlIjoieXlzS09vVXhRNlg3YkE0eFFlV2dxMTJPNGM4azVrMTQwVGxEcnlRaCtSd3ZzaXZESGxFYXFcL2hMTVdcL0RIMFVpck5YZHMyVlNNeTZpTG9jVmtXTWQxWnREUHAzc0Y0WHBWbzJaZ2hHRXFJaVV1YVJaVW1kYjFyUUFSSEw4V2l0ayIsIm1hYyI6Ijk5MzhhNzBlMzQxNTk2NzdiNjMwNzU1MDA0ZGI4MTM4YzhiMWFhMmY5Mzc4N2I4MTgxMzk5ZTBiNjE5MGMyODYifQ%3D%3D\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":135,\"memory\":2,\"hostname\":\"Hasti6013\"}', '2021-01-28 05:43:20'),
(85, '929814ec-c19d-44b8-8a0e-2b6bfb12f851', '929814ec-c346-465e-a019-a8606b07da9c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 07:27:17'),
(86, '929814ec-c313-408f-97b6-4c730abcb9be', '929814ec-c346-465e-a019-a8606b07da9c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":242,\"memory\":8,\"hostname\":\"Hasti6013\"}', '2021-01-28 07:27:17'),
(87, '929814f8-07dd-4801-b905-20ea1326888b', '929814f8-0880-4135-aa68-e2f56899bb2c', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 07:27:24'),
(88, '929814f8-0857-49be-a09c-03a94b2b4c8f', '929814f8-0880-4135-aa68-e2f56899bb2c', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":197,\"memory\":6,\"hostname\":\"Hasti6013\"}', '2021-01-28 07:27:24'),
(89, '9298177b-f675-4e4a-bd24-5896ec946513', '9298177b-f702-43d7-8b81-3f09583d7e09', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 07:34:26'),
(90, '9298177b-f6de-477d-a833-66f28c7bf2d4', '9298177b-f702-43d7-8b81-3f09583d7e09', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":186,\"memory\":6,\"hostname\":\"Hasti6013\"}', '2021-01-28 07:34:26'),
(91, '9298177c-2c1b-44ce-86b1-2f9a408e398a', '9298177c-2ca3-4656-be41-0214d88a2cfd', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 07:34:26'),
(92, '9298177c-2c7c-46d6-b752-7a6eb2b582a2', '9298177c-2ca3-4656-be41-0214d88a2cfd', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":120,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 07:34:26'),
(93, '92984c3d-116f-4a45-89d5-91809e21bbae', '92984c3d-1341-4912-b683-45210a7c8560', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 10:01:57'),
(94, '92984c3d-1309-49de-b916-2a85b2a8204c', '92984c3d-1341-4912-b683-45210a7c8560', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":229,\"memory\":6,\"hostname\":\"Hasti6013\"}', '2021-01-28 10:01:57'),
(95, '92984c3d-4577-41ce-b3e4-6102a44cb15b', '92984c3d-45fa-485a-90b6-b0d9155c531a', NULL, 1, 'command', '{\"command\":\"help\",\"exit_code\":0,\"arguments\":[],\"options\":[],\"hostname\":\"Hasti6013\"}', '2021-01-28 10:01:57'),
(96, '92984c3d-45d4-4365-8665-cacd5d19cffa', '92984c3d-45fa-485a-90b6-b0d9155c531a', NULL, 1, 'request', '{\"ip_address\":\"192.168.0.24\",\"uri\":\"\\/_ignition\\/health-check\",\"method\":\"GET\",\"controller_action\":\"Facade\\\\Ignition\\\\Http\\\\Controllers\\\\HealthCheckController\",\"middleware\":[\"Facade\\\\Ignition\\\\Http\\\\Middleware\\\\IgnitionEnabled\"],\"headers\":{\"host\":\"192.168.0.28\",\"connection\":\"keep-alive\",\"user-agent\":\"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.96 Safari\\/537.36 Edg\\/88.0.705.53\",\"accept\":\"*\\/*\",\"referer\":\"http:\\/\\/192.168.0.28\\/laravel\\/flowdash-laravel\\/\",\"accept-encoding\":\"gzip, deflate\",\"accept-language\":\"en-US,en;q=0.9,it;q=0.8\"},\"payload\":[],\"session\":[],\"response_status\":200,\"response\":{\"can_execute_commands\":true},\"duration\":100,\"memory\":4,\"hostname\":\"Hasti6013\"}', '2021-01-28 10:01:57');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_entries_tags`
--

DROP TABLE IF EXISTS `telescope_entries_tags`;
CREATE TABLE IF NOT EXISTS `telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  KEY `telescope_entries_tags_tag_index` (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `telescope_entries_tags`
--

TRUNCATE TABLE `telescope_entries_tags`;
--
-- Dumping data for table `telescope_entries_tags`
--

INSERT INTO `telescope_entries_tags` (`entry_uuid`, `tag`) VALUES
('9297d59e-a873-4d14-80b1-326b3d6be7bb', 'App\\Models\\User:1');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_monitoring`
--

DROP TABLE IF EXISTS `telescope_monitoring`;
CREATE TABLE IF NOT EXISTS `telescope_monitoring` (
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `telescope_monitoring`
--

TRUNCATE TABLE `telescope_monitoring`;
-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG' COMMENT 'Default Password: 12345678',
  `user_type` tinyint(1) NOT NULL COMMENT '0: admin\r\n1: super admin',
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `mobile_number`, `email`, `email_verified_at`, `password`, `user_type`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, 'admin@admin.com', NULL, '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG', 1, NULL, NULL, 'urNul8RDDLurqh5nOPMYNmwdQvkSV5iAw7MOQMsXHt0rCHgTbsUIVFRlaWWN', NULL, NULL, '2021-01-27 23:00:16', '2021-01-27 23:00:16'),
(2, 'Mohit Mangukia', NULL, 'mohit@sixty13.com', NULL, '$2y$10$fSBrrDdi3TW8o4xbdTLW0u69y15ahvFBaFfPpCABwBUvuy1r3jUqG', 0, NULL, NULL, 'll2Oz7qfHUkLyY4tZSKnvIgT7uomVGQo3FdWxFpp3NRhuqXRBOBv4ml73WjV', NULL, NULL, '2021-02-05 04:13:16', '2021-02-05 04:13:16');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
